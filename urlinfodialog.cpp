#include "urlinfodialog.h"
#include "ui_urlinfodialog.h"

UrlInfoDialog::UrlInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UrlInfoDialog)
{
    ui->setupUi(this);
}

void UrlInfoDialog::setUrl(QString url) {ui->lineEdit->setText(url);}

void UrlInfoDialog::setUrlInfo(TrackInfo tif) {
    if(!tif.hasShoutCast()) return;
    QString text;
    if(!tif.getIcyField(ICY_NOTICE1).isEmpty())
        text += "Notice 1: " +tif.getIcyField(ICY_NOTICE1) + "<br>";
    if(!tif.getIcyField(ICY_NOTICE2).isEmpty())
        text += "Notice 2: " +tif.getIcyField(ICY_NOTICE2) + "<br>";
    if(!tif.getIcyField(ICY_NAME).isEmpty())
        text += "Steam name: " +tif.getIcyField(ICY_NAME) + "<br>";
    if(!tif.getIcyField(ICY_GENRE).isEmpty())
        text += "Stream genre: " +tif.getIcyField(ICY_GENRE) + "<br>";
    if(!tif.getIcyField(ICY_CURRENT).isEmpty())
        text += "Currently playing: " +tif.getIcyField(ICY_CURRENT) + "<br>";
    if(!tif.getIcyField(ICY_BR).isEmpty())
        text += "Stream Bitrate: " +tif.getIcyField(ICY_BR) + "kbps<br>";
    ui->urlInfoTextBrowser->setHtml(text);
}

UrlInfoDialog::~UrlInfoDialog()
{
    delete ui;
}
