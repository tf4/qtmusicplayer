#ifndef FILEINFODIALOG_H
#define FILEINFODIALOG_H

#include <QDialog>
#include <QHash>
#include <QValidator>
#include "trackinfo.h"
#include "utils/taghandler.h"

namespace Ui {
class FileInfoDialog;
}

class FileInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FileInfoDialog(QWidget *parent = 0);
    void fillDialog(QString filepath);
    void fillID3v2(TrackInfo *t);
    void fillID3v1(TrackInfo *t);
    void clearBasicInfo();
    void copyID3v1toBasic();
    void copyID3v2toBasic();
    void copyID3v1toId3v2();
    void copyID3v2toId3v1();
    void fillBasicInfo(TrackInfo *t, QString filepath);
    void enableID3v1Fields(int state);
    void enableID3v2Fields(int state);
    void populateGenreWidgets();
    QString getFormat(QString filepath);
    QString getWmaFormat(void *wfm);
    void fieldEdited(const QString & text);
    void id3v1FieldEdited(const QString & text);
    void id3v2FieldEdited(const QString & text);
    void updateEncLabel(const QString & text);
    void saveChanges();
    void saveID3v1Changes();
    void saveID3v2Changes();
    void initID3v1ObjectHash();
    void initID3v2ObjectHash();
    ~FileInfoDialog();

private:
    Ui::FileInfoDialog *ui;
    TrackInfo *tif;
    QHash<QString, ID3v1Identifiers> editedID3v1Fields;
    QHash<QString, ID3v1Identifiers> id3v1ObjectHash;
    QHash<QString, QString> editedID3v2Fields;
    QHash<QString, QString> id3v2ObjectHash;
    void setupSignals();
    void setupInputMasks();
    void setupID3v1Signals(bool val);
    void commentsFieldEdited();
};

#endif // FILEINFODIALOG_H
