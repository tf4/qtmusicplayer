#ifndef LISTOPERATIONSWORKER_H
#define LISTOPERATIONSWORKER_H

#include <QObject>
#include "customtypes.h"

enum OperationTypes {NOP, RANDOMIZE, REVERSE, SORTBYTITLE, SORTBYFILENAME, SORTBYFILEPATH, REMOVEITEMS, CROPITEMS,
                    REMOVEMISSING, REMOVEDUPLICATES, APPENDITEMS};

class ListOperationsWorker : public QObject
{
    Q_OBJECT
public:
    explicit ListOperationsWorker(QObject *parent = 0);
    void stopWorker(bool stop);
    bool isWorking();
    void setDataToOperate(VectorList data, QVector<int> selected, int busyitem, int lastduration);
    void setFolderToAppend(QString folderpath);
    void clearData();
    void selectOperation(OperationTypes type);
    void performSelectedOperation();
    bool randomize();
    bool reverse();
    bool quickSortColumnOfQStrings(int column, int lo, int hi, bool getfn=false);
    bool sortByTitle();
    bool sortByFilename();
    bool sortByFilePath();
    bool removeItems();
    bool removeMissing();
    bool removeDuplicates();
    bool cropItems();
    bool appendItems();
    int getOperationProgress();

private:
    bool stopflag;
    OperationTypes operationtype;
    VectorList datalist;
    QVector<int> dataswaps;
    QVector<int> selections;
    QString foldertoappend;
    int activeitem;
    int duration;
    int operationprogress;
    void exch(int i, int j);
    int partition(int column, int lo, int hi, bool getfilename=false);

signals:
    void signalOperationFinished(bool, VectorList, QVector<int>, int, int);

public slots:

};

#endif // LISTOPERATIONSWORKER_H
