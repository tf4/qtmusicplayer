#ifndef PLAYLISTWRITERWORKER_H
#define PLAYLISTWRITERWORKER_H

#include <QObject>
#include <QList>
#include <QVector>
#include "customtypes.h"

class PlaylistWriterWorker : public QObject
{
    Q_OBJECT
public:
    explicit PlaylistWriterWorker(QObject *parent = 0);
    void setPlaylistPath(QString path);
    void setDataToWrite(VectorList data);
    void clearData();
    void stopWorker(bool stop);
    int getOperationProgress();


private:
    QString filepath;
    VectorList datalist;
    bool stopflag;
    int operationprogress;

signals:
    void signalSavedPlaylist(bool success);

public slots:
    void writeM3UFile();

};

#endif // PLAYLISTWRITERWORKER_H
