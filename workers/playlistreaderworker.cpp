#include "playlistreaderworker.h"
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QDirIterator>
#include <QTextStream>
#include <QRegExp>
#include <QCoreApplication>


PlaylistReaderWorker::PlaylistReaderWorker(QObject *parent) :
    QObject(parent),
    stopflag(false), readFolder(false), operationprogress(0)
{
}

void PlaylistReaderWorker::stopWorker(bool stop) {
    stopflag = stop;
}

void PlaylistReaderWorker::setPlaylistPath(QString path, bool isFolder) {
    this->filepath = path;
    readFolder = isFolder;
}

void PlaylistReaderWorker::readM3Ufile() {
    VectorList playlistitems;
    if(readFolder) {
        QStringList newitems;
        QDir dir(filepath);
        dir.setFilter(QDir::Files);
        dir.setNameFilters(QStringList() << "*.mp3" << "*.wav" << "*.wma");
        QDirIterator it(dir, QDirIterator::Subdirectories);
        QFile *fh = new QFile(QDir::toNativeSeparators(QCoreApplication::applicationDirPath()) + DEF_TEMPFILE);
        fh->open(QIODevice::WriteOnly);
        QTextStream writestream(fh);
        //writestream.setCodec("UTF-8");
        while(it.hasNext()) {
            if(stopflag) break;
            it.next();
            newitems << it.filePath();
            writestream << it.filePath() + "\n";
        }
        fh->close();
        filepath = QString(DEF_TEMPFILE);
    }
    QFile *fh = new QFile(filepath);
    fh->open(QIODevice::ReadOnly);
    QTextStream *readstream = new QTextStream(fh);
    readstream->setCodec("UTF-8");
    QString line;
    QRegExp rx("^#EXTINF:([^,]+),(.*)$");
    //rx.setPattern("^#EXTINF:([^,]+),(.*)$");
    QVector<QString> entry(ITEMCOLUMNS,"");
    while(!readstream->atEnd() && !stopflag) {
        line = readstream->readLine();
        if(line.startsWith("#EXTINF:")) {
            if (rx.indexIn(line) != -1) {
                entry[ITEMLENGTH] = rx.cap(1);
                entry[ITEMTEXT] = rx.cap(2);
                entry[ITEMPATH] = readstream->readLine();
                entry[ITEMHASINFO] = QString::number(true);
                playlistitems.append(entry);
            }
        }
        else if(line.startsWith("#EXTM3U", Qt::CaseInsensitive))
            continue;
        else {
            if(line.isEmpty() || line.isNull()) continue;
            QFileInfo f(line);
            entry[ITEMLENGTH] = "";
            entry[ITEMTEXT] = f.fileName();
            entry[ITEMPATH] = line;
            entry[ITEMHASINFO] = QString::number(false);
            playlistitems.append(entry);
        }
    }
    fh->close();
    stopflag = false;
    emit signalParsedPlaylist(playlistitems);
}

int PlaylistReaderWorker::getOperationProgress() {return operationprogress;}

