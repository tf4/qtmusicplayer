#ifndef PLAYLISTREADERWORKER_H
#define PLAYLISTREADERWORKER_H

#include <QObject>
#include <QList>
#include <QVector>
#include "customtypes.h"
#include "defaults.h"

class PlaylistReaderWorker : public QObject
{
    Q_OBJECT
public:
    explicit PlaylistReaderWorker(QObject *parent = 0);
    void setPlaylistPath(QString path, bool isFolder=false);
    void stopWorker(bool stop);
    int getOperationProgress();


private:
    QString filepath;
    bool stopflag;
    bool readFolder;
    int operationprogress;

signals:
    void signalParsedPlaylist(VectorList data);

public slots:
    void readM3Ufile();
};

Q_DECLARE_METATYPE(VectorList)

#endif // PLAYLISTREADERWORKER_H
