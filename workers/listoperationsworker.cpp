#include "listoperationsworker.h"
#include <QTime>
#include <QDebug>
#include <QtGlobal>
#include <QFileInfo>
#include <QDirIterator>
#include <QStringList>
#include <QHash>
#include "bassaudio.h"

ListOperationsWorker::ListOperationsWorker(QObject *parent) :
    QObject(parent),
    stopflag(false), operationtype(NOP), operationprogress(0)
{
}

void ListOperationsWorker::stopWorker(bool stop) {stopflag = stop;}

bool ListOperationsWorker::isWorking() {return stopflag;}

void ListOperationsWorker::setDataToOperate(VectorList data, QVector<int> selected, int busyitem, int lastduration) {
    datalist = data;
    selections = selected;
    activeitem = busyitem;
    duration = lastduration;
}

void ListOperationsWorker::setFolderToAppend(QString folderpath) {
    foldertoappend = folderpath;
}


void ListOperationsWorker::clearData() {
    datalist.clear();
    dataswaps.clear();
    selections.clear();
    foldertoappend.clear();
    duration = 0;
    activeitem = -1;
    operationprogress = 0;
}

int ListOperationsWorker::getOperationProgress() {return operationprogress;}

void ListOperationsWorker::selectOperation(OperationTypes type) {operationtype = type;}

void ListOperationsWorker::performSelectedOperation() {
    bool success = false;
    switch (operationtype) {
    case RANDOMIZE:
        if(randomize()) {
            success = true;
            for(int i = 0; i < selections.size(); i++)
                selections[i] = dataswaps.indexOf(selections[i]);
        }
        break;
    case REVERSE:
        if(reverse())
            success = true;
        break;
    case SORTBYTITLE:
        if(sortByTitle()) {
            success = true;
            for(int i = 0; i < selections.size(); i++)
                selections[i] = dataswaps.indexOf(selections[i]);
        }
        break;
    case SORTBYFILENAME:
        if(sortByFilename()) {
            success = true;
            for(int i = 0; i < selections.size(); i++)
                selections[i] = dataswaps.indexOf(selections[i]);
        }
        break;
    case SORTBYFILEPATH:
        if(sortByFilePath()) {
            success = true;
            for(int i = 0; i < selections.size(); i++)
                selections[i] = dataswaps.indexOf(selections[i]);
        }
        break;
    case REMOVEITEMS:
        if(removeItems()) {
            success = true;
        }
        break;
    case CROPITEMS:
        if(cropItems()) {
            success = true;
        }
        break;
    case REMOVEMISSING:
        if(removeMissing()) {
            success = true;
        }
        break;
    case REMOVEDUPLICATES:
        if(removeDuplicates()) {
            success = true;
        }
        break;
    case APPENDITEMS:
        if(appendItems()) {
            success = true;
        }
        break;
    default:
        success = false;
        break;
    }
    stopflag = false;
    operationtype = NOP;
    emit this->signalOperationFinished(success, datalist, selections, activeitem, duration);
}

bool ListOperationsWorker::appendItems() {
    QStringList newitems;
    QDir dir(foldertoappend);
    dir.setFilter(QDir::Files);
    dir.setNameFilters(QStringList() << "*.mp3" << "*.wav" << "*.wma");
    QDirIterator it(dir, QDirIterator::Subdirectories);
    while(it.hasNext()) {
        if(stopflag) return false;
        it.next();
        newitems << it.filePath();
    }
    int initialsize = newitems.size();
    QVector<QString> entry(ITEMCOLUMNS,"");
    QString line = "";
    while(!newitems.isEmpty()) {
        if(stopflag) return false;
        line = newitems.takeFirst();
        if(line.isEmpty() || line.isNull()) continue;
        QFileInfo f(line);
        entry[ITEMLENGTH] = "";
        entry[ITEMTEXT] = f.fileName();
        entry[ITEMPATH] = line;
        entry[ITEMHASINFO] = QString::number(false);
        datalist.append(entry);
        operationprogress = ((initialsize - newitems.size())*100) / initialsize;
    }
    return true;
}

bool ListOperationsWorker::removeItems() {
    int row, initialsize;
    if(selections.isEmpty() || datalist.isEmpty())
        return false;
    initialsize = selections.size();
    qSort(selections.begin(), selections.end());
    while(!selections.isEmpty()) {
        if(stopflag) return false;
        row = selections.takeLast();
        datalist.removeAt(row);
        if(activeitem > row){
            activeitem -= 1;
        }
        else if(activeitem == row)
            activeitem = -1;
        operationprogress = ((initialsize - selections.size())*100)/initialsize;
    }
    duration = 0;
    return true;
}

bool ListOperationsWorker::cropItems() {
    VectorList newdatalist;
    bool activeitemIsSet = false;
    if(datalist.isEmpty() || selections.isEmpty())
        return false;
    qSort(selections.begin(), selections.end());
    for(int i = 0; i < selections.size(); i++) {
        int row = selections[i];
        newdatalist.append(datalist[row]);
        if(!activeitemIsSet && row == activeitem) {
            activeitem = newdatalist.size() - 1;
        }
        operationprogress = (i*100)/(selections.size()-1);
    }
    datalist.clear();
    datalist = newdatalist;
    for(int i = 0; i < selections.size(); i++)
        selections[i] = i;
    return true;
}

bool ListOperationsWorker::randomize() { //Knuth Fischer Yates algorithm
    if(datalist.isEmpty()) return false;
    dataswaps = QVector<int>(datalist.size(), 0);
    for(int i = 0; i < datalist.size(); ++i) dataswaps[i] = i;
    int high, low, next;
    high = datalist.size() - 1;
    for(int i = 0; i < datalist.size(); ++i) {
        if(stopflag) return false;
        qsrand(uint(QTime::currentTime().msec()) + uint(datalist[i][ITEMLENGTH].toInt()));
        low = i;
        next = qrand() % ((high+1)-low) + low;
        exch(i, next);
        operationprogress = (i*100)/high;
    }
    activeitem = dataswaps.indexOf(activeitem);

    return true;
}

bool ListOperationsWorker::reverse() {
    if(datalist.isEmpty()) return false;
    dataswaps = QVector<int>(datalist.size(), datalist.size()/2);
    for(int i = 0; i < datalist.size() / 2; i++) {
        if(stopflag)
            return false;
        QVector<QString> temp = datalist[datalist.size() - 1 - i];
        datalist[datalist.size() - 1 - i] = datalist[i];
        datalist[i] = temp;
        dataswaps[i] = datalist.size() - 1 - i;
        dataswaps[datalist.size() - 1 - i] = i;
        operationprogress = (i*100)/(datalist.size()/2);
    }
    activeitem = dataswaps.indexOf(activeitem);
    for(int i = 0; i < selections.size(); i++)
        selections[i] = dataswaps.indexOf(selections[i]);
    return true;
}

bool ListOperationsWorker::sortByTitle() {
    if(datalist.size() < 2)
        return false;
    if(!randomize())
        return false;
    operationprogress = 0;
    return quickSortColumnOfQStrings(ITEMTEXT, 0, datalist.size() - 1);
}

bool ListOperationsWorker::sortByFilename() {
    if(datalist.size() < 2)
        return false;
    if(!randomize())
        return false;
    operationprogress = 0;
    return quickSortColumnOfQStrings(ITEMPATH, 0, datalist.size() - 1, true);
}

bool ListOperationsWorker::sortByFilePath() {
    if(datalist.size() < 2)
        return false;
    if(!randomize())
        return false;
    operationprogress = 0;
    return quickSortColumnOfQStrings(ITEMPATH, 0, datalist.size() - 1);
}

bool ListOperationsWorker::removeMissing() {
    int itemduration, initialsize;
    if(datalist.isEmpty()) return false;
    initialsize = datalist.size() - 1;
    if(!selections.isEmpty())
        qSort(selections.begin(), selections.end());
    for(int i = initialsize; i >= 0; --i){
        if(!QFileInfo(datalist[i][ITEMPATH]).isFile()) {
            itemduration = datalist[i][ITEMLENGTH].toInt();
            datalist.removeAt(i);
            int j = selections.size() - 1;
            if(!selections.isEmpty()) {
                while(selections[j] >= i) {
                    if(i == selections[j]) {
                        selections.remove(j);
                        duration -= itemduration;
                    }
                    else {
                        selections[j] -= 1;
                    }
                    j--;
                    if(j > selections.size()-1 || j < 0) break;
                }
            }
            if(activeitem > i){
                activeitem -= 1;
            }
            else if(activeitem == i)
                activeitem = -1;
            operationprogress = ((i - initialsize)*100) / initialsize;
        }
    }
    return true;
}


bool ListOperationsWorker::removeDuplicates() {
    if(datalist.isEmpty()) return false;
    int itemduration;
    QHash<QString, int> hash;
    int i = 0;
    if(!selections.isEmpty())
        qSort(selections.begin(), selections.end());
    while(i < datalist.size()) {
        if(hash.contains(datalist[i][ITEMPATH])) {
            itemduration = datalist[i][ITEMPATH].toInt();
            datalist.removeAt(i);
            int j = selections.size() - 1;
            if(!selections.isEmpty()) {
                while(selections[j] >= i) {
                    if(i == selections[j]) {
                        selections.remove(j);
                        duration -= itemduration;
                    }
                    else {
                        selections[j] -= 1;
                    }
                    j--;
                    if(j > selections.size()-1 || j < 0) break;
                }
            }
            if(activeitem > i){
                activeitem -= 1;
            }
            else if(activeitem == i)
                activeitem = -1;
        }
        else {
            hash.insert(datalist[i][ITEMPATH], i);
            i++;
        }
        operationprogress = ((datalist.size() - i)*100) / datalist.size();
    }
    return true;
}

bool ListOperationsWorker::quickSortColumnOfQStrings(int column, int lo, int hi, bool getfn) {
    if(hi <= lo) return true;
    int j = partition(column, lo, hi, getfn);
    if(j < 0)
        return false;
    quickSortColumnOfQStrings(column, lo, j-1, getfn);
    quickSortColumnOfQStrings(column, j+1, hi, getfn);
    return true;
}


void ListOperationsWorker::exch(int i, int j) {
    QVector<QString> temp = datalist[i];
    int tempswap = dataswaps[i];
    datalist[i] = datalist[j];
    datalist[j] = temp;
    dataswaps[i] = dataswaps[j];
    dataswaps[j] = tempswap;
}


int ListOperationsWorker::partition(int column, int lo, int hi, bool getfilename) {
    int i = lo, j = hi + 1;
    while(true) {
        if(stopflag) return -1;
        if(getfilename) {
            while(QString::compare(QFileInfo(datalist[++i][ITEMPATH]).fileName(),
                                   QFileInfo(datalist[lo][ITEMPATH]).fileName(), Qt::CaseInsensitive) < 0)
                if(i == hi) break;
            while(QString::compare(QFileInfo(datalist[lo][ITEMPATH]).fileName(),
                                   QFileInfo(datalist[--j][ITEMPATH]).fileName(), Qt::CaseInsensitive) < 0)
                if(j == lo) break;
        }
        else {
            while(QString::compare(datalist[++i][column], datalist[lo][column], Qt::CaseInsensitive) < 0)
                if(i == hi) break;
            while(QString::compare(datalist[lo][column], datalist[--j][column], Qt::CaseInsensitive) < 0)
                if(j == lo) break;
        }
        if(i >= j) break;
        exch(i, j);
    }
    exch(lo, j);
    return j;
}


