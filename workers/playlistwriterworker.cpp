#include "playlistwriterworker.h"
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QTextStream>

PlaylistWriterWorker::PlaylistWriterWorker(QObject *parent) :
    QObject(parent),
    stopflag(false), operationprogress(0)
{
}

void PlaylistWriterWorker::stopWorker(bool stop) {
    stopflag = stop;
}

void PlaylistWriterWorker::setPlaylistPath(QString path) {
    this->filepath = path;
}

void PlaylistWriterWorker::setDataToWrite(VectorList data) {datalist = data;}

void PlaylistWriterWorker::clearData() {
    datalist.clear();
    filepath = "";
    operationprogress = 0;
}

int PlaylistWriterWorker::getOperationProgress() {return operationprogress;}

void PlaylistWriterWorker::writeM3UFile() {
    QFile *fh = new QFile(filepath);
    fh->open(QIODevice::WriteOnly);
    if(!fh->isWritable()) {
        emit this->signalSavedPlaylist(false);
        return;
    }
    QTextStream writestream(fh);
    writestream.setCodec("UTF-8");
    writestream << "#EXTM3U\n";
    for(int i = 0; i < datalist.size(); i++) {
        if(stopflag) {
            emit this->signalSavedPlaylist(false);
            return;
        }
        if(datalist[i][ITEMTEXT] != "") {
            writestream << "#EXTINF:" << datalist[i][ITEMLENGTH] << "," << datalist[i][ITEMTEXT] << "\n";
            writestream << datalist[i][ITEMPATH] << "\n";
        }
        else
            writestream << datalist[i][ITEMPATH] << "\n";
        operationprogress = (i*100)/datalist.size();
    }
    fh->close();
    stopflag = false;
    emit this->signalSavedPlaylist(true);
}
