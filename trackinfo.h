#ifndef TRACKINFO_H
#define TRACKINFO_H

#include "bass.h"
#include <QObject>


enum TrackFields {
    TITLE = 0,
    ARTIST = 1,
    ALBUM = 2,
    ALBUM_ARTIST = 3,
    TRACK_LENGTH = 4,
    BITRATE = 5,
    TRACK_CHANNELS = 6,
    SAMPLE_RATE = 7,
    TRACK_NUM = 8,
    TRACK_YEAR= 9,
    TRACK_GENRE = 10,
    TRACK_COMMENTS = 11,
    TRACK_BPM = 12,
    DISC_NUM = 13,
    TRACK_COMPOSER = 14,
    TRACK_PUBLISHER = 15,
    TRACK_ENCODED_BY = 16,
    TRACK_ORIGARTIST = 17,
    TRACK_COPYRIGHT = 18,
    TRACK_URL = 19,
    NUM_OF_FIELDS = 20
};

enum ShoutCastFields {
    ICY_CODE = 0,
    ICY_NOTICE1 = 1,
    ICY_NOTICE2 = 2,
    ICY_NAME = 3,
    ICY_GENRE = 4,
    ICY_URL = 5,
    ICY_CONTENT = 6,
    ICY_PUB = 7,
    ICY_BR = 8,
    ICY_METAINT = 9,
    ICY_CURRENT = 10,
    ICY_NUM_OF_FIELDS = 11
};

class TrackInfo
{
public:
    TrackInfo();
    QString getTrackPath();
    void setTrackPath(QString path);
    void setField(int field, QString value);
    QString getField(int field);
    void setIcyField(int field, QString value);
    QString getIcyField(int field);
    void clearFields();
    void clearIcyFields();
    bool hasID3v1();
    bool hasID3v2();
    bool hasAPE();
    bool hasShoutCast();
    void setID3v1(bool val);
    void setID3v2(bool val);
    void setAPE(bool val);
    void setShoutCast(bool val);

private:
    QString trackPath;
    DWORD frequency;
    QString fields[NUM_OF_FIELDS];
    QString icyfields[ICY_NUM_OF_FIELDS];
    bool id3v1;
    bool id3v2;
    bool ape;
    bool shoutcast;
};

#endif // TRACKINFO_H
