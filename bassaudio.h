#ifndef BASSAUDIO_H
#define BASSAUDIO_H

#include <QObject>
#include <QString>
#include <QTimer>
#include "trackinfo.h"
#include "bass.h"
#include "bassmix.h"

void qSleep(int ms);

typedef struct {
    int freq;
    int channels;
} StreamAttr;

class BassAudio : public QObject
{
    Q_OBJECT
public:
    explicit BassAudio(int v, QObject *parent = 0);
    ~BassAudio();
    bool prepareTrack(QString path, int* length);
    void playTrack();
    void stopTrack();
    void pauseTrack();
    unsigned int getVolume();
    void setVolume(unsigned int value);
    bool isPlaying();
    void enableTrackPositionUpdates(bool state);
    void setTrackPosition(double posinsecs);
    void metaUrlInfo();
    const char* getMetaUrlInfo();
    const char* getMetaUrlExtraInfo();
    void stopUrlMetaTimer();
    StreamAttr getAudioAttributes();
private:
    HSTREAM Stream;
    HSTREAM MixStream;
    DWORD Stream_State;
    DWORD flags;
    DWORD frequency;
    QWORD lengthinbytes;
    unsigned int volume;
    bool paused;
    QTimer* posupdatetimer;
    QTimer* urlmetaupdatetimer;
    static void CALLBACK EndSyncProc(HSYNC handle, DWORD channel, DWORD data, void *user);
    static void CALLBACK MetaSyncProc(const void* buffer, DWORD length, void *user);


signals:
    void signalStreamEnded();
    void signalTrackPosition(int position);
    void signalMetaUrlUpdate(const char* metainfo, const char* metaextra);
    void signalStopMetaTimer();

public slots:
    void emitTrackPosition();
};



#endif // TRACK_H
