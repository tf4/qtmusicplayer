#include "playlistmodel.h"
#include <QColor>
#include <QDebug>

/*PlaylistModel::PlaylistModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}*/
using namespace global_methods;

VectorList PlaylistModel::getModelRawData() {
    return datalist;
}

int PlaylistModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return datalist.count();
}

int PlaylistModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return ITEMCOLUMNS;
}

QVariant PlaylistModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if(role == Qt::TextAlignmentRole) {
        if(orientation == Qt::Horizontal) {
            if(section == ITEMTEXT)
                return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
            return QVariant(Qt::AlignRight | Qt::AlignVCenter);
        }
    }
    if(role != Qt::DisplayRole)
        return QVariant();
    else {
        if(orientation == Qt::Horizontal){
            if(section == ITEMLENGTH){
                return QString("Length");
            }
            else if( section == ITEMTEXT)
                return QString("Song Info");
        }
    }
    return QVariant();
}

QVariant PlaylistModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid())
        return QVariant();
    if(index.row() >= datalist.size())
        return QVariant();
    if(index.column() >= ITEMCOLUMNS)
        return QVariant();
    if(role == Qt::DisplayRole) {
        switch(index.column()) {
        case ITEMTEXT:
            return QString::number(index.row()+1) +"." + datalist.at(index.row())[index.column()];
            break;
        case ITEMLENGTH:
            return global_methods::formatLengthDisplay(datalist.at(index.row())[index.column()]);
            break;
        /*case ITEMPATH:
            return datalist.at(index.row())[index.column()];
            break;*/
        default:
            return datalist.at(index.row())[index.column()];
        }
    }
    if(role == Qt::UserRole) {
        return datalist.at(index.row())[ITEMLENGTH];
    }
    //Formatting stuff...
    if(role == Qt::TextAlignmentRole) {
        if(index.column() == ITEMLENGTH)
            return QVariant(Qt::AlignRight | Qt::AlignVCenter);
        return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
    }
    if(role == Qt::TextColorRole){
        return QVariant(QColor(Qt::black));
    }
    return QVariant();
}

bool PlaylistModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    Q_UNUSED(role);
    if(index.isValid() && index.row() >= 0 && index.row() < datalist.length()) {
        int column = index.column();
        int row = index.row();
        if(column < ITEMCOLUMNS) {
            datalist[row][column] = value.toString();
        }
        emit this->dataChanged(index, index);
    }
    return true;
}

bool PlaylistModel::insertRows(int row, int count, const QModelIndex & parent /*= QModelIndex()*/) {
    this->beginInsertRows(parent, row, row + count -1);
    QVector<QString> newentry(ITEMCOLUMNS,"");
    newentry[ITEMLENGTH] = "0";
    for(int i = 0; i < count; i++){
        this->datalist.insert(row+i, newentry);
    }
    this->endInsertRows();
    return true;
}

bool PlaylistModel::removeRows(int row, int count, const QModelIndex & parent /*= QModelIndex()*/) {
    this->beginRemoveRows(parent, row, row + count - 1);
    for(int i = 0; i < count; i++) {
        datalist.removeAt(row + i);
    }
    this->endRemoveRows();
    return true;
}

bool PlaylistModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int s,
                            const QModelIndex &destinationParent, int destinationChild)
{
    Q_UNUSED(sourceParent); Q_UNUSED(s); Q_UNUSED(destinationParent);
    if(sourceRow == destinationChild)
        return true;
    QVector<QString> temp = datalist.at(sourceRow);
    datalist[sourceRow] = datalist[destinationChild];
    datalist[destinationChild] = temp;
    return true;
}

Qt::DropActions PlaylistModel::supportedDragActions() const {
    return Qt::MoveAction;
}

Qt::ItemFlags PlaylistModel::flags(const QModelIndex & index) const {
    if(index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable
                | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    else
        return Qt::ItemIsDropEnabled | Qt::ItemIsEditable
                | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

