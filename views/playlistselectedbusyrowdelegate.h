#ifndef PLAYLISTSELECTEDBUSYROWDELEGATE_H
#define PLAYLISTSELECTEDBUSYROWDELEGATE_H

#include <QItemDelegate>

class PlaylistSelectedBusyRowDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit PlaylistSelectedBusyRowDelegate(QWidget *parent = 0);
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;

signals:

public slots:

};

#endif // PLAYLISTSELECTEDBUSYROWDELEGATE_H
