#ifndef PLAYLISTSELECTEDROWDELEGATE_H
#define PLAYLISTSELECTEDROWDELEGATE_H

#include <QItemDelegate>

class PlaylistSelectedRowDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit PlaylistSelectedRowDelegate(QWidget *parent = 0);
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;

signals:

public slots:

};

#endif // PLAYLISTSELECTEDROWDELEGATE_H
