#include "playlisttableview.h"
#include "utils/taghandler.h"
#include <QList>
#include <QVector>
#include <QHeaderView>
#include <QApplication>
#include <QFileInfo>
#include <QDebug>
#include <QScrollBar>
#include <QtGlobal>
#include <QMimeData>
#include <QDrag>
#include <QTime>
#include "editplaylistentrydialog.h"
#include "fileinfodialog.h"
#include "urlinfodialog.h"

using namespace global_methods;

PlaylistTableView::PlaylistTableView(QWidget *parent) :
    QTableView(parent),
    busyitem(-1), mousereleasedflag(false), dragged_row(-1), doubleclickflag(false),
    rightclickflag(false), cachedSelectionsDurationFlag(false)
{
    this->urliteminfo = new TrackInfo();
    busyItemRowDelegate = new PlaylistRowDelegate();
    selectedItemRowDelegate = new PlaylistSelectedRowDelegate();
    selectedBusyItemRowDelegate = new PlaylistSelectedBusyRowDelegate();
    //Setup playlist reader worker and thread
    playlistReadQueue = new QQueue<QString>();
    m3uFileReaderThread = new QThread;
    m3uFileWriterThread = new QThread;
    m3uFileReader = new PlaylistReaderWorker();
    m3uFileWriter = new PlaylistWriterWorker();
    m3uFileReader->moveToThread(m3uFileReaderThread);
    m3uFileWriter->moveToThread(m3uFileWriterThread);
    connect(m3uFileReader, &PlaylistReaderWorker::signalParsedPlaylist, this, &PlaylistTableView::createViewModel);
    connect(m3uFileReaderThread, &QThread::started, m3uFileReader, &PlaylistReaderWorker::readM3Ufile);
    connect(m3uFileReaderThread, &QThread::finished, this, &PlaylistTableView::readM3UFinished);
    connect(m3uFileWriter, &PlaylistWriterWorker::signalSavedPlaylist, this, &PlaylistTableView::saveM3UDone);
    connect(m3uFileWriterThread, &QThread::started, m3uFileWriter, &PlaylistWriterWorker::writeM3UFile);
    connect(m3uFileWriterThread, &QThread::finished, this, &PlaylistTableView::writeM3UFinished);
    //Setup list operations worker and thread
    listOperationWorker = new ListOperationsWorker();
    listOperationThread = new QThread;
    listOperationWorker->moveToThread(listOperationThread);
    connect(listOperationWorker, &ListOperationsWorker::signalOperationFinished,
            this, &PlaylistTableView::updateViewModel);
    connect(listOperationThread, &QThread::started, listOperationWorker,
            &ListOperationsWorker::performSelectedOperation);
    connect(listOperationThread, &QThread::finished, this, &PlaylistTableView::listOperationFinished);
    //
    connect(this->verticalScrollBar(), &QScrollBar::valueChanged, this, &PlaylistTableView::updateItemsInfo);
    //Create an temporary model just to set the TableView format
    VectorList V;
    this->createViewModel(V);
    setupTableView();
}

void PlaylistTableView::createViewModel(VectorList m){
    this->clearSelection();
    this->cachedSelections.clear();
    this->cachedSelectionsDuration = 0;
    this->cachedSelectionsDurationFlag = false;
    QItemSelectionModel *pm = this->selectionModel();
    model = new PlaylistModel(m);
    this->setModel(model);
    delete pm;
    m3uFileReaderThread->quit();
    selection_model = this->selectionModel();
    setBusyItemInPlaylist(getBusyItemFromPlaylist());
    connect(selection_model, &QItemSelectionModel::selectionChanged, this, &PlaylistTableView::selectionChangedSlot);
    updateItemsInfo();
    emit this->signalPlaylistTotalDurationChanged(getPlaylistTotalTime());
    emit this->signalPlaylistRowNumChanged(true);
    //m->deleteLater();
}

void PlaylistTableView::updateViewModel(bool success, VectorList um, QVector<int> swaps, int newbusyitem,
                                        int newduration) {
    if(!success) {
        listOperationThread->quit();
        return;
    }
    QVector<int> oldselections = cachedSelections;
    cachedSelections.clear();
    this->clearSelection();
    QItemSelectionModel *pm = this->selectionModel();
    model = new PlaylistModel(um);
    this->setModel(model);
    delete pm;
    listOperationWorker->clearData();
    listOperationThread->quit();
    selection_model = this->selectionModel();
    setBusyItemInPlaylist(newbusyitem);
    cachedSelections = swaps;
    cachedSelectionsDuration = newduration;
    updateItemsInfo();
    connect(selection_model, &QItemSelectionModel::selectionChanged, this, &PlaylistTableView::selectionChangedSlot);
    emit this->signalSelectedItemsDurationChanged(cachedSelectionsDuration);
}

void PlaylistTableView::readM3UList(QString listpath, bool isFolder) {
    //Use of queue is deprecated. It never works this way
    mutex.lock();
    if(!m3uFileReaderThread->isRunning()) {
        m3uFileReader->setPlaylistPath(listpath, isFolder);
        playlistReadQueue->enqueue(listpath);
        m3uFileReader->stopWorker(false);
        this->setEnabled(false);
        emit this->signalListOperationRunning(true);
        m3uFileReaderThread->start();
    }
    else{
        playlistReadQueue->enqueue(listpath);
        m3uFileReader->stopWorker(true);
    }
    mutex.unlock();

}

void PlaylistTableView::readM3UFinished() {
    mutex.lock();
    playlistReadQueue->dequeue();
    if(!playlistReadQueue->isEmpty()) {
        //qDebug() << "inside if";
        m3uFileReader->setPlaylistPath(playlistReadQueue->head());
        m3uFileReader->stopWorker(false);
        this->setEnabled(false);
        emit this->signalListOperationRunning(true);
        m3uFileReaderThread->start();
    }
    else {
        emit this->signalListOperationRunning(false);
        this->setEnabled(true);
        this->setFocus();
    }
    if(busyitem >= numOfRows())
        setBusyItemInPlaylist(-1);
    mutex.unlock();
}

void PlaylistTableView::writeM3UList(QString listpath) {
    mutex.lock();
    if(!m3uFileWriterThread->isRunning()) {
        m3uFileWriter->setPlaylistPath(listpath);
        m3uFileWriter->setDataToWrite(dynamic_cast<PlaylistModel*>(this->model)->getModelRawData());
        m3uFileWriter->stopWorker(false);
        this->setEnabled(false);
        emit this->signalListOperationRunning(true);
        m3uFileWriterThread->start();
    }
    mutex.unlock();
}

void PlaylistTableView::saveM3UDone(bool success) {
    m3uFileWriter->clearData();
    m3uFileWriterThread->quit();
    emit this->signalPlaylistSavedSuccessfully(success);
}

void PlaylistTableView::writeM3UFinished() {
    mutex.lock();
    emit this->signalListOperationRunning(false);
    this->setEnabled(true);
    this->setFocus();
    mutex.unlock();
}

void PlaylistTableView::selectPlaylistRow(int row) {
    this->selectRow(row);
    if(row == busyitem)
        this->setItemDelegateForRow(row, selectedBusyItemRowDelegate);
    else this->setItemDelegateForRow(row, selectedItemRowDelegate);
}

void PlaylistTableView::selectionChangedSlot(const QItemSelection &selected, const QItemSelection &deselected) {
    Q_UNUSED(selected);
    Q_UNUSED(deselected);
    QModelIndexList selecteditems = this->selectionModel()->selectedRows();
    QModelIndex item;
    int selecteditemsduration = 0;
    bool ok;
    cachedSelections.clear();
    foreach(item, selecteditems) {
        cachedSelections.append(item.row());
        int temp = model->data(model->index(item.row(), ITEMLENGTH), Qt::UserRole).toString().toInt(&ok);
        if(ok)
            selecteditemsduration += temp;
    }
    updateItemsInfo();
    if(!cachedSelectionsDurationFlag){
        cachedSelectionsDuration = selecteditemsduration;
    }
    cachedSelectionsDurationFlag = false;
    emit this->signalSelectedItemsDurationChanged(cachedSelectionsDuration);
}

void PlaylistTableView::setPlaylistRowDelegate(int row) {
    if(row < 0 || row >= numOfRows() ) return;
    if(cachedSelections.contains(row))
        if(row == getBusyItemFromPlaylist())
            this->setItemDelegateForRow(row, selectedBusyItemRowDelegate);
        else
            this->setItemDelegateForRow(row, selectedItemRowDelegate);
    else if(row == getBusyItemFromPlaylist())
        this->setItemDelegateForRow(row, busyItemRowDelegate);
    else
        this->setItemDelegateForRow(row, NULL);
}

void PlaylistTableView::setBusyItemInPlaylist(int item) {
    int previousbusyitem = busyitem;
    busyitem = item;
    this->setPlaylistRowDelegate(previousbusyitem);
    this->setPlaylistRowDelegate(item);
    if(!this->verticalScrollBar()->isSliderDown()){
        //TODO: Disconnect scroll event from update info
        this->scrollTo(this->model->index(busyitem, 0), QAbstractItemView::PositionAtCenter);
        //TODO: updateinfo and reconnect scroll event
    }
}


int PlaylistTableView::getBusyItemFromPlaylist() {
    return this->busyitem;
}

int PlaylistTableView::swapRows(int start, int target) {
    QItemSelection selection = this->selectionModel()->selection();
    QModelIndex startleft;
    QModelIndex startright;
    QModelIndex targetleft;
    QModelIndex targetright;
    if(!selection.isEmpty()){
        startleft = this->model->index(start, 0);
        startright = this->model->index(start, selection[0].bottomRight().column());
        targetleft = this->model->index(target, 0);
        targetright = this->model->index(target, selection[0].bottomRight().column());
    }
    this->setSelectionMode(QAbstractItemView::MultiSelection);
    QList<int> updateselectionslist;
    if(selection.contains(startleft)){
        updateselectionslist.append(target);
        QItemSelection deselect = QItemSelection(startleft, startright);
        this->selectionModel()->select(deselect, QItemSelectionModel::Deselect);
    }
    if(selection.contains(targetleft)){
        updateselectionslist.append(start);
        QItemSelection deselect = QItemSelection(targetleft, targetright);
        this->selectionModel()->select(deselect, QItemSelectionModel::Deselect);
    }
    while(!updateselectionslist.isEmpty()){
        this->selectPlaylistRow(updateselectionslist.takeLast());
    }
    this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->model->moveRows(QModelIndex(), start, start, QModelIndex(), target);
    if(start == getBusyItemFromPlaylist())
        setBusyItemInPlaylist(target);
    else if(target == getBusyItemFromPlaylist())
        setBusyItemInPlaylist(start);
    return target;
}

void PlaylistTableView::startListOperation(OperationTypes type, QString foldertoappend) {
    mutex.lock();
    if(listOperationThread->isRunning()) {
        mutex.unlock();
        return;
    }
    listOperationWorker->selectOperation(type);
    listOperationWorker->clearData();
    listOperationWorker->setDataToOperate(dynamic_cast<PlaylistModel*>(this->model)->getModelRawData(),
                                          cachedSelections, getBusyItemFromPlaylist(), cachedSelectionsDuration);
    if(type == APPENDITEMS)
        listOperationWorker->setFolderToAppend(foldertoappend);
    cachedSelectionsDurationFlag = true;
    listOperationWorker->stopWorker(false);
    this->setEnabled(false);
    emit this->signalListOperationRunning(true);
    listOperationThread->start();
    mutex.unlock();
}

void PlaylistTableView::listOperationFinished() {
    mutex.lock();
    qDebug() << "Operation finished";
    this->listOperationWorker->stopWorker(false);
    emit this->signalListOperationRunning(false);
    this->setEnabled(true);
    this->setFocus();
    mutex.unlock();
}

void PlaylistTableView::stopListOperation() {
    mutex.lock();
    if(listOperationThread->isRunning())
        listOperationWorker->stopWorker(true);
    if(m3uFileReaderThread->isRunning())
        m3uFileReader->stopWorker(true);
    if(m3uFileWriterThread->isRunning())
        m3uFileWriter->stopWorker(true);
    mutex.unlock();
}

int PlaylistTableView::getOperationProgress(){
    if(this->listOperationThread->isRunning())
        return this->listOperationWorker->getOperationProgress();
    if(this->m3uFileReaderThread->isRunning())
        return this->m3uFileReader->getOperationProgress();
    return 0;
}



void PlaylistTableView::addItems(QStringList items) {
    int start_row = this->model->rowCount();
    this->model->insertRows(start_row, items.length());
    QString item;
    foreach (item, items) {
        QFileInfo f(item);
        this->model->setData(model->index(start_row, ITEMTEXT), f.fileName());
        this->model->setData(model->index(start_row, ITEMPATH), item);
        this->model->setData(model->index(start_row, ITEMHASINFO), false);
        if(busyitem >= start_row){
            setBusyItemInPlaylist(busyitem+1);
        }
        start_row++;
    }
    this->updateItemsInfo();
    emit this->signalPlaylistRowNumChanged(false);
}

void PlaylistTableView::appendItemsToPlaylist(QString folderpath) {
    startListOperation(APPENDITEMS, folderpath);
}

void PlaylistTableView::removeSelectedItems() {
    this->startListOperation(REMOVEITEMS);
}

void PlaylistTableView::removeAllItems() {
    VectorList V;
    createViewModel(V);
    setBusyItemInPlaylist(-1);
    emit this->signalPlaylistTotalDurationChanged(getPlaylistTotalTime());
    //emit this->signalPlaylistRowNumChanged(true);
}

void PlaylistTableView::cropSelectedItems() {
    this->startListOperation(CROPITEMS);
}

void PlaylistTableView::removeMissingItems() {
    this->startListOperation(REMOVEMISSING);
}

void PlaylistTableView::removeDuplicateItems() {
    this->startListOperation(REMOVEDUPLICATES);
}

void PlaylistTableView::invertSelections() {
    int newduration;
    QVector<int> oldselections = cachedSelections;
    cachedSelections.clear();
    newduration = this->getPlaylistTotalTime() - cachedSelectionsDuration;
    this->clearSelection();
    for(int i = 0; i < numOfRows(); i++) {
        if(!oldselections.contains(i))
            cachedSelections.append(i);
    }
    updateItemsInfo();
    emit this->signalSelectedItemsDurationChanged(newduration);
    cachedSelectionsDuration = newduration;
}

void PlaylistTableView::selectNone() {
    this->clearSelection();
    this->cachedSelections.clear();
    updateItemsInfo();
}

void PlaylistTableView::reversePlaylist() {
    this->startListOperation(REVERSE);
}


void PlaylistTableView::randomizePlaylist() {
    this->startListOperation(RANDOMIZE);
}

void PlaylistTableView::sortPlaylistByTitle() {
    this->startListOperation(SORTBYTITLE);
}

void PlaylistTableView::sortPlaylistByFilename() {
    this->startListOperation(SORTBYFILENAME);
}

void PlaylistTableView::sortPlaylistByFilepath() {
    this->startListOperation(SORTBYFILEPATH);
}

void PlaylistTableView::playSelectedItem() {
    QString filepath;
    TrackInfo tif;
    int row = cachedSelections.last();
    this->setBusyItemInPlaylist(row);
    filepath = model->data(model->index(row, ITEMPATH)).toString();
    this->updateItemInfo(row, &tif);
    emit this->signalDoubleClickOnItem(filepath, tif);
}

QString PlaylistTableView::getItem(int itemrow, TrackInfo *tif) {
    QString filepath;
    filepath = model->data(model->index(itemrow, ITEMPATH)).toString();
    this->updateItemInfo(itemrow, tif);
    return filepath;
}

int PlaylistTableView::getPlaylistTotalTime() {
    int temp;
    bool ok;
    int totalsum = 0;
    for(int i = 0; i < model->rowCount(); i++) {
        temp = model->data(model->index(i, ITEMLENGTH), Qt::UserRole).toString().toInt(&ok);
        if(ok){
            totalsum += temp;
        }
    }
    return totalsum;
}

void PlaylistTableView::updateItemsInfo() {
    TrackInfo *tif = new TrackInfo();
    int row = this->rowAt(0);
    QModelIndex item = model->index(row, ITEMPATH);
    if(item.isValid()) {
        this->setPlaylistRowDelegate(row);
        if(!model->data(model->index(row, ITEMHASINFO)).toBool())
            this->updateItemInfo(row, tif);
    }
    row++;
    while(model->index(row, ITEMPATH).isValid()){
        if(this->viewport()->rect().contains(visualRect(model->index(row, ITEMTEXT)))) {
            this->setPlaylistRowDelegate(row);
            item = model->index(row, ITEMPATH);
            if(!model->data(model->index(row, ITEMHASINFO)).toBool())
                this->updateItemInfo(row, tif);
            row++;
        }
        else{
            if(!model->data(model->index(row, ITEMHASINFO)).toBool())
                this->updateItemInfo(row, tif);
            break;
        }
    }
    delete tif;
    emit this->signalPlaylistTotalDurationChanged(getPlaylistTotalTime());
}

void PlaylistTableView::updateUrlItemInfo(int row, TrackInfo *tif) {
    QModelIndex item = model->index(row, ITEMPATH);
    QString itemtext;
    if(row == this->getBusyItemFromPlaylist()) {
        tif->setShoutCast(true);
        itemtext = tif->getIcyField(ICY_CURRENT) + "..." + tif->getIcyField(ICY_NAME);
        if(itemtext == "...")
            itemtext = model->data(item).toString();
        model->setData(model->index(row, ITEMTEXT), itemtext);
        model->setData(model->index(row, ITEMHASINFO), true);
        model->setData(model->index(row, ITEMLENGTH), "0");
        this->urliteminfo->clearIcyFields();
        urliteminfo->setShoutCast(true);
        for(int i = 0; i < ICY_NUM_OF_FIELDS; ++i)
            this->urliteminfo->setIcyField(i, tif->getIcyField(i));
    }
    else {
        itemtext = model->data(item).toString();
        model->setData(model->index(row, ITEMTEXT), itemtext);
        model->setData(model->index(row, ITEMHASINFO), true);
        model->setData(model->index(row, ITEMLENGTH), "0");
    }
}

void PlaylistTableView::updateItemInfo(int row, TrackInfo *tif) {
    QModelIndex item = model->index(row, ITEMPATH);
    QString itemtext;
    //Case input is a URL
    if(global_methods::isURL(model->data(item).toString())) {
        this->updateUrlItemInfo(row, tif);
        return;
    }
    //Case input is a file
    *tif = TagHandler::readTagInfo(model->data(item).toString());
    itemtext = tif->getField(ARTIST) + " - " + tif->getField(TITLE);
    if(itemtext == " - ") {
        itemtext = model->data(item).toString();
        tif->setField(ARTIST, "Unknown");
        tif->setField(TITLE, "Unknown");
        tif->setField(ALBUM, "Unknown");
    }
    model->setData(model->index(item.row(), ITEMTEXT), itemtext);
    model->setData(model->index(item.row(), ITEMHASINFO), true);
    model->setData(model->index(item.row(), ITEMLENGTH), tif->getField(TRACK_LENGTH));
}

void PlaylistTableView::editPlaylistEntry() {
    EditPlaylistEntryDialog dlg(this->parentWidget());
    connect(&dlg, &EditPlaylistEntryDialog::signalItemEntryChanged, this, &PlaylistTableView::setItemEntry);
    for(int i = cachedSelections.size() - 1; i >= 0; i--) {
        TrackInfo *t = new TrackInfo();
        dlg.setRow(cachedSelections[i]);
        dlg.setOldEntry(model->data(model->index(cachedSelections[i], ITEMPATH)).toString());
        dlg.setNewEntry(model->data(model->index(cachedSelections[i], ITEMPATH)).toString());
        if(dlg.exec() == EditPlaylistEntryDialog::Rejected) break;
        updateItemInfo(cachedSelections[i], t);
    }
}

void PlaylistTableView::viewFileInfo(QVector<int> items) {
    if(items.isEmpty()) return;
    FileInfoDialog dlg(this->parentWidget());
    UrlInfoDialog urldlg(this->parentWidget());
    for(int i  = items.size() - 1; i >= 0; i--) {
        TrackInfo *t = new TrackInfo();
        if(!isURL(model->data(model->index(items[i], ITEMPATH)).toString())) {
            dlg.fillDialog(model->data(model->index(items[i], ITEMPATH)).toString());
            if(dlg.exec() == FileInfoDialog::Rejected) break;
            updateItemInfo(items[i], t);
        }
        else {
            if(items[i] == getBusyItemFromPlaylist()) {
                urldlg.setUrl(model->data(model->index(items[i], ITEMPATH)).toString());
                urldlg.setUrlInfo(*urliteminfo);
                urldlg.exec();
            }
        }
    }
}

void PlaylistTableView::viewSelectedItemsInfo() {
    this->viewFileInfo(cachedSelections);
}


void PlaylistTableView::setItemEntry(int row, QString entry) {
    if(row >= numOfRows())
        return;
    model->setData(model->index(row, ITEMPATH), entry);
    this->updateItemInfo(row, new TrackInfo());
}


void PlaylistTableView::showEvent(QShowEvent *event){
    Q_UNUSED(event);
    this->updateItemsInfo();
}


void PlaylistTableView::mouseDoubleClickEvent(QMouseEvent *event) {
    QString filepath;
    TrackInfo tif;
    QModelIndex index = this->indexAt(event->pos());
    if(!index.isValid())
        return;
    doubleclickflag = true; //Set flag to notify mouseRelease that a double click occured
    this->setBusyItemInPlaylist(index.row());
    filepath = model->data(model->index(index.row(), ITEMPATH)).toString();
    this->updateItemInfo(index.row(), &tif);
    emit this->signalDoubleClickOnItem(filepath, tif);
}


void PlaylistTableView::mousePressEvent(QMouseEvent * event) {
    QModelIndex index = this->indexAt(event->pos());
    if(!index.isValid()) {
        this->clearSelection();
        this->cachedSelections.clear();
        this->cachedSelectionsDuration = 0;
        updateItemsInfo();
        emit this->signalSelectedItemsDurationChanged(0);
        return;
    }
    if(event->buttons() & Qt::RightButton)
        rightclickflag = true;
    QItemSelection selection = this->selectionModel()->selection();
    //Set behavior when ctrl key is held down
    if(QApplication::keyboardModifiers() == Qt::ControlModifier) {
        //If row is already selected, then deselect it
        if(selection.contains(index)) {
            //First row, first column of selected item
            QModelIndex topleft = this->model->index(index.row(), 0);
            //First row, last column of selected item
            QModelIndex bottomright = this->model->index(index.row(), selection[0].bottomRight().column());
            QItemSelection deselect = QItemSelection(topleft, bottomright);
            this->selectionModel()->select(deselect, QItemSelectionModel::Deselect);
        }
        else
            this->selectPlaylistRow(index.row());
    }
    else {
        if(selection.contains(index) && QApplication::keyboardModifiers() != Qt::ShiftModifier)
            return;
        this->selectionModel()->clear();
        this->selectPlaylistRow(index.row());
    }
//I need to store in a private the last selected or deselected index
//This migth be useful for shift selections following control selections
}


void PlaylistTableView::mouseReleaseEvent(QMouseEvent *event) {
    QModelIndex index = this->indexAt(event->pos());
    if(!index.isValid())
        return;
    if(doubleclickflag){ //Following a doubleclick event. No need to do anything
        doubleclickflag = false;
        return;
    }
    //QItemSelection selection = this->selectionModel()->selection();
    if(QApplication::keyboardModifiers() == Qt::ShiftModifier ||
            QApplication::keyboardModifiers() == Qt::ControlModifier) {
        if(rightclickflag){
            rightclickflag = false;
            QPoint p(event->pos().x(), rowViewportPosition(index.row()) + 2*rowHeight(index.row()));
            emit this->customContextMenuRequested(p);
        }
        return;
    }
    else {
        if(rightclickflag){
            rightclickflag = false;
            QPoint p(event->pos().x(), rowViewportPosition(index.row()) + 2*rowHeight(index.row()));
            emit this->customContextMenuRequested(p);
            return;
        }
        this->selectionModel()->clear();
        this->selectPlaylistRow(index.row());
    }
}


void PlaylistTableView::dragEnterEvent(QDragEnterEvent *event) {
    event->accept();
}



void PlaylistTableView::mouseMoveEvent(QMouseEvent *event) {
    if(event->buttons() & Qt::LeftButton)
        this->startDrag(Qt::MoveAction);
}



void PlaylistTableView::startDrag(Qt::DropActions supportedActions) {
    QItemSelection dragged_items = this->selectionModel()->selection();
    if(dragged_items.isEmpty())
        return;
    int start_row = dragged_items.indexes()[0].row();
    dragged_row = start_row;
    QDrag *drag = new QDrag(this);
    QMimeData *mimedata = new QMimeData;
    mimedata->setData("text/plain", "");
    drag->setMimeData(mimedata);
    QPixmap pixmap;
    drag->setPixmap(pixmap);
    drag->exec(supportedActions);
}



void PlaylistTableView::dragMoveEvent(QDragMoveEvent *event) {
    QModelIndex index = this->indexAt(event->pos());
    if(index.isValid()){
        while(qAbs(this->dragged_row - index.row())) {
            if(dragged_row < index.row())
                dragged_row = swapRows(dragged_row, dragged_row+1);
            else
                dragged_row = swapRows(dragged_row, dragged_row-1);
        }
        this->setCurrentIndex(index);
        this->repaint();
    }
    event->accept();
}



void PlaylistTableView::dragLeaveEvent(QDragLeaveEvent *event) {
    Q_UNUSED(event);
    this->selectPlaylistRow(dragged_row);
    this->repaint();
}



void PlaylistTableView::dropEvent(QDropEvent *event) {
    event->accept();
    this->selectPlaylistRow(dragged_row);
    this->repaint();
}


int PlaylistTableView::numOfRows() {
    return this->model->rowCount();
}

bool PlaylistTableView::isBusy() {
    mutex.lock();
    if(this->m3uFileReaderThread->isRunning() || this->m3uFileWriterThread->isRunning()
            || listOperationThread->isRunning()) {
        mutex.unlock();
        return true;
    }
    mutex.unlock();
    return false;
}

void PlaylistTableView::setupTableView() {
    this->hideColumn(ITEMPATH);
    this->hideColumn(ITEMHASINFO);
    this->verticalHeader()->hide();
    this->setShowGrid(false);
    this->setWordWrap(false);
    this->resizeColumnsToContents();
    this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    //drag n drop
    //this->setSelectionMode(this->ContiguousSelection);
    this->setDragEnabled(true);
    this->setAcceptDrops(true);
    this->setDragDropMode(this->InternalMove);
    this->setDropIndicatorShown(false);
    //SEt heigth of items
    this->verticalHeader()->setDefaultSectionSize(this->verticalHeader()->fontMetrics().height() + 2);
    //Set stretching of first column in order to fill the whole parent widget, else sometimes there's
    //blank space after 'length'
    this->horizontalHeader()->setSectionResizeMode(ITEMTEXT, QHeaderView::Stretch);
}
