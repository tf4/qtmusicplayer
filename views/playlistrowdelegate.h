#ifndef PLAYLISTROWDELEGATE_H
#define PLAYLISTROWDELEGATE_H

#include <QItemDelegate>

class PlaylistRowDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit PlaylistRowDelegate(QWidget *parent = 0);
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;

signals:

public slots:

};

#endif // PLAYLISTROWDELEGATE_H
