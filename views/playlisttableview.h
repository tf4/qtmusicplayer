#ifndef PLAYLISTTABLEVIEW_H
#define PLAYLISTTABLEVIEW_H

#include "playlistmodel.h"
#include "playlistreaderworker.h"
#include "playlistwriterworker.h"
#include "playlistrowdelegate.h"
#include "playlistselectedrowdelegate.h"
#include "playlistselectedbusyrowdelegate.h"
#include "trackinfo.h"
#include <QTableView>
#include <QThread>
#include <QQueue>
#include <QMutex>
#include <QMouseEvent>
#include <QShowEvent>
#include "customtypes.h"
#include "listoperationsworker.h"

class PlaylistTableView : public QTableView
{
    Q_OBJECT
public:
    explicit PlaylistTableView(QWidget *parent = 0);
    void setupTableView();
    int numOfRows();
    bool isBusy();
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);
    void startDrag(Qt::DropActions supportedActions);
    void showEvent(QShowEvent * event);  


private:
    QAbstractTableModel *model;
    QThread *m3uFileReaderThread;
    QThread *m3uFileWriterThread;
    PlaylistReaderWorker *m3uFileReader;
    PlaylistWriterWorker *m3uFileWriter;
    PlaylistRowDelegate *busyItemRowDelegate;
    PlaylistSelectedRowDelegate *selectedItemRowDelegate;
    PlaylistSelectedBusyRowDelegate *selectedBusyItemRowDelegate;
    QQueue<QString> *playlistReadQueue;
    QMutex mutex;
    bool mousereleasedflag;
    int busyitem;
    int dragged_row;
    QItemSelectionModel *selection_model;
    QThread *listOperationThread;
    ListOperationsWorker *listOperationWorker;
    bool doubleclickflag;
    bool rightclickflag;
    QVector<int> cachedSelections;
    int cachedSelectionsDuration;
    bool cachedSelectionsDurationFlag;
    int cachedSelectionsMin;
    int cachedSelectionsMax;
    TrackInfo *urliteminfo;


signals:
    //void signalModelData(QList<QVector<QString> > v);
    void signalPlaylistTotalDurationChanged(int);
    void signalSelectedItemsDurationChanged(int);
    void signalDoubleClickOnItem(QString, TrackInfo);
    void signalPlaylistRowNumChanged(bool);
    void signalListOperationRunning(bool);
    void signalPlaylistSavedSuccessfully(bool);

public slots:
    void createViewModel(VectorList m);
    void updateViewModel(bool success, VectorList um, QVector<int> swaps, int newbusyitem, int newduration);
    void readM3UList(QString listpath, bool isFolder=false);
    void readM3UFinished();
    void writeM3UList(QString listpath);
    void writeM3UFinished();
    void saveM3UDone(bool success);
    void selectPlaylistRow(int row);
    void selectionChangedSlot(const QItemSelection &selected, const QItemSelection &deselected);
    void setPlaylistRowDelegate(int row);
    void setBusyItemInPlaylist(int item);
    int getBusyItemFromPlaylist();
    int swapRows(int start, int target);
    void startListOperation(OperationTypes type, QString foldertoappend = QString());
    int getOperationProgress();
    void stopListOperation();
    void listOperationFinished();
    void invertSelections();
    void selectNone();
    void addItems(QStringList items);
    void removeSelectedItems();
    void removeAllItems();
    void cropSelectedItems();
    void removeMissingItems();
    void removeDuplicateItems();
    void reversePlaylist();
    void randomizePlaylist();
    void sortPlaylistByTitle();
    void sortPlaylistByFilename();
    void sortPlaylistByFilepath();
    void appendItemsToPlaylist(QString folderpath);
    void updateItemsInfo();
    void updateUrlItemInfo(int row, TrackInfo *tif);
    void updateItemInfo(int row, TrackInfo *tif);
    void playSelectedItem();
    QString getItem(int itemrow, TrackInfo *tif);
    int getPlaylistTotalTime();   
    void editPlaylistEntry();
    void setItemEntry(int row, QString entry);
    void viewFileInfo(QVector<int> items);
    void viewSelectedItemsInfo();

};

#endif // PLAYLISTTABLEVIEW_H
