#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QVector>
#include "customtypes.h"



class PlaylistModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    //explicit PlaylistModel(QObject *parent = 0);
    PlaylistModel(const QList<QVector<QString> > &input, QObject *parent=0)
        :QAbstractTableModel(parent), datalist(input){}
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    bool insertRows(int row, int count, const QModelIndex & parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex & parent = QModelIndex());
    bool moveRows(const QModelIndex &sourceParent, int sourceRow, int s,
                  const QModelIndex &destinationParent, int destinationChild);
    Qt::DropActions supportedDragActions() const;
    Qt::ItemFlags flags(const QModelIndex & index) const;
    VectorList getModelRawData();

private:
    //QList<QVector<QString> > datalist;
    VectorList datalist;

signals:

public slots:

};

#endif // PLAYLISTMODEL_H
