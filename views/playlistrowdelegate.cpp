#include "playlistrowdelegate.h"
#include <QPainter>
#include "customtypes.h"

PlaylistRowDelegate::PlaylistRowDelegate(QWidget *parent) :
    QItemDelegate(parent)
{
}

void PlaylistRowDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const {
    painter->save();
    painter->setPen(QColor(Qt::blue));
    QRect rect = option.rect;
    QVariant textqvar = index.model()->data(index, Qt::DisplayRole);
    QString text = textqvar.toString();
    QFontMetrics fm = QFontMetrics(option.font);
    int textwidth = fm.width(text, text.length());
    int toff = 3;
    text = fm.elidedText(text, Qt::ElideRight, rect.width(), 0);
    if(index.column() == ITEMTEXT) {
        painter->drawText(rect.x()+toff, rect.y(), rect.width(), rect.height(),
                           Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap, text);
    }
    else {
        painter->drawText(rect.x()+rect.width() - textwidth -toff, rect.y(),textwidth,
                           rect.height(), Qt::AlignVCenter | Qt::TextWordWrap, text);
    }
    painter->translate(rect.x(), rect.y());
    painter->restore();
}


