#ifndef DEFAULTS_H
#define DEFAULTS_H

#define DEF_VOLUME  11
#define DEF_VOLUMESTEP  5
#define DEF_MAIN_WIDTH  341
#define DEF_MAIN_HEIGHT 215
#define DEF_PLS_WIDTH   351
#define DEF_PLS_HEIGHT  215
#define DEF_MAIN_MINWIDTH   275
#define DEF_MAIN_MINHEIGHT  172
#define DEF_MAIN_MAXWIDTH   1030
#define DEF_MAIN_MAXHEIGHT  420

const char DEF_PLAYLIST[] = "\\playlist.m3u";
const char DEF_INIFILE[] = "\\inifile.ini";
const char DEF_TEMPFILE[] = "\\tempfile";
const char PLAYBACK[] = "Playback";
const char WINDOWS[] = "Windows";
const char DIMENSIONS[] = "Dimensions";
const char LAST_TRACK[] = "last_track";
const char LAST_TRACK_LINE[] = "last_track_line";
const char REPEAT[] = "repeat";
const char SHUFFLE[] = "shuffle";
const char VOLUME[] = "volume";
const char MAIN_GEOMETRY[] = "main_geometry";
const char PLS_GEOMETRY[] = "pls_geometry";
const char PLAYLIST[] = "playlist";
const char PLS_DOCKED[] = "pl_docked";
const char EQUALIZER[] = "equalizer";

#endif // DEFAULTS_H
