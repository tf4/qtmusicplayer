#ifndef MUTETOOLBUTTON_H
#define MUTETOOLBUTTON_H

#include <QToolButton>

class MuteToolButton : public QToolButton
{
    Q_OBJECT
public:
    explicit MuteToolButton(QWidget *parent = 0);
    void setState(bool state);
    bool getState();

private:
    bool state;

signals:

public slots:

};

#endif // MUTETOOLBUTTON_H
