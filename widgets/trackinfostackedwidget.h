#ifndef TRACKINFOSTACKEDWIDGET_H
#define TRACKINFOSTACKEDWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QStackedWidget>
#include <QMutex>
#include "trackinfolabel.h"

class TrackInfoStackedWidget : public QStackedWidget
{
    Q_OBJECT
public:
    explicit TrackInfoStackedWidget(QWidget *parent = 0);
    void setTimerInterval(int interval);
    int getTimerInterval();
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void resizeEvent(QResizeEvent *);

private:
    int timerInterval;
    int crawlTimerInterval;
    int crawlPauseInterval;
    QTimer *pagetimer;
    QTimer *crawlLabelTimer;
    QMutex *mutex;
    TrackInfoLabel *label;
    bool movingLeft;

signals:

public slots:
    void rotatePages();
    void crawlContent();

};

#endif // TRACKINFOSTACKEDWIDGET_H
