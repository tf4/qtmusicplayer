#include "shuffletoolbutton.h"

ShuffleToolButton::ShuffleToolButton(QWidget *parent) :
    QToolButton(parent),
    state(false)
{
    this->setIcon(QPixmap(":/icons/noshuffle.png"));
    this->setIconSize(QSize(16,16));
}

void ShuffleToolButton::setState(bool state) {
    this->state = state;
    if(state) {
        this->setIcon(QPixmap(":/icons/mediashuffle.png"));
        this->setIconSize(QSize(16,16));
    }
    else {
        this->setIcon(QPixmap(":/icons/noshuffle.png"));
        this->setIconSize(QSize(16,16));
    }
}

bool ShuffleToolButton::getState() {return state;}
