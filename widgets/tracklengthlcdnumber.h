#ifndef TRACKLENGTHLCDNUMBER_H
#define TRACKLENGTHLCDNUMBER_H

#include <QLCDNumber>

class TrackLengthLCDNumber : public QLCDNumber
{
    Q_OBJECT
public:
    explicit TrackLengthLCDNumber(QWidget *parent = 0);
    void setLength(int value);
    void toggleReverse();
    void setDisplay(int value);
    void mousePressEvent(QMouseEvent *);

private:
    int length;
    int pos;
    bool reversed;

signals:

public slots:

};

#endif // TRACKLENGTHLCDNUMBER_H
