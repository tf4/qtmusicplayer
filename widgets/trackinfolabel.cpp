#include "trackinfolabel.h"
#include <QDebug>

enum chunkidx {PRETITLE=0, CONTENTNAME=1, PRECOLOR=2, COLOR=3, PRECONTENT=4,
              CONTENT=5, END=6, NUM_OF_CHUUNKS=7};

TrackInfoLabel::TrackInfoLabel(QWidget *parent) :
    QLabel(parent),
    align(""), color("aa007f"), contentName(""), content("")
{
    this->setFont(QFont(this->fontInfo().family(),this->fontInfo().pointSize(), QFont::Bold));
    initChunks();
    this->setAlignment(Qt::AlignCenter);
}


void TrackInfoLabel::setContentName(QString tagname) {
    this->contentName = tagname;
    chunks[CONTENTNAME] = contentName;
    this->displayHtml();
    this->contentStack.clear();
    this->contentNameStack.clear();
}

void TrackInfoLabel::setContent(QString text) {
    content = text;
    chunks[CONTENT] = text;   
    this->displayHtml();
    this->contentStack.clear();
    this->contentNameStack.clear();
}

void TrackInfoLabel::resetAllContent() {
    contentNameStack.clear();
    contentStack.clear();
    chunks[CONTENTNAME] = contentName;
    chunks[CONTENT] = content;
    this->displayHtml();
}

bool TrackInfoLabel::crawlLeftOneStep() {
    if(fontMetrics().width(chunks[CONTENTNAME]+chunks[CONTENT]) <= this->width()) return false;
    if(!chunks[CONTENTNAME].isEmpty()) {
        contentNameStack.append(chunks[CONTENTNAME][0]);
        chunks[CONTENTNAME].remove(0, 1);
        this->displayHtml();
        return true;
    }
    else {
        if(!chunks[CONTENT].isEmpty()) {
            contentStack.append(chunks[CONTENT][0]);
            chunks[CONTENT].remove(0, 1);
            this->displayHtml();
            return true;
        }
        else
            return false;
    }
}

bool TrackInfoLabel::crawlBackOneStep() {
    if(!contentStack.isEmpty()) {
        chunks[CONTENT].prepend(contentStack.takeLast());
        this->displayHtml();
        return true;
    }
    else {
        if(!contentNameStack.isEmpty()) {
            chunks[CONTENTNAME].prepend(contentNameStack.takeLast());
            this->displayHtml();
            return true;
        }
        else
            return false;
    }
}


void TrackInfoLabel::setTextAlignment(QString align) {
    return;
    if(align == "center" || align == "right" || align == "left") {
        this->align = align;
        /*chunks[ALIGN] = align;
        displayHtml();*/
    }
}

void TrackInfoLabel::setTextColor(QString col) {
    this->color = col;
    chunks[COLOR] = color;
    this->displayHtml();

}

void TrackInfoLabel::initChunks() {
    chunks.append("<html><head/><body><p><span >");
    chunks.append(contentName);
    chunks.append("</span><span style=\"  color:#");
    chunks.append(color);
    chunks.append(";\">");
    chunks.append(content);
    chunks.append("</span></p></body></html>");
}

void TrackInfoLabel::displayHtml() {
    QString newtext;
    for(int i = 0; i < NUM_OF_CHUUNKS; ++i)
        newtext += chunks[i];
    this->setText(newtext);
}

QString TrackInfoLabel::getContentName() {return contentName;}

QString TrackInfoLabel::getContent() {return content;}

int TrackInfoLabel::getTextTotalSize() {
    int leftmargin;
    int rightmargin;
    this->getContentsMargins(&leftmargin, NULL, &rightmargin, NULL);
    return this->fontMetrics().width(content + contentName) + leftmargin + rightmargin;
}

void TrackInfoLabel::mouseReleaseEvent(QMouseEvent *ev) {
    ev->ignore();
}


