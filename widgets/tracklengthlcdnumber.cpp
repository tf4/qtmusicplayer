#include "tracklengthlcdnumber.h"
#include <QDebug>

TrackLengthLCDNumber::TrackLengthLCDNumber(QWidget *parent) :
    QLCDNumber(parent)
{
    this->length = 0;
    this->pos = 0;
    this->reversed = false;
}

void TrackLengthLCDNumber::setLength(int value) {
    this->length = value;
}

void TrackLengthLCDNumber::toggleReverse() {
    this->reversed = !this->reversed;
}

void TrackLengthLCDNumber::mousePressEvent(QMouseEvent *) {
    this->toggleReverse();
}

void TrackLengthLCDNumber::setDisplay(int value) {
    int minutes, seconds;
    QString toshow = "";
    this->pos = value;
    if(this->reversed)
        this->pos = this->length - this->pos;
    if(this->pos == 0) {
        this->display("00:00");
        return;
    }
    minutes = this->pos / 60;
    if(minutes > 99) {
        toshow += "99";
    }
    else if(minutes < 10){
       toshow += "0" + QString::number(minutes);
    }
    else
        toshow += QString::number(minutes);
    toshow += ":";
    seconds = this->pos % 60;
    if(seconds > 9){
        toshow += QString::number(seconds);
    }
    else
        toshow += "0" + QString::number(seconds);
    this->display(toshow);
}
