#include "mutetoolbutton.h"

MuteToolButton::MuteToolButton(QWidget *parent) :
    QToolButton(parent),
    state(false)
{
    this->setIcon(QPixmap(":/icons/unmute.png"));
    this->setIconSize(QSize(16,16));
}

void MuteToolButton::setState(bool state) {
    this->state = state;
    if(state) {
        this->setIcon(QPixmap(":/icons/mute.png"));
        this->setIconSize(QSize(16,16));
    }
    else {
        this->setIcon(QPixmap(":/icons/unmute.png"));
        this->setIconSize(QSize(16,16));
    }
}

bool MuteToolButton::getState() {return state;}

