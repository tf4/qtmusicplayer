#ifndef SHUFFLETOOLBUTTON_H
#define SHUFFLETOOLBUTTON_H

#include <QToolButton>

class ShuffleToolButton : public QToolButton
{
    Q_OBJECT
public:
    explicit ShuffleToolButton(QWidget *parent = 0);
    void setState(bool state);
    bool getState();

private:
    bool state;

signals:

public slots:

};

#endif // SHUFFLETOOLBUTTON_H
