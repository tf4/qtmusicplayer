#include "trackinfostackedwidget.h"
#include <QDebug>
#include <QTimer>
#include <QLabel>
#include <QMouseEvent>
#include <QtGlobal>

TrackInfoStackedWidget::TrackInfoStackedWidget(QWidget *parent) :
    QStackedWidget(parent),
    timerInterval(6000), crawlTimerInterval(150), crawlPauseInterval(1000)
{
    this->pagetimer = new QTimer();
    connect(this->pagetimer, &QTimer::timeout, this, &TrackInfoStackedWidget::rotatePages);
    this->pagetimer->start(this->timerInterval);
    this->crawlLabelTimer = new QTimer();
    connect(this->crawlLabelTimer, &QTimer::timeout, this, &TrackInfoStackedWidget::crawlContent);
    mutex = new QMutex();
    label = new TrackInfoLabel();
    this->installEventFilter(this);
}

void TrackInfoStackedWidget::rotatePages() {
    this->crawlLabelTimer->stop();
    mutex->lock();
    if(this->currentIndex() < this->count() - 1)
        this->setCurrentIndex(this->currentIndex() + 1);
    else
        this->setCurrentIndex(0);

    if(this->children().last()->objectName().contains("Page")){
        label = this->children().last()->findChild<TrackInfoLabel*>();
        label->resetAllContent();
        movingLeft = true;
        if(label->getTextTotalSize() > label->width()) {
            this->crawlLabelTimer->start(crawlPauseInterval);
        }
    }
    mutex->unlock();
}

void TrackInfoStackedWidget::crawlContent() {
    mutex->lock();
    if(movingLeft) {
        movingLeft = label->crawlLeftOneStep();
        if(!movingLeft) crawlLabelTimer->start(crawlPauseInterval);
        else crawlLabelTimer->start(crawlTimerInterval);
    }
    else {
        movingLeft = !label->crawlBackOneStep();
        if(movingLeft) crawlLabelTimer->start(crawlPauseInterval);
        else crawlLabelTimer->start(crawlTimerInterval);
    }
    mutex->unlock();
}


void TrackInfoStackedWidget::setTimerInterval(int interval) {
    this->timerInterval = interval;
}

int TrackInfoStackedWidget::getTimerInterval() {
    return this->timerInterval;
}

void TrackInfoStackedWidget::mousePressEvent(QMouseEvent *ev) {
    this->pagetimer->stop();
    ev->accept();
}

void TrackInfoStackedWidget::mouseReleaseEvent(QMouseEvent *) {
    this->rotatePages();
    this->pagetimer->start();
    qDebug() << "release";
}

void TrackInfoStackedWidget::mouseMoveEvent(QMouseEvent *){
    qDebug() << "mouse move";
}

void TrackInfoStackedWidget::resizeEvent(QResizeEvent *) {
    QList<TrackInfoLabel*> allLabels = this->findChildren<TrackInfoLabel*>();
    crawlLabelTimer->stop();
    pagetimer->stop();
    for(int i = 0; i < allLabels.size(); ++i) {
        allLabels[i]->resetAllContent();
    }
    pagetimer->start(timerInterval);
}

