#ifndef BUTTONS_H
#define BUTTONS_H

#include <QToolButton>

enum RepeatStates {
    NO_REPEAT = 0,
    REPEAT_ONE = 1,
    REPEAT_ALL = 2
};


class RepeatToolButton : public QToolButton
{
    Q_OBJECT
public:
    explicit RepeatToolButton(QWidget *parent = 0);
    int getState();
    void setState(int s);

private:
    RepeatStates state;

signals:

public slots:
    void virtualClick();

};

#endif // BUTTONS_H
