#include "toolbuttonleftcontextmenu.h"
#include <QMouseEvent>
#include <QDebug>

ToolButtonLeftContextMenu::ToolButtonLeftContextMenu(QWidget *parent) :
    QToolButton(parent)
{

}

void ToolButtonLeftContextMenu::mousePressEvent(QMouseEvent *e) {
    QPoint point;
    if(e->buttons() & Qt::LeftButton) {
        point = this->rect().bottomLeft();
        emit customContextMenuRequested(point);
    }
}
