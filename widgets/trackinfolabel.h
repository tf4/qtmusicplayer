#ifndef TRACKINFOLABEL_H
#define TRACKINFOLABEL_H

#include <QLabel>
#include <QTimer>
#include <QHash>
#include <QVector>
#include <QMouseEvent>

class TrackInfoLabel : public QLabel
{
    Q_OBJECT
public:
    explicit TrackInfoLabel(QWidget *parent = 0);
    void setContentName(QString tagname);
    void setContent(QString text);
    void setTextColor(QString col);
    void setTextAlignment(QString align);
    QString getContentName();
    QString getContent();
    int getTextTotalSize();
    void setMovingText(QString contentName, QString text);
    bool crawlLeftOneStep();
    bool crawlBackOneStep();
    void resetAllContent();
    void mouseReleaseEvent(QMouseEvent *ev);


private:
    QString align;
    QString color;
    QString contentName;
    QString content;
    QVector<QString> chunks;
    QVector<QChar> contentNameStack;
    QVector<QChar> contentStack;
    void initChunks();
    void displayHtml();

signals:

public slots:

};

#endif // TRACKINFOLABEL_H
