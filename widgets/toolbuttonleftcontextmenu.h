#ifndef TOOLBUTTONLEFTCONTEXTMENU_H
#define TOOLBUTTONLEFTCONTEXTMENU_H

#include <QToolButton>
#include <QMenu>

class ToolButtonLeftContextMenu : public QToolButton
{
    Q_OBJECT
public:
    explicit ToolButtonLeftContextMenu(QWidget *parent = 0);
    void mousePressEvent(QMouseEvent *);




signals:

public slots:

};

#endif // TOOLBUTTONLEFTCONTEXTMENU_H
