#include "repeattoolbutton.h"
#include <QDebug>

RepeatToolButton::RepeatToolButton(QWidget *parent) :
    QToolButton(parent)
{
    state = NO_REPEAT;
}

void RepeatToolButton::setState(int s) {
    switch (s) {
    case NO_REPEAT:
        state = NO_REPEAT;
        setToolTip("Repeat off");
        setChecked(false);
        this->setIcon(QPixmap(":/icons/norepeat.png"));
        this->setIconSize(QSize(16,16));
        break;
    case REPEAT_ONE:
        state = REPEAT_ONE;
        setToolTip("Repeat one");
        //setChecked(true);
        this->setIcon(QPixmap(":/icons/repeatone.png"));
        this->setIconSize(QSize(16,16));
        break;
    case REPEAT_ALL:
        state = REPEAT_ALL;
        setToolTip("Repeat all");
        this->setIcon(QPixmap(":/icons/repeatall.png"));
        this->setIconSize(QSize(16,16));
        //setChecked(true);
        break;
    default:
        break;
    }
}

int RepeatToolButton::getState() {
    return state;
}

void RepeatToolButton::virtualClick() {
    qDebug() << "repeat btn clicked";
    switch (state) {
    case NO_REPEAT:
        setState(REPEAT_ONE);
        break;
    case REPEAT_ONE:
        setState(REPEAT_ALL);
        break;
    case REPEAT_ALL:
        setState(NO_REPEAT);
        break;
    default:
        break;
    }
}
