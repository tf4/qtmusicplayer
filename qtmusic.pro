#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T22:20:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtmusic
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        utils/taghandler.cpp \
        widgets/repeattoolbutton.cpp \
        widgets/trackinfostackedwidget.cpp\
        trackinfo.cpp \
        bassaudio.cpp \
    widgets/trackinfolabel.cpp \
    widgets/tracklengthlcdnumber.cpp \
    widgets/toolbuttonleftcontextmenu.cpp \
    playlistdialog.cpp \
    views/playlisttableview.cpp \
    views/playlistmodel.cpp \
    workers/playlistreaderworker.cpp \
    views/playlistrowdelegate.cpp \
    workers/listoperationsworker.cpp \
    views/playlistselectedrowdelegate.cpp \
    views/playlistselectedbusyrowdelegate.cpp \
    workers/playlistwriterworker.cpp \
    editplaylistentrydialog.cpp \
    fileinfodialog.cpp \
    urldialog.cpp \
    customtypes.cpp \
    urlinfodialog.cpp \
    widgets/mutetoolbutton.cpp \
    widgets/shuffletoolbutton.cpp \
    utils/dockdialogmanager.cpp

HEADERS  += mainwindow.h \
    bass.h \
    bassmix.h \
    utils/taghandler.h \
    widgets/repeattoolbutton.h \
    widgets/trackinfostackedwidget.h\
    trackinfo.h \
    bassaudio.h \
    widgets/trackinfolabel.h \
    widgets/tracklengthlcdnumber.h \
    widgets/toolbuttonleftcontextmenu.h \
    playlistdialog.h \
    views/playlisttableview.h \
    views/playlistmodel.h \
    workers/playlistreaderworker.h \
    customtypes.h \
    views/playlistrowdelegate.h \
    defaults.h \
    workers/listoperationsworker.h \
    views/playlistselectedrowdelegate.h \
    views/playlistselectedbusyrowdelegate.h \
    workers/playlistwriterworker.h \
    editplaylistentrydialog.h \
    fileinfodialog.h \
    urldialog.h \
    urlinfodialog.h \
    widgets/mutetoolbutton.h \
    widgets/shuffletoolbutton.h \
    utils/dockdialogmanager.h
FORMS    += mainwindow.ui \
            playlistdialog.ui \
    editplaylistentrydialog.ui \
    fileinfodialog.ui \
    urldialog.ui \
    urlinfodialog.ui

INCLUDEPATH += $$PWD/widgets
DEPENDPATH += $$PWD/widgets

INCLUDEPATH += $$PWD/views
DEPENDPATH += $$PWD/views

INCLUDEPATH += $$PWD/workers
DEPENDPATH += $$PWD/workers

INCLUDEPATH += $$PWD/
DEPENDPATH += $$PWD/


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/ -lbass
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/ -lbass
else:unix: LIBS += -L$$PWD/ -lbass

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/ -lbassmix
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/ -lbassmix
else:unix: LIBS += -L$$PWD/ -lbassmix

INCLUDEPATH += $$PWD/
DEPENDPATH += $$PWD/

RESOURCES = icons.qrc

OTHER_FILES += \
    customwidgets.pri


win32: LIBS += -L$$PWD/ -ltaglib
INCLUDEPATH += $$PWD/../../../../../taglib-1.9.1/taglib
DEPENDPATH += $$PWD/../../../../../taglib-1.9.1/taglib

INCLUDEPATH += $$PWD/taglib-1.9.1
DEPENDPATH += $$PWD/taglib-1.9.1

HEADERS += $$PWD/taglib-1.9.1/taglib/ape/apefile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ape/apefooter.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ape/apeitem.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ape/apeproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ape/apetag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/asf/asfattribute.h
HEADERS += $$PWD/taglib-1.9.1/taglib/asf/asffile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/asf/asfpicture.h
HEADERS += $$PWD/taglib-1.9.1/taglib/asf/asfproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/asf/asftag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/audioproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/fileref.h
HEADERS += $$PWD/taglib-1.9.1/taglib/flac/flacfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/flac/flacmetadatablock.h
HEADERS += $$PWD/taglib-1.9.1/taglib/flac/flacpicture.h
HEADERS += $$PWD/taglib-1.9.1/taglib/flac/flacproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/flac/flacunknownmetadatablock.h
HEADERS += $$PWD/taglib-1.9.1/taglib/it/itfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/it/itproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mod/modfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mod/modfilebase.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mod/modfileprivate.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mod/modproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mod/modtag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mp4/mp4atom.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mp4/mp4coverart.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mp4/mp4file.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mp4/mp4item.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mp4/mp4properties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mp4/mp4tag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpc/mpcfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpc/mpcproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v1/id3v1genres.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v1/id3v1tag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/attachedpictureframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/commentsframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/generalencapsulatedobjectframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/ownershipframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/popularimeterframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/privateframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/relativevolumeframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/textidentificationframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/uniquefileidentifierframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/unknownframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/unsynchronizedlyricsframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/frames/urllinkframe.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2extendedheader.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2footer.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2frame.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2framefactory.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2header.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2synchdata.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/id3v2/id3v2tag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/mpegfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/mpegheader.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/mpegproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/mpeg/xingheader.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/flac/oggflacfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/oggfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/oggpage.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/oggpageheader.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/opus/opusfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/opus/opusproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/speex/speexfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/speex/speexproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/vorbis/vorbisfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/vorbis/vorbisproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/ogg/xiphcomment.h
HEADERS += $$PWD/taglib-1.9.1/taglib/riff/aiff/aifffile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/riff/aiff/aiffproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/riff/rifffile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/riff/wav/infotag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/riff/wav/wavfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/riff/wav/wavproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/s3m/s3mfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/s3m/s3mproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/tag.h
HEADERS += $$PWD/taglib-1.9.1/taglib/taglib_export.h
HEADERS += $$PWD/taglib-1.9.1/taglib/tagunion.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/taglib.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tbytevector.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tbytevectorlist.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tbytevectorstream.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tdebug.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tdebuglistener.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tfilestream.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tiostream.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tlist.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tmap.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tpropertymap.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/trefcounter.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tstring.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tstringlist.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/tutils.h
HEADERS += $$PWD/taglib-1.9.1/taglib/toolkit/unicode.h
HEADERS += $$PWD/taglib-1.9.1/taglib/trueaudio/trueaudiofile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/trueaudio/trueaudioproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/wavpack/wavpackfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/wavpack/wavpackproperties.h
HEADERS += $$PWD/taglib-1.9.1/taglib/xm/xmfile.h
HEADERS += $$PWD/taglib-1.9.1/taglib/xm/xmproperties.h

HEADERS += $$PWD/taglib-1.9.1/taglib_config.h

Release: DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
#win32: LIBS += -L$$PWD/../../../../../Libraries/taglib/lib/ -ltaglib

#INCLUDEPATH += $$PWD/../../../../../Libraries/taglib/include
#DEPENDPATH += $$PWD/../../../../../Libraries/taglib/include
