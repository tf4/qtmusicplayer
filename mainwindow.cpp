#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "bassaudio.h"
#include "trackinfo.h"
#include "utils/taghandler.h"
#include <QtGlobal>
#include <QDebug>
#include <QFileDialog>
#include <QMouseEvent>
#include <iostream>
#include <QSettings>
#include <QMessageBox>
#include <tag.h>
#include <fileref.h>
#include <mpeg/mpegfile.h>
#include <mpeg/id3v2/id3v2frame.h>
#include <mpeg/id3v2/id3v2header.h>
#include <mpeg/id3v2/id3v2tag.h>
#include <toolkit/tbytevector.h>
#include "defaults.h"

using namespace TagLib;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    trackSliderGroovePressed(false)
{
    ui->setupUi(this);
    this->setupLabelTags();
    execPath = QCoreApplication::applicationDirPath();
    //Initialize playback toolbuttons control state machine
    static playbackstate _playback_fsm[] = {
        {{true, true, false, false, false}, {STOP_ST, PLAY_ST, STOP_ST, STOP_ST, FIN_ST}},
        {{true, false, true, true, false}, {STOP_ST, PLAY_ST, PLAY_ST, PAUSE_ST, FIN_ST}},
        {{true, false, false, true, true}, {STOP_ST, PLAY_ST, PLAY_ST, PLAY_ST, FIN_ST}},
        {{true, false, false, false, false}, {STOP_ST, PLAY_ST, PLAY_ST, FIN_ST, FIN_ST}}
    };
    playback_fsm = _playback_fsm;
    this->currentPlaybackState = playback_fsm[STOP_ST];
    executePlaybackFsm(STOP_TRS);
    setMinimumSize(DEF_MAIN_MINWIDTH, DEF_MAIN_MINHEIGHT);
    setMaximumSize(DEF_MAIN_MAXWIDTH, DEF_MAIN_MAXHEIGHT);
    audio = new BassAudio(50);
    //Connect toolbuttons
    connect(ui->playToolButton, &QToolButton::clicked, this, &MainWindow::playCurrentTrack);
    connect(ui->stopToolButton, &QToolButton::clicked, this, &MainWindow::stopCurrentTrack);
    connect(ui->pauseToolButton, &QToolButton::clicked, this, &MainWindow::pauseCurrentTrack);
    connect(ui->repeatToolButton, &QToolButton::clicked, ui->repeatToolButton, &RepeatToolButton::virtualClick);
    connect(ui->ejectToolButton, &QToolButton::clicked, this, &MainWindow::openFiles);
    connect(audio, &BassAudio::signalStreamEnded, this, &MainWindow::trackFinished, Qt::QueuedConnection);
    connect(audio, &BassAudio::signalTrackPosition, this, &MainWindow::updateTrackPositionDisplay);
    connect(audio, &BassAudio::signalMetaUrlUpdate, this, &MainWindow::receivedUrlMetaInfo);
    connect(ui->trackLengthSlider, &QSlider::sliderReleased, this, &MainWindow::seekTrackPosition);
    connect(ui->trackLengthSlider, &QSlider::sliderMoved, this, &MainWindow::seekTime);
    connect(ui->trackLengthSlider, &QSlider::sliderPressed, this, &MainWindow::handleSliderPress);
    connect(ui->volumeSlider, &QSlider::valueChanged, this, &MainWindow::volumeSlide);
    connect(ui->muteToolButton, &QToolButton::clicked, this, &MainWindow::toggleVolumeMute);
    this->readConfigIniFile();
    this->setTrackDisplayInfo(TagHandler::readTagInfo(this->getCurrentTrack()));
    this->initializeTrackPositionDisplay(0);
    ui->trackLengthSlider->installEventFilter(this);
    ui->volumeSlider->installEventFilter(this);
    //qSleep(4000);
    this->show();
    //Initialize playlist dialog
    playlistdialog = new PlaylistDialog(this);
    qRegisterMetaType<TrackInfo>("TrackInfo");
    //Connect toolbuttons that depend on playlist dialog
    connect(playlistdialog, &PlaylistDialog::signalPlaylistItemDoubleClicked, this, &MainWindow::playlistDoubleClicked,
            Qt::QueuedConnection); //without queued connection in some cases we get stack overflow
    connect(playlistdialog, &PlaylistDialog::signalFetchedNewItem, this, &MainWindow::receivedNextTrack,
            Qt::QueuedConnection);
    connect(ui->fwdToolButton_22, &QToolButton::clicked, this, &MainWindow::playNextTrack);
    connect(ui->rwdToolButton, &QToolButton::clicked, this, &MainWindow::playPreviousTrack);
    connect(ui->shuffleToolButton, &QToolButton::clicked, this, &MainWindow::toggleShuffle);
    connect(playlistdialog, &PlaylistDialog::signalMainBlockUserInput, this, &MainWindow::blockUserInput);
    connect(ui->actionView_file_info, &QAction::triggered, playlistdialog, &PlaylistDialog::viewBusyItemInfo);
    connect(this->playlistdialog, &PlaylistDialog::signalCloseEvent, ui->actionPlaylist, &QAction::setChecked);
    this->setupActions();
    if(playlistdialog->initializeDefaults()) {
        ui->actionPlaylist->setChecked(true);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    delete playlistdialog;
    delete audio;
}

//Public methods

//Configuration methods

void MainWindow::readConfigIniFile() {
    //Read saved preferences from ini file
    QSettings settings(QDir::toNativeSeparators(execPath) + DEF_INIFILE, QSettings::IniFormat);
    settings.beginGroup(PLAYBACK);
    ui->repeatToolButton->setState(settings.value(REPEAT, 0).toInt());
    ui->shuffleToolButton->setState(settings.value(SHUFFLE, 0).toBool());
    this->setCurrentTrack(settings.value(LAST_TRACK, "D:\\Jet.mp3").toString());
    audio->setVolume(settings.value(VOLUME, DEF_VOLUME).toInt());
    ui->volumeSlider->setValue(settings.value(VOLUME, DEF_VOLUME).toInt());
    settings.endGroup();
    settings.beginGroup(DIMENSIONS);
    restoreGeometry(settings.value(MAIN_GEOMETRY).toByteArray());
    settings.endGroup();
}

void MainWindow::writeConfigIniFile() {
    //Save currrent preferences in an ini file
    QSettings settings(QDir::toNativeSeparators(execPath) + DEF_INIFILE, QSettings::IniFormat);
    settings.beginGroup(PLAYBACK);
    settings.setValue(REPEAT, ui->repeatToolButton->getState());
    settings.setValue(SHUFFLE, ui->shuffleToolButton->getState());
    settings.setValue(LAST_TRACK, this->getCurrentTrack());
    settings.setValue(VOLUME, audio->getVolume());
    settings.endGroup();
    settings.beginGroup(DIMENSIONS);
    settings.setValue(MAIN_GEOMETRY, saveGeometry());
    settings.endGroup();
}

void MainWindow::setWindowSize(QSize sz) {
    this->resize(sz);
    //qDebug() << "resized window";
}

void MainWindow::setupActions() {
    //Connect menu actions
    connect(ui->actionEnqueue_file_s, &QAction::triggered, this, &MainWindow::enqueueFiles);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::openFiles);
    connect(ui->actionPlay_URL, &QAction::triggered, this->playlistdialog, &PlaylistDialog::openNewUrl);
    connect(ui->actionOpen_playlist, &QAction::triggered, this->playlistdialog, &PlaylistDialog::openPlaylist);
    connect(ui->actionSave_playlist, &QAction::triggered, this->playlistdialog, &PlaylistDialog::savePlaylist);
    connect(ui->actionPlay_folder, &QAction::triggered, this->playlistdialog, &PlaylistDialog::openFolders);
    connect(ui->actionEnqueue_folder, &QAction::triggered, this->playlistdialog, &PlaylistDialog::enqueueFolder);
    connect(ui->actionPlay, &QAction::triggered, ui->playToolButton, &QToolButton::click);
    connect(ui->actionPause, &QAction::triggered, ui->pauseToolButton, &QToolButton::click);
    connect(ui->actionStop, &QAction::triggered, ui->stopToolButton, &QToolButton::click);
    connect(ui->actionNext, &QAction::triggered, ui->fwdToolButton_22, &QToolButton::click);
    connect(ui->actionPrevious, &QAction::triggered, ui->rwdToolButton, &QToolButton::click);
    connect(ui->actionBack_N_seconds, &QAction::triggered, this, &MainWindow::backNSecs);
    connect(ui->actionFwd_N_seconds, &QAction::triggered, this, &MainWindow::forwardNSEcs);
    connect(ui->actionRepeat, &QAction::triggered, ui->repeatToolButton, &RepeatToolButton::virtualClick);
    connect(ui->actionShuffle, &QAction::triggered, ui->shuffleToolButton, &QToolButton::click);
    connect(ui->actionToggle_Time, &QAction::triggered, ui->trackLengthLcdNumber, &TrackLengthLCDNumber::toggleReverse);
    connect(ui->actionVolume_Up, &QAction::triggered, this, &MainWindow::volumeUp);
    connect(ui->actionVolume_Down, &QAction::triggered, this, &MainWindow::volumeDown);
    connect(ui->actionPlaylist, &QAction::triggered, this, &MainWindow::togglePlaylistVisibility);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionCancel_operation, &QAction::triggered,
            this->playlistdialog, &PlaylistDialog::cancelListOperation);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::showAboutInfo);
}

void MainWindow::loadPlaybackOptions() {
    qDebug("%i",QThread::currentThreadId());
}

void MainWindow::setupLabelTags(bool url) {
    if(url) {
        ui->trackArtistLabel->setContentName("Stream: ");
        ui->trackAlbumLabel->setContentName("Now playing: ");
        ui->trackTitleLabel->setContentName("Now playing: ");
        return;
    }
    ui->trackArtistLabel->setContentName("Artist: ");
    ui->trackAlbumLabel->setContentName("Album: ");
    ui->trackTitleLabel->setContentName("Title: ");
}

void MainWindow::initializeTrackPositionDisplay(int length) {
    bool state = length > 0 ? true : false;
    ui->trackLengthSlider->setEnabled(state);
    ui->trackLengthSlider->setMinimum(0);
    ui->trackLengthSlider->setMaximum(length);
    ui->trackLengthLcdNumber->setLength(length);
    ui->trackLengthLcdNumber->setDisplay(0);
}

void MainWindow::updateTrackPositionDisplay(int length) {
    if(ui->trackLengthSlider->isSliderDown()) {
        return;
    }
    ui->trackLengthSlider->setValue(length);
    ui->trackLengthLcdNumber->setDisplay(length);
}

void MainWindow::setTrackDisplayInfo(TrackInfo trackinfo) {
    qDebug() << "Set track info to be displayed";
    if(trackinfo.hasShoutCast()) {
        StreamAttr attr = audio->getAudioAttributes();
        this->setupLabelTags(true);
        ui->trackTitleLabel->setContent(trackinfo.getIcyField(ICY_CURRENT));
        ui->trackAlbumLabel->setContent(trackinfo.getIcyField(ICY_CURRENT));
        ui->trackArtistLabel->setContent(trackinfo.getIcyField(ICY_NAME));
        this->setAudioInfo(trackinfo.getIcyField(ICY_BR), QString::number(attr.freq),
                           QString::number(attr.channels));
        this->playlistdialog->setUrlItemInfo(&trackinfo);
        return;
    }
    this->setupLabelTags();
    ui->trackTitleLabel->setContent(trackinfo.getField(TITLE));
    ui->trackArtistLabel->setContent(trackinfo.getField(ARTIST));
    ui->trackAlbumLabel->setContent(trackinfo.getField(ALBUM));
    this->setAudioInfo(trackinfo.getField(BITRATE), trackinfo.getField(SAMPLE_RATE),
                                                                      trackinfo.getField(TRACK_CHANNELS));

}

void MainWindow::setAudioInfo(QString bitrate, QString freq, QString channels) {
    QString brunits = "kbps";
    QString funits = "kHz";
    if(bitrate.toFloat() > 1000) {
        bitrate.setNum(bitrate.toFloat()/1000, 'g', 3);
        brunits = "Mbps";
    }
    freq.setNum(freq.toFloat()/1000, 'g', 3);
    if(channels == "2") {
        channels = "Stereo";
    }
    else if(channels == "1")
        channels = "Mono";
    else
        channels = "--?--";
    ui->audioInfoLabel->setText(bitrate + brunits + "\n" + freq + funits + "\n" + channels);
}

void MainWindow::receivedUrlMetaInfo(const char *metainfo, const char *metaextra) {
    if(!metainfo) return;
    TrackInfo tif = TagHandler::readShoutCastTagInfo(metainfo, metaextra);
    this->setTrackDisplayInfo(tif);
}

void MainWindow::trackFinished() {
    qDebug("Track finished");
    audio->enableTrackPositionUpdates(false);
    this->initializeTrackPositionDisplay(0);
    executePlaybackFsm(FIN_TRS);
    if(ui->repeatToolButton->getState() == REPEAT_ONE)
        this->playCurrentTrack();
    else
        this->playNextTrack();
}

void MainWindow::playlistDoubleClicked(QString filepath, TrackInfo tif){
    //ui->stopToolButton->setChecked(false);
    this->setCurrentTrack(filepath);
    this->setTrackDisplayInfo(tif);
    if(!this->playCurrentTrack()) {
            this->playNextTrack();
    }
}

void MainWindow::receivedNextTrack(QString filepath, TrackInfo tif) {
    executePlaybackFsm(NEXT_TRS);
    this->setCurrentTrack(filepath);
    this->setTrackDisplayInfo(tif);
    if(ui->stopToolButton->isChecked())
        return;
    if(!this->playCurrentTrack()) {
            this->playNextTrack();
    }
}



bool MainWindow::playCurrentTrack() {
    int length;
    if(ui->pauseToolButton->isChecked()) {
        this->pauseCurrentTrack();
        return true;
    }
    executePlaybackFsm(PLAY_TRS);
    audio->stopTrack();
    if(!audio->prepareTrack(this->getCurrentTrack(), &length)){
        executePlaybackFsm(FIN_TRS);
        return false;
    }
    this->initializeTrackPositionDisplay(length);
    audio->enableTrackPositionUpdates(true);
    audio->playTrack();
    return true;
}

void MainWindow::stopCurrentTrack() {
    executePlaybackFsm(STOP_TRS);
    audio->stopTrack();
    this->updateTrackPositionDisplay(0);
}

void MainWindow::pauseCurrentTrack() {
    executePlaybackFsm(PAUSE_TRS);
    audio->pauseTrack();
}

void MainWindow::playNextTrack() {
    bool repeatall = false;
    bool shuffle = ui->shuffleToolButton->getState();
    if(ui->repeatToolButton->getState() == REPEAT_ALL)
        repeatall = true;
    this->playlistdialog->getNextPlaylistItem(shuffle, repeatall);
}

void MainWindow::playPreviousTrack() {
    this->playlistdialog->getPreviousPlaylistItem(ui->shuffleToolButton->getState());
}


void MainWindow::toggleShuffle() {
    if(!ui->shuffleToolButton->getState()) {
        ui->shuffleToolButton->setState(true);
        this->playlistdialog->reshufflePlaylist();
    }
    else {
        ui->shuffleToolButton->setState(false);
        this->playlistdialog->clearShuffledPlaylist();
    }
}


void MainWindow::openFiles() {
    QStringList audiofiles = QFileDialog::getOpenFileNames(this, "Select one more files to open",
                                                           ".", "Audio files (*.mp3 *.wav *.wma)");
    if(!audiofiles.isEmpty()) {
        this->playlistdialog->playNewFiles(audiofiles);
    }
}


void MainWindow::enqueueFiles() {
    this->playlistdialog->addFilesInPlaylist();
}

QString MainWindow::getCurrentTrack() { return currentTrack; }

void MainWindow::setCurrentTrack(QString path) { currentTrack = path; }


void MainWindow::seekTrackPosition() {
    int position;
    audio->enableTrackPositionUpdates(false);
    if(this->trackSliderGroovePressed) {
        position = (this->trackSliderLastPos*ui->trackLengthSlider->maximum())/ui->trackLengthSlider->width();
        audio->enableTrackPositionUpdates(false);
        ui->trackLengthSlider->setValue(position);
    }
    else
        position = ui->trackLengthSlider->value();
    audio->setTrackPosition(static_cast<double>(position));
    updateTrackPositionDisplay(position);
    if(audio->isPlaying()) {
        audio->enableTrackPositionUpdates(true);
    }
    ui->trackLengthSlider->setPageStep(10);
    trackSliderGroovePressed = false;
}

void MainWindow::handleSliderPress() {
    this->trackSliderGroovePressed = false;
}

void MainWindow::backNSecs() {
    int position = 0;
    if(this->trackSliderGroovePressed) return;
    audio->enableTrackPositionUpdates(false);
    if(ui->trackLengthSlider->value() >= 5)
        position = ui->trackLengthSlider->value() - 5;
    updateTrackPositionDisplay(position);
    audio->setTrackPosition(static_cast<double>(position));
    if(audio->isPlaying()) {
        audio->enableTrackPositionUpdates(true);
    }
}

void MainWindow::forwardNSEcs() {
    int position = ui->trackLengthSlider->maximum();
    if(this->trackSliderGroovePressed) return;
    audio->enableTrackPositionUpdates(false);
    if(ui->trackLengthSlider->value() <= ui->trackLengthSlider->maximum() - 5)
        position = ui->trackLengthSlider->value() + 5;
    updateTrackPositionDisplay(position);
    audio->setTrackPosition(static_cast<double>(position));
    if(audio->isPlaying()) {
        audio->enableTrackPositionUpdates(true);
    }
}

void MainWindow::seekTime() {
    int position = ui->trackLengthSlider->value();
    audio->setTrackPosition(static_cast<double>(position));
    ui->trackLengthLcdNumber->setDisplay(ui->trackLengthSlider->value());
}


void MainWindow::volumeSlide() {
    int value = ui->volumeSlider->value();
    audio->setVolume(value);
    ui->muteToolButton->setState(false);
    ui->volumeSlider->setToolTip(QString::number(value) + "%");
}

void MainWindow::volumeUp() {
    int volumestep = 5;
    int value = ui->volumeSlider->value();
    if(value <= 99 - volumestep) {
        ui->volumeSlider->setValue(value + volumestep);
    }
    else {
        ui->volumeSlider->setValue(99);
    }
}

void MainWindow::volumeDown() {
    int volumestep = 5;
    int value = ui->volumeSlider->value();
    if(value >= volumestep) {
        ui->volumeSlider->setValue(value - volumestep);
    }
    else {
        ui->volumeSlider->setValue(0);
    }
}


void MainWindow::toggleVolumeMute() {
    if(!ui->muteToolButton->getState()) {
        ui->muteToolButton->setState(true);
        audio->setVolume(0);
        ui->muteToolButton->setToolTip(QString::fromStdString("Unmute"));
    }
    else {
        ui->muteToolButton->setState(false);
        audio->setVolume(ui->volumeSlider->value());
        ui->muteToolButton->setToolTip(QString::fromStdString("Mute"));
    }
}


void MainWindow::showAboutInfo() {
    QMessageBox::about(this, "About", "This is a project under construction");
}

void MainWindow::togglePlaylistVisibility() {
    if(playlistdialog->isVisible())
        playlistdialog->hide();
    else
        playlistdialog->show();
}

void MainWindow::closeEvent(QCloseEvent *event) {
    int ret;
    QMessageBox msgbox;
    msgbox.setText("Please wait while the current playlist is being saved.");
    msgbox.setInformativeText("Press Quit to exit");
    msgbox.setStandardButtons(QMessageBox::Close);
    connect(playlistdialog, &PlaylistDialog::signalLastSaveDone, &msgbox, &QMessageBox::done);
    if(true) {
        if(playlistdialog->isBusy()) {
            ret = QMessageBox::warning(this, tr("Operation running"),
                                 "An operation has not finished yet!\n Are you sure you want to exit?",
                                 QMessageBox::Ok | QMessageBox::Cancel);
            if(ret == QMessageBox::Cancel) {
                event->ignore();
                return;
            }
            playlistdialog->cancelListOperation();
        }
        this->writeConfigIniFile();
        playlistdialog->saveDefaults();
        if(playlistdialog->isBusy()) {
           ret = msgbox.exec();
        }
        event->accept();
    }
    else {
        event->ignore();
    }
}

void MainWindow::blockUserInput(bool block){
    ui->actionOpen->setDisabled(block);
    ui->actionOpen_playlist->setDisabled(block);
    ui->actionEnqueue_file_s->setDisabled(block);
    ui->actionEnqueue_folder->setDisabled(block);
    ui->actionPlay_folder->setDisabled(block);
    ui->actionPlay_URL->setDisabled(block);
    ui->actionSave_playlist->setDisabled(block);
    ui->ejectToolButton->setDisabled(block);
    ui->actionCancel_operation->setEnabled(block);
}

void MainWindow::moveEvent(QMoveEvent *ev) {
    if(!renderFinished)
        return;
    int x = ev->pos().x() - ev->oldPos().x();
    int y = ev->pos().y() - ev->oldPos().y();
    playlistdialog->dragWindow(x, y);
}

bool MainWindow::event(QEvent *event) {
    if(event->type() == QEvent::NonClientAreaMouseButtonPress){
        renderFinished = true;
        return true;
    }
    return QMainWindow::event(event);
}

void MainWindow::resizeEvent(QResizeEvent *ev) {
    renderFinished = false;
}

bool MainWindow::eventFilter(QObject *source, QEvent *event) {
    //check also sliderPressed signal
    if(source == ui->trackLengthSlider) {
        if(event->type() == QEvent::Wheel) {
            if(this->trackSliderGroovePressed || ui->trackLengthSlider->isSliderDown()) return true;
            int position = ui->trackLengthSlider->value();
            audio->enableTrackPositionUpdates(false);
            audio->setTrackPosition(static_cast<double>(position));
            ui->trackLengthLcdNumber->setDisplay(position);
            this->seekTime();
            if(audio->isPlaying())
                audio->enableTrackPositionUpdates(true);
            return false;
        }
        //Check if slider is pressed
        if(event->type() == QEvent::MouseButtonPress) {
            QMouseEvent *mEvent = static_cast<QMouseEvent *>(event);
            if(mEvent->buttons() == Qt::LeftButton) {
                this->trackSliderGroovePressed = true;
                return false;
            }
            else
                return true;
        }
        if(event->type() == QEvent::MouseButtonRelease && this->trackSliderGroovePressed) {
            QMouseEvent *mEvent = static_cast<QMouseEvent *>(event);
            if(mEvent->button() == Qt::LeftButton) {
                this->trackSliderLastPos = mEvent->pos().x();
                if(ui->trackLengthSlider->rect().contains(mEvent->pos()))
                    this->seekTrackPosition();
            }
            return false;
        }
        return false;
    }
    //Volume slider jump at clicked position on left mouse press
    if(source == ui->volumeSlider) {
        if(event->type() == QEvent::MouseButtonPress) {
            QMouseEvent *mEvent = static_cast<QMouseEvent*>(event);
            if(mEvent->buttons() == Qt::LeftButton) {
                ui->volumeSlider->setValue((ui->volumeSlider->maximum()*mEvent->pos().x())/ui->volumeSlider->width());
                return false;
            }
        }
    }
    return QMainWindow::eventFilter(source, event);
}

//Private methods

void MainWindow::executePlaybackFsm(playback_transitions transition) {
    this->currentPlaybackState = playback_fsm[currentPlaybackState.next[transition]];
    if(ui->stopToolButton->isEnabled() != currentPlaybackState.out[0])
        ui->stopToolButton->setEnabled(currentPlaybackState.out[0]);
    if(ui->stopToolButton->isEnabled())
        if(ui->stopToolButton->isChecked() != currentPlaybackState.out[1])
            ui->stopToolButton->setChecked(currentPlaybackState.out[1]);
    if(ui->playToolButton->isChecked() != currentPlaybackState.out[2])
        ui->playToolButton->setChecked(currentPlaybackState.out[2]);
    if(ui->pauseToolButton->isEnabled() != currentPlaybackState.out[3])
        ui->pauseToolButton->setEnabled(currentPlaybackState.out[3]);
    if(ui->pauseToolButton->isChecked() != currentPlaybackState.out[4])
        ui->pauseToolButton->setChecked(currentPlaybackState.out[4]);
}
