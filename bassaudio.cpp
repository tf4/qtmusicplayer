#include "bassaudio.h"
#include <QtGlobal>
#include <QDebug>
#include <QThread>
#include "customtypes.h"




using namespace global_methods;

BassAudio::BassAudio(int v, QObject *parent) :
    QObject(parent)
{
    posupdatetimer = new QTimer();
    urlmetaupdatetimer = new QTimer();
    this->setVolume(v);
    qDebug() << "Track constructor";
    qDebug("%i",this->getVolume());
    flags = 0;
    flags |= BASS_MUSIC_PRESCAN;
    flags |= BASS_STREAM_DECODE;
    flags |= BASS_SAMPLE_FLOAT;
    frequency = 44100;
    BASS_Init(-1, frequency, 0, NULL, NULL);
    connect(this->posupdatetimer, &QTimer::timeout, this, &BassAudio::emitTrackPosition);
    connect(this->urlmetaupdatetimer, &QTimer::timeout, this, &BassAudio::metaUrlInfo);
}



bool BassAudio::prepareTrack(QString path, int *length) {
    BASS_StreamFree(Stream);
    if(global_methods::isURL(path)) {
        qDebug() << "found url";
        Stream = BASS_StreamCreateURL(path.toStdWString().c_str(), 0,
                                      BASS_SAMPLE_FLOAT|BASS_STREAM_BLOCK|BASS_STREAM_DECODE|BASS_STREAM_STATUS,
                                      NULL, 0);
    }
    else
        Stream = BASS_StreamCreateFile(false, path.toStdWString().c_str(), 0, 0, flags);
    qDebug("Stream %i", Stream);
    if(!Stream) {
        return false;
    }
    BASS_ChannelSetSync(Stream, BASS_SYNC_END, 0, EndSyncProc, this);
    if(global_methods::isURL(path))
        urlmetaupdatetimer->start(3000);
    MixStream = BASS_Mixer_StreamCreate(frequency, 2, BASS_SAMPLE_FLOAT );
    BASS_Mixer_StreamAddChannel(MixStream, Stream, BASS_MIXER_DOWNMIX | BASS_MIXER_BUFFER);
    BASS_ChannelSetAttribute(MixStream, BASS_ATTRIB_VOL, (float)volume/100);
    this->lengthinbytes = BASS_ChannelGetLength(Stream, BASS_POS_BYTE);
    *length = qRound(BASS_ChannelBytes2Seconds(Stream, lengthinbytes));
    return true;
}

void BassAudio::playTrack() {
    BASS_ChannelPlay(MixStream, true);
    paused = false;
}

void BassAudio::stopTrack() {
    BASS_ChannelStop(MixStream);
    BASS_ChannelStop(Stream);
    enableTrackPositionUpdates(false);
    urlmetaupdatetimer->stop();
    paused = false;
}

void BassAudio::pauseTrack() {
    if(!paused) {
        BASS_ChannelPause(MixStream);
        paused = true;
    }
    else {
        BASS_ChannelPlay(MixStream, false);
        paused = false;
    }
}

unsigned int BassAudio::getVolume() {
    return this->volume;
}

void BassAudio::setVolume(unsigned int value) {
    value = qMax((unsigned int)0, qMin(value, (unsigned int)99));
    this->volume = value;
    BASS_ChannelSetAttribute(MixStream, BASS_ATTRIB_VOL, static_cast<float>(volume)/100);
}

bool BassAudio::isPlaying() {
    if(BASS_ChannelIsActive(Stream) && BASS_ChannelIsActive(MixStream)) {
        return true;
    }
    return false;
}

void BassAudio::emitTrackPosition() {
    QWORD posinbytes = BASS_ChannelGetPosition(Stream, BASS_POS_BYTE);
    double posinsecs = BASS_ChannelBytes2Seconds(Stream, posinbytes);
    //qDebug("%i", qRound(posinsecs));
    emit this->signalTrackPosition(qRound(posinsecs));
}

void BassAudio::enableTrackPositionUpdates(bool state) {
    if(state) { posupdatetimer->start(1000); }
    else { posupdatetimer->stop();}
}

void BassAudio::setTrackPosition(double posinsecs) {
    QWORD posinbytes;
    double percent;
    if(this->isPlaying()) {
        percent = posinsecs / BASS_ChannelBytes2Seconds(Stream, this->lengthinbytes);
        posinbytes = static_cast<int>(percent*static_cast<double>(this->lengthinbytes));
        BASS_ChannelSetPosition(Stream, posinbytes, BASS_POS_BYTE);
    }
}


void BassAudio::metaUrlInfo() {
    urlmetaupdatetimer->setInterval(10000);
    //Shoutcast tags
    const char* meta=BASS_ChannelGetTags(Stream, BASS_TAG_ICY);
    const char* metaextra = BASS_ChannelGetTags(Stream, BASS_TAG_META);
    if(meta) {
        emit this->signalMetaUrlUpdate(meta, metaextra);
    }
}

const char* BassAudio::getMetaUrlInfo() {
    return BASS_ChannelGetTags(Stream, BASS_TAG_ICY);
}

const char* BassAudio::getMetaUrlExtraInfo() {
    return BASS_ChannelGetTags(Stream, BASS_TAG_META);
}

StreamAttr BassAudio::getAudioAttributes() {
    BASS_CHANNELINFO *cinfo = new BASS_CHANNELINFO;
    StreamAttr statt;
    float tempval;
    BASS_ChannelGetAttribute(Stream, BASS_ATTRIB_FREQ, &tempval);
    statt.freq = static_cast<int>(tempval);
    BASS_ChannelGetInfo(Stream, cinfo);
    statt.channels = static_cast<int>(cinfo->chans);
    return statt;
}

void CALLBACK BassAudio::EndSyncProc(HSYNC handle, DWORD channel, DWORD data, void *user) {
    Q_UNUSED(handle); Q_UNUSED(channel); Q_UNUSED(data);
    qDebug("Stream has ended");
    //this->stopUrlMetaTimer();
    static_cast<BassAudio*>(user)->stopUrlMetaTimer();
    emit static_cast<BassAudio*>(user)->signalStreamEnded();
}

void CALLBACK BassAudio::MetaSyncProc(const void* buffer, DWORD length, void *user) {
    Q_UNUSED(buffer); Q_UNUSED(length); Q_UNUSED(user);
    qDebug("Meta proc has ended");
}

void BassAudio::stopUrlMetaTimer() {urlmetaupdatetimer->stop();}

BassAudio::~BassAudio() {
    qDebug() << "Track destructor";
}


