#include "mainwindow.h"
#include "playlistdialog.h"
#include "customtypes.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<VectorList>("VectorList");
    qRegisterMetaType<QVector<int> >("QVector<int>");
    MainWindow w;
    //w.show();

    return a.exec();
}
