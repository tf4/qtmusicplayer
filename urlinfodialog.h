#ifndef URLINFODIALOG_H
#define URLINFODIALOG_H

#include <QDialog>
#include "trackinfo.h"

namespace Ui {
class UrlInfoDialog;
}

class UrlInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UrlInfoDialog(QWidget *parent = 0);
    void setUrl(QString url);
    void setUrlInfo(TrackInfo tif);
    ~UrlInfoDialog();

private:
    Ui::UrlInfoDialog *ui;
};

#endif // URLINFODIALOG_H
