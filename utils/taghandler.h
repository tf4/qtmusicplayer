#ifndef TAGHANDLER_H
#define TAGHANDLER_H

#include <QObject>
#include "trackinfo.h"

enum ID3v1Identifiers {ID3_TRACK = 0, ID3_TITLE = 1, ID3_ARTIST = 2, ID3_ALBUM = 3, ID3_YEAR = 4,
                       ID3_GENRE = 5, ID3_COMMENTS = 6, ID3_FIELDS = 7};

class TagHandler : public QObject
{
    Q_OBJECT
public:
    explicit TagHandler(QObject *parent = 0);
    static TrackInfo readTagInfo(const QString &filepath);
    static TrackInfo readID3v2TagInfo(const QString &filepath);
    static TrackInfo readID3TagInfo(const QString &filepath);
    static bool writeID3TagInfo(QHash<ID3v1Identifiers, QString> data, const QString &filepath);
    static bool writeID3v2TagInfo(QHash<QString, QString> data, const QString &filepath);
    static bool removeId3v1Tags(const QString &filepath);
    static bool removeId3v2Tags(const QString &filepath);
    static TrackInfo readShoutCastTagInfo(const char* metadata, const char* metaextra);
    static QStringList getStandardGenreList();
    void readMultipleTagInfo(QStringList filelist);

signals:

public slots:

};

#endif // TAGHANDLER_H
