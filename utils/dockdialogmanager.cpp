#include "dockdialogmanager.h"
#include <QtGlobal>
#include <QDebug>

#define HOOKSIZE 10

DockDialogManager::DockDialogManager()
{
    dockState.isDocked = true;
    dockState.dockSide = DOCK_RIGHT;
}

void DockDialogManager::initDockState(QRect child, QRect parent) {
    POINT mousePoint;
    if(!GetCursorPos(&mousePoint)) return;
    initialMousePos = QPoint(mousePoint.x, mousePoint.y);
    initialFramePos = QPoint(child.x(), child.y());
    forceHooks(HOOKSIZE, child, parent);
}

bool DockDialogManager::isDocked() {return dockState.isDocked;}

int DockDialogManager::getDockSide() {return dockState.dockSide;}

QPoint DockDialogManager::calcDockPosition(QRect child, QRect parent) {
    int offset_x = currentMousePos.x() - initialMousePos.x();
    int offset_y = currentMousePos.y() - initialMousePos.y();
    switch (dockState.dockSide) {
    case DOCK_BELOW:
        return QPoint(initialFramePos.x()+offset_x, parent.y()+parent.height());
        break;
    case DOCK_RIGHT:
        return QPoint(parent.x()+parent.width(), initialFramePos.y()+offset_y);
        break;
    case DOCK_ABOVE:
        return QPoint(initialFramePos.x()+offset_x, parent.y()-child.height());
        break;
    case DOCK_LEFT:
        return QPoint(parent.x()-child.width(), initialFramePos.y()+offset_y);
        break;
    default:
        return QPoint(child.x(), child.y());
        break;
    }
}

void DockDialogManager::updateDockState(QRect child, QRect parent) {
    POINT mousePoint;
    QPoint mousePos;
    if(!GetCursorPos(&mousePoint)) return;
    currentMousePos = QPoint(mousePoint.x, mousePoint.y);
    mousePos = currentMousePos;
    if(dockState.isDocked) {
        switch (dockState.dockSide) {
        case DOCK_BELOW:
            if(qAbs(mousePos.y() - initialMousePos.y()) < HOOKSIZE+1 &&
                    QRect(child.x(), parent.y(), child.width(), parent.height()).intersects(parent))
                return;
            break;
        case DOCK_RIGHT:
            if(qAbs(mousePos.x() - initialMousePos.x()) < HOOKSIZE+1 &&
                    QRect(parent.x(), child.y(), parent.width(), child.height()).intersects(parent))
                return;
            break;
        case DOCK_ABOVE:
            if(qAbs(mousePos.y() - initialMousePos.y()) < HOOKSIZE+1 &&
                    QRect(child.x(), parent.y(), child.width(), parent.height()).intersects(parent))
                return;
            break;
        case DOCK_LEFT:
            if(qAbs(mousePos.x() - initialMousePos.x()) < HOOKSIZE+1 &&
                    QRect(parent.x(), child.y(), parent.width(), child.height()).intersects(parent))
                return;
            break;
        default:
            break;
        }
        initialFramePos = QPoint(child.x(), child.y());
        initialMousePos = mousePos;
        dockState.isDocked = false;
        dockState.dockSide = NO_DOCK;
    }
    else {
        if(!child.intersects(parent))
            forceHooks(HOOKSIZE, child, parent);
    }
}

void DockDialogManager::forceHooks(int range, QRect child, QRect parent) {
    if(child.intersects(parent)) {
        dockState.isDocked = false;
        dockState.dockSide = NO_DOCK;
        return;
    }
    int maxInsct = NO_DOCK;
    QRect hooks[5];
    hooks[DOCK_BELOW] = QRect(QPoint(child.x(), child.y()-range), QSize(child.width(), range));
    hooks[DOCK_ABOVE] = QRect(child.bottomLeft(), QSize(child.width(), range));
    hooks[DOCK_LEFT] = QRect(child.topRight(), QSize(range, child.height()));
    hooks[DOCK_RIGHT] = QRect(QPoint(child.x()-HOOKSIZE, child.y()), QSize(range, child.height()));
    hooks[NO_DOCK] = QRect(0, 0, 0, 0);
    for(int i = 0; i < 4; i++) {
        hooks[i] = parent.intersected(hooks[i]);
        if(hooks[i].width()*hooks[i].height() > 0 &&
                hooks[i].width()*hooks[i].height() > hooks[maxInsct].width()*hooks[maxInsct].height()) {
            maxInsct = i;
        }
    }
    dockState.dockSide = maxInsct;
    if(maxInsct == NO_DOCK) dockState.isDocked = false;
    else dockState.isDocked = true;
}

void DockDialogManager::handleWin32MoveEvent(void *message, WId wid, QRect child, QRect parent) {
    MSG *msg = static_cast<MSG*>(message);
    if(msg->message == WM_MOVE) {
        updateDockState(child, parent);
        updateDockedWinPos(wid, child, parent);
    }
}

void DockDialogManager::updateDockedWinPos(WId wid, QRect child, QRect parent) {
    if(isDocked()) {
        QPoint p = calcDockPosition(child, parent);
        SetWindowPos((HWND)wid, NULL, p.x(), p.y(), child.width(), child.height(),
                 SWP_SHOWWINDOW|SWP_NOSENDCHANGING|SWP_NOSIZE);
    }
}
