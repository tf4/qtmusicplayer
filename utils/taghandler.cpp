#include "taghandler.h"
#include <tag.h>
#include <fileref.h>
#include <mpeg/mpegfile.h>
#include <mpeg/id3v2/id3v2frame.h>
#include <mpeg/id3v2/id3v2header.h>
#include <mpeg/id3v2/id3v2tag.h>
#include <mpeg/id3v1/id3v1tag.h>
#include <mpeg/id3v1/id3v1genres.h>
#include <toolkit/tbytevector.h>
#include <toolkit/tpropertymap.h>
#include <toolkit/tstringlist.h>
#include <mpeg/id3v2/frames/textidentificationframe.h>
#include <mpeg/id3v2/frames/urllinkframe.h>
#include <mpeg/id3v2/frames/commentsframe.h>

#include <QFileInfo>
#include <QStringList>
#include <QDebug>

TagHandler::TagHandler(QObject *parent) :
    QObject(parent)
{
}

QStringList TagHandler::getStandardGenreList(){
    QStringList genrelist;
    TagLib::StringList gl = TagLib::ID3v1::genreList();
    for(TagLib::StringList::ConstIterator it = gl.begin(); it != gl.end(); ++it) {
        genrelist.append(QString::fromWCharArray((*it).toWString().c_str()));
    }
    genrelist.prepend("");
    return genrelist;
}

TrackInfo TagHandler::readTagInfo(const QString &filepath) {
    TrackInfo trackinfo;
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    TagLib::FileRef *ref = new TagLib::FileRef(encodedName);
    if(!ref->isNull() && ref->tag()) {
        TagLib::Tag *tag = ref->tag();
        trackinfo.setTrackPath(path);
        trackinfo.setField(ARTIST, QString::fromUtf8(tag->artist().to8Bit(true).data()));
        trackinfo.setField(TITLE, QString::fromUtf8(tag->title().to8Bit(true).data()));
        trackinfo.setField(ALBUM, QString::fromUtf8(tag->album().to8Bit(true).data()));
        trackinfo.setField(TRACK_LENGTH, QString::number(ref->audioProperties()->length()));
        trackinfo.setField(BITRATE, QString::number(ref->audioProperties()->bitrate()));
        trackinfo.setField(TRACK_CHANNELS, QString::number(ref->audioProperties()->channels()));
        trackinfo.setField(SAMPLE_RATE, QString::number(ref->audioProperties()->sampleRate()));
        trackinfo.setField(TRACK_NUM, QString::number(tag->track()));
        trackinfo.setField(TRACK_YEAR, QString::number(tag->year()));
        trackinfo.setField(TRACK_GENRE, QString::fromUtf8(tag->genre().to8Bit().data()));
        trackinfo.setField(TRACK_COMMENTS, QString::fromUtf8(tag->comment().to8Bit().data()));
        TagLib::PropertyMap m = tag->properties();
        if(m.contains("MEDIA"))
            qDebug() << QString::fromWCharArray(m["MEDIA"][0].toWString().c_str());
    }
    else {
        qDebug() << "error reading tag";
    }
    delete ref;
    return trackinfo;

}

TrackInfo TagHandler::readID3v2TagInfo(const QString &filepath) {
    TrackInfo tif;
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    tif = TagHandler::readTagInfo(filepath);

    TagLib::MPEG::File *mf= new TagLib::MPEG::File(encodedName, true);
    if(!mf->hasID3v2Tag()){
        qDebug() << " has no id3v2";
    }
    else {
        TagLib::ID3v2::Tag *mtag = mf->ID3v2Tag();
        if(mtag){
            tif.setID3v2(true);
            TagLib::ID3v2::FrameList l;
            qDebug("id3 version %i", mtag->header()->majorVersion());
            //mtag->header()->revisionNumber();
            l = mtag->frameListMap()["TPE2"]; //Album artist
            if(!l.isEmpty()){
                tif.setField(ALBUM_ARTIST, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
            }
            l = mtag->frameListMap()["TCON"]; //Genre
            if(!l.isEmpty()){
                tif.setField(TRACK_GENRE, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TBPM"]; //BPM
            if(!l.isEmpty()){
                tif.setField(TRACK_BPM, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TRCK"]; //Track#
            if(!l.isEmpty()){
                tif.setField(TRACK_NUM, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TPOS"]; //Disc#
            if(!l.isEmpty()){
                tif.setField(DISC_NUM, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TCOM"]; //Composer
            if(!l.isEmpty()){
                tif.setField(TRACK_COMPOSER, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TPUB"]; //Publisher
            if(!l.isEmpty()){
                tif.setField(TRACK_PUBLISHER, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TOPE"]; //Original artist
            if(!l.isEmpty()){
                tif.setField(TRACK_ORIGARTIST, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["WCOP"]; //Copyright
            if(!l.isEmpty()){
                tif.setField(TRACK_COPYRIGHT, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }
            l = mtag->frameListMap()["TENC"]; //Encoded by
            if(!l.isEmpty()){
                tif.setField(TRACK_ENCODED_BY, QString::fromWCharArray(l.front()->toString().toWString().c_str()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str()) + " TEnc";
            }
            l = mtag->frameListMap()["WXXX"]; //url
            if(!l.isEmpty()){
                tif.setField(TRACK_URL,
                             QString::fromWCharArray(dynamic_cast<TagLib::ID3v2::UserUrlLinkFrame*>(l.front())->url().toWString().data()));
                //qDebug() << QString::fromWCharArray(l.front()->toString().toWString().c_str());
            }

        }
        else {
            qDebug() << "ID3v2 not found";
        }
    }
    delete mf;
    return tif;
}

TrackInfo TagHandler::readID3TagInfo(const QString &filepath) {
    TrackInfo tif;
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    TagLib::MPEG::File *mf= new TagLib::MPEG::File(encodedName, true);
    if(!mf->hasID3v1Tag()){
        qDebug() << " has no id3v1";
    }
    else {
        TagLib::ID3v1::Tag *mtag = mf->ID3v1Tag();
        if(mtag) {
            tif.setID3v1(true);
            tif.setField(TRACK_NUM, QString::number(mtag->track()));
            tif.setField(TITLE, QString::fromWCharArray(mtag->title().toWString().c_str()));
            tif.setField(ARTIST, QString::fromWCharArray(mtag->artist().toWString().c_str()));
            tif.setField(ALBUM, QString::fromWCharArray(mtag->album().toWString().c_str()));
            tif.setField(TRACK_YEAR, QString::number(mtag->year()));
            tif.setField(TRACK_GENRE, QString::fromWCharArray(mtag->genre().toWString().c_str()));
            tif.setField(TRACK_COMMENTS, QString::fromWCharArray(mtag->comment().toWString().c_str()));
        }
    }
    delete mf;
    return tif;
}

bool TagHandler::writeID3TagInfo(QHash<ID3v1Identifiers, QString> data, const QString &filepath) {
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    TagLib::MPEG::File *mf= new TagLib::MPEG::File(encodedName, true);
    TagLib::ID3v1::Tag *mtag = mf->ID3v1Tag(true);
    if(!mtag || data.isEmpty()) {
        delete mf;
        return false;
    }
    if(data.contains(ID3_TRACK))
        mtag->setTrack(data.value(ID3_TRACK).toUInt());
    if(data.contains(ID3_TITLE))
        mtag->setTitle(data.value(ID3_TITLE).toStdString().data());
    if(data.contains(ID3_ARTIST))
        mtag->setArtist(data.value(ID3_ARTIST).toStdString().data());
    if(data.contains(ID3_ALBUM))
        mtag->setAlbum(data.value(ID3_ALBUM).toStdString().data());
        //mtag->setAlbum(TagLib::String(data.value(ID3_ALBUM).toStdString().data(), TagLib::String::UTF8));
    if(data.contains(ID3_YEAR))
        mtag->setYear(data.value(ID3_YEAR).toUInt());
    if(data.contains(ID3_GENRE))
        mtag->setGenre(data.value(ID3_GENRE).toStdString().data());
    if(data.contains(ID3_COMMENTS))
        mtag->setComment(data.value(ID3_COMMENTS).toStdString().data());
    mf->save(TagLib::MPEG::File::ID3v1, false, 4, false);
    if(mf->hasID3v2Tag()) qDebug() << "v2 added";
    delete mf;
    return true;
}

bool TagHandler::writeID3v2TagInfo(QHash<QString, QString> data, const QString &filepath) {
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    TagLib::MPEG::File *mf= new TagLib::MPEG::File(encodedName, true);
    TagLib::ID3v2::Tag *mtag = mf->ID3v2Tag(true);
    if(!mtag || data.isEmpty()) {
        delete mf;
        return false;
    }
    QHashIterator<QString, QString> i(data);
    while(i.hasNext()) {
        i.next();
        TagLib::ID3v2::UserUrlLinkFrame *userurlframe = NULL;
        TagLib::ID3v2::TextIdentificationFrame *frame = NULL;
        TagLib::ID3v2::UrlLinkFrame *urlframe = NULL;
        TagLib::ID3v2::CommentsFrame *commframe = NULL;
        if(i.key() == "COMM") {
            if( !mtag->frameListMap()[i.key().toStdString().data()].isEmpty() )
                commframe = dynamic_cast< TagLib::ID3v2::CommentsFrame * >(
                                              mtag->frameListMap()[i.key().toStdString().data()].front() );
            if( !commframe )
            {
                commframe = new TagLib::ID3v2::CommentsFrame( i.key().toStdString().data() );
                mtag->addFrame( commframe );
            }
        }
        else if(i.key() == "WCOP") {
            if( !mtag->frameListMap()[i.key().toStdString().data()].isEmpty() )
                urlframe = dynamic_cast< TagLib::ID3v2::UrlLinkFrame * >(
                                              mtag->frameListMap()[i.key().toStdString().data()].front() );
            if( !urlframe )
            {
                urlframe = new TagLib::ID3v2::UrlLinkFrame( i.key().toStdString().data() );
                mtag->addFrame( urlframe );
            }
        }
        else if(i.key() == "WXXX") {
            if( !mtag->frameListMap()[i.key().toStdString().data()].isEmpty() )
                userurlframe = dynamic_cast< TagLib::ID3v2::UserUrlLinkFrame * >(
                                              mtag->frameListMap()[i.key().toStdString().data()].front() );
            if( !userurlframe )
            {
                userurlframe = new TagLib::ID3v2::UserUrlLinkFrame( i.key().toStdString().data() );
                mtag->addFrame( userurlframe );
            }

        }
        else {
            if( !mtag->frameListMap()[i.key().toStdString().data()].isEmpty() )
                frame = dynamic_cast< TagLib::ID3v2::TextIdentificationFrame * >(
                                              mtag->frameListMap()[i.key().toStdString().data()].front() );
            if( !frame )
            {
                frame = new TagLib::ID3v2::TextIdentificationFrame( i.key().toStdString().data() );
                mtag->addFrame( frame );
            }
        }
        // note: TagLib is smart enough to automatically set UTF8 encoding if needed.
        if(i.key() == "TYER")
            mtag->setYear(i.value().toUInt());
        else if(i.key() == "COMM")
            commframe->setText(TagLib::String(i.value().toUtf8().data(), TagLib::String::UTF8));
        else if(i.key() == "WXXX")
            userurlframe->setUrl(TagLib::String(i.value().toUtf8().data(), TagLib::String::UTF8));
        else if(i.key() == "WCOP")
            urlframe->setUrl(TagLib::String(i.value().toUtf8().data(), TagLib::String::UTF8));
        else
            frame->setText( TagLib::String(i.value().toUtf8().data(), TagLib::String::UTF8) );
            //frame->setText( i.value().toStdString().data() );
    }
    mf->save(TagLib::MPEG::File::ID3v2, false, 3);
    delete mf;
    return true;
}

bool TagHandler::removeId3v1Tags(const QString &filepath) {
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    TagLib::MPEG::File *mf= new TagLib::MPEG::File(encodedName, true);
    TagLib::ID3v1::Tag *mtag = mf->ID3v1Tag(true);
    if(mtag) {
        if(mf->hasID3v1Tag()) {
            mf->strip(TagLib::MPEG::File::ID3v1);
            delete mf;
            return true;
        }
    }
    delete mf;
    return false;
}

bool TagHandler::removeId3v2Tags(const QString &filepath) {
    QFileInfo info( filepath );
    QString path = info.absoluteFilePath();
    QByteArray fileName = QFile::encodeName(path);
    const char *encodedName = fileName.constData();
    TagLib::MPEG::File *mf= new TagLib::MPEG::File(encodedName, true);
    TagLib::ID3v2::Tag *mtag = mf->ID3v2Tag(true);
    if(mtag) {
        if(mf->hasID3v2Tag()) {
            mf->strip(TagLib::MPEG::File::ID3v2);
            delete mf;
            return true;
        }
    }
    delete mf;
    return false;
}

TrackInfo TagHandler::readShoutCastTagInfo(const char *metadata, const char *metaextra) {
    TrackInfo tif;
    tif.setShoutCast(true);
    if(!metadata) return tif;
    while (*metadata) {
        QString meta = QString(metadata);
        if(meta.startsWith("ICY 200 OK")) {
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-notice1")) {
            meta.remove("icy-notice1:");
            tif.setIcyField(ICY_NOTICE1, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-notice2")) {
            meta.remove("icy-notice2:");
            tif.setIcyField(ICY_NOTICE2, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-name")) {
            meta.remove("icy-name:");
            tif.setIcyField(ICY_NAME, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-genre")) {
            meta.remove("icy-genre:");
            tif.setIcyField(ICY_GENRE, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-url")) {
            meta.remove("icy-url:");
            tif.setIcyField(ICY_URL, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-pub")) {
            meta.remove("icy-pub:");
            tif.setIcyField(ICY_PUB, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-metaint")) {
            meta.remove("icy-metaint:");
            tif.setIcyField(ICY_METAINT, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else if(meta.startsWith("icy-br")) {
            meta.remove("icy-br:");
            tif.setIcyField(ICY_BR, meta);
            metadata += strlen(metadata) + 1;
            continue;
        }
        else {
            metadata += strlen(metadata) + 1;
            continue;
        }
    }
    if(metaextra) {
        QString extra = QString(metaextra).split(';').first();
        extra.remove("StreamTitle='");
        extra = extra.trimmed();
        extra.remove(extra.size()-1, 1);
        extra = extra.trimmed();
        tif.setIcyField(ICY_CURRENT, extra);
    }
    return tif;
}
