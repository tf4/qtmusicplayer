#ifndef DOCKDIALOGMANAGER_H
#define DOCKDIALOGMANAGER_H

#include <QPoint>
#include <QRect>
#include <windows.h>
#include <windef.h>
#include <w32api.h>
#include <qwindowdefs.h>

enum DOCKSIDES {DOCK_BELOW=0, DOCK_ABOVE=1, DOCK_LEFT=2, DOCK_RIGHT=3, NO_DOCK=4};
typedef struct {
    bool isDocked;
    int dockSide;
}DockState;

class DockDialogManager
{
public:
    DockDialogManager();
    void initDockState(QRect child, QRect parent);
    bool isDocked();
    int getDockSide();
    void updateDockState(QRect child, QRect parent);
    QPoint calcDockPosition(QRect child, QRect parent);
    void handleWin32MoveEvent(void *message, WId wid, QRect child, QRect parent);
    void updateDockedWinPos(WId wid, QRect child, QRect parent);

private:
    DockState dockState;
    QPoint initialFramePos;
    QPoint initialMousePos;
    QPoint currentMousePos;
    void forceHooks(int range, QRect child, QRect parent);
};

#endif // DOCKDIALOGMANAGER_H
