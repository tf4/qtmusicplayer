#ifndef PLAYLISTDIALOG_H
#define PLAYLISTDIALOG_H

#include <QDialog>
#include <QMenu>
#include <QTimer>
#include <QPoint>
#include <QList>
#include "trackinfo.h"
#include "utils/dockdialogmanager.h"

namespace Ui {
class PlaylistDialog;
}

class PlaylistDialog : public QDialog
{
    Q_OBJECT
public:
    explicit PlaylistDialog(QWidget *parent = 0);
    ~PlaylistDialog();
    void testFunc(const QPoint &point);


private:
    Ui::PlaylistDialog *ui;
    QString execpath;
    QTimer *listOperationProgressTimer;
    QAction *actionAddFiles;
    QAction *actionAddFolders;
    QAction *actionAddURL;
    QAction *actionSelectAll;
    QAction *actionSelectNone;
    QAction *actionInvertSelection;
    QAction *actionRemoveSelected;
    QAction *actionCropSelected;
    QAction *actionRemoveAll;
    QAction *actionRemoveMissing;
    QAction *actionRemoveDuplicates;
    QAction *actionSortByTitle;
    QAction *actionSortByFilename;
    QAction *actionSortByFilepath;
    QAction *actionReverseList;
    QAction *actionRandomizeList;
    QAction *actionViewFileInfo;
    QAction *actionViewPlaylistEntry;
    QAction *actionOpenPlaylist;
    QAction *actionSavePlaylist;
    QAction *actionNewPlaylist;
    QAction *actionCancelOperation;
    QAction *actionPlayItem;
    QList<int> shuffledList;
    QList<int> shuffleHistoryList;
    int shuffleHistoryCursor;
    QRect currentGeometry;
    bool framePressed;
    DockDialogManager *dock;
    //METHODS
    void setupActions();
    void runAddContextMenu(const QPoint &point);
    void runSelectContextMenu(const QPoint &point);
    void runRemoveContextMenu(const QPoint &point);
    void runMiscContextMenu(const QPoint &point);
    void runManageContextMenu(const QPoint &point);
    void runItemContextMenu(const QPoint &point);

signals:
    void signalPlaylistItemDoubleClicked(QString, TrackInfo);
    void signalFetchedNewItem(QString, TrackInfo);
    void signalMainBlockUserInput(bool);
    void signalLastSaveDone(int);
    void signalCloseEvent(bool b=false);

public slots:
    bool initializeDefaults();
    void saveDefaults();
    bool isBusy();
    void writeConfigIniFile();
    bool readConfigIniFile();
    void openPlaylist();
    void savePlaylist();
    void savePlaylistResult(bool success);
    void clearPlaylist();
    void addFilesInPlaylist();
    void addUrlInPlaylist();
    void openNewUrl();
    void openFolders();
    void enqueueFolder();
    void cancelListOperation();
    void viewBusyItemInfo();
    void updateProgressBar();
    void playNewFiles(QStringList files);
    void updateTotalTime(int totaltime);
    void updatePartialTime(int partialtime);
    void playlistItemDoubleClicked(QString filepath, TrackInfo tif);
    void getNextPlaylistItem(bool shuffle=false, bool repeatall=false);
    void getPreviousPlaylistItem(bool shuffle=false);
    void reshufflePlaylist(bool clear=true);
    void clearShuffledPlaylist();
    void blockUserInput(bool block);  
    void setUrlItemInfo(TrackInfo *tif);
    void showEvent(QShowEvent *ev);
    void hideEvent(QHideEvent *ev);
    void closeEvent(QCloseEvent *);
    bool event(QEvent *ev);
    bool nativeEvent(const QByteArray &eventType, void *message, long *result);
    void dragWindow(int x, int y);
    bool isDocked();
    void setDocked(bool value);
};

#endif // PLAYLISTDIALOG_H
