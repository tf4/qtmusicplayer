#ifndef URLDIALOG_H
#define URLDIALOG_H

#include <QDialog>
#include <QStringList>

namespace Ui {
class UrlDialog;
}

class UrlDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UrlDialog(QWidget *parent = 0);
    void onAccept();
    ~UrlDialog();

private:
    Ui::UrlDialog *ui;

signals:
    void signalUrlEntered(QStringList url);
};

#endif // URLDIALOG_H
