#include "editplaylistentrydialog.h"
#include "ui_editplaylistentrydialog.h"
#include <QFileDialog>
#include <QFileInfo>

EditPlaylistEntryDialog::EditPlaylistEntryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditPlaylistEntryDialog),
    entryrow(-1)
{
    ui->setupUi(this);
    connect(ui->toolButton, &QToolButton::clicked, this, &EditPlaylistEntryDialog::selectEntryFile);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &EditPlaylistEntryDialog::sendItemEntry);
}


void EditPlaylistEntryDialog::setOldEntry(QString entry) {ui->oldEntryLineEdit->setText(entry);}

void EditPlaylistEntryDialog::setNewEntry(QString entry) {ui->newEntryLineEdit->setText(entry);}

void EditPlaylistEntryDialog::setRow(int row) {entryrow = row;}

void EditPlaylistEntryDialog::sendItemEntry() {
    if(ui->newEntryLineEdit->text().isEmpty() || ui->newEntryLineEdit->text().isNull())
        emit this->signalItemEntryChanged(entryrow, ui->oldEntryLineEdit->text());
    else
        emit this->signalItemEntryChanged(entryrow, ui->newEntryLineEdit->text());
    this->accept();
}

void EditPlaylistEntryDialog::selectEntryFile() {
    QString newpath = QFileDialog::getOpenFileName(this, "New path", ".", "Audio files (*.mp3 *.wav *.wma)");
    newpath = QDir::toNativeSeparators(newpath);
    setNewEntry(newpath);
}

EditPlaylistEntryDialog::~EditPlaylistEntryDialog()
{
    delete ui;
}
