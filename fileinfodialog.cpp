#include "fileinfodialog.h"
#include "ui_fileinfodialog.h"
#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include "mmreg.h"

FileInfoDialog::FileInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileInfoDialog)
{
    ui->setupUi(this);
    this->setupInputMasks();
    tif = new TrackInfo();
    ui->extra1Label->setHidden(true);
    ui->extra2Label->setHidden(true);
    ui->moodLabel->setHidden(true);
    initID3v1ObjectHash();
    initID3v2ObjectHash();
    populateGenreWidgets();
    enableID3v1Fields(Qt::Unchecked);
    enableID3v2Fields(Qt::Unchecked);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &FileInfoDialog::saveChanges);
}

FileInfoDialog::~FileInfoDialog()
{
    delete ui;
}

void FileInfoDialog::fillDialog(QString filepath) {
    //TrackInfo tif;
    *tif = TagHandler::readID3v2TagInfo(filepath);
    fillBasicInfo(tif, filepath);
    if(tif->hasID3v2()) {
        fillID3v2(tif);
        tif->clearFields();
        *tif = TagHandler::readID3TagInfo(filepath);
        if(tif->hasID3v1())
            fillID3v1(tif);
    }
    else {
        fillID3v1(tif);
    }
    this->setupSignals();
}

void FileInfoDialog::fillID3v2(TrackInfo *t) {
    if(t->getField(TRACK_NUM).toInt() > 0 || t->getField(TRACK_NUM).contains("/"))
        ui->trackId3v2LineEdit->setText(t->getField(TRACK_NUM));
    if(t->getField(DISC_NUM).toInt() > 0 || t->getField(DISC_NUM).contains("/"))
        ui->discId3v2LineEdit->setText(t->getField(DISC_NUM));
    if(t->getField(TRACK_BPM).toInt() > 0)
        ui->bpmId3v2LineEdit->setText(t->getField(TRACK_BPM));
    ui->albumId3v2LineEdit->setText(t->getField(ALBUM));
    ui->artistId3v2LineEdit->setText(t->getField(ARTIST));
    ui->albumArtistId3v2LineEdit->setText(t->getField(ALBUM_ARTIST));
    ui->titleId3v2LineEdit->setText(t->getField(TITLE));
    if(t->getField(TRACK_YEAR).toInt() > 0)
        ui->yearId3v2LineEdit->setText(t->getField(TRACK_YEAR));
    QString g = t->getField(TRACK_GENRE);
    int i = ui->genreId3v2ComboBox->findText(g);
    if(i == -1) {
        ui->genreId3v2ComboBox->addItem(g);
        ui->genreId3v2ComboBox->setCurrentIndex(ui->genreId3v2ComboBox->findText(g));
    }
    else {
        ui->genreId3v2ComboBox->insertItem(1, g);
        ui->genreId3v2ComboBox->setCurrentIndex(1);
    }
    ui->commentsId3v2PlainTextEdit->setPlainText(t->getField(TRACK_COMMENTS));
    ui->composerId3v2LineEdit->setText(t->getField(TRACK_COMPOSER));
    ui->publisherId3v2LineEdit->setText(t->getField(TRACK_PUBLISHER));
    ui->origArtistId3v2LineEdit->setText(t->getField(TRACK_ORIGARTIST));
    ui->copyrightId3v2LineEdit->setText(t->getField(TRACK_COPYRIGHT));
    ui->encodedId3v2LineEdit->setText(t->getField(TRACK_ENCODED_BY));
    ui->urlId3v2LineEdit->setText(t->getField(TRACK_URL));
    enableID3v2Fields(Qt::Checked);
}

void FileInfoDialog::fillID3v1(TrackInfo *t) {
    if(t->getField(TRACK_NUM).toInt() > 0 || t->getField(TRACK_NUM).contains("/"))
        ui->trackId3v1LineEdit->setText(t->getField(TRACK_NUM));
    ui->albumId3v1LineEdit->setText(t->getField(ALBUM));
    ui->artistId3v1LineEdit->setText(t->getField(ARTIST));
    ui->titleId3v1LineEdit->setText(t->getField(TITLE));
    if(t->getField(TRACK_YEAR).toInt() > 0)
        ui->yearId3v1LineEdit->setText(t->getField(TRACK_YEAR));
    QString g = t->getField(TRACK_GENRE);
    int i = ui->genreId3v1ComboBox->findText(g);
    if(i == -1) {
        ui->genreId3v1ComboBox->addItem(g);
        ui->genreId3v1ComboBox->setCurrentIndex(ui->genreId3v1ComboBox->findText(g));
    }
    else
        ui->genreId3v1ComboBox->setCurrentIndex(i);
    ui->commentsId3v1PlainTextEdit->setText(t->getField(TRACK_COMMENTS));
    enableID3v1Fields(Qt::Checked);
}

void FileInfoDialog::fillBasicInfo(TrackInfo *t, QString filepath) {
    ui->entryLineEdit->setText(QDir::toNativeSeparators(filepath));
    if(t->getField(TRACK_NUM).toInt() > 0 || t->getField(TRACK_NUM).contains("/"))
        ui->trackLineEdit->setText(t->getField(TRACK_NUM));
    if(t->getField(DISC_NUM).toInt() > 0 || t->getField(DISC_NUM).contains("/"))
        ui->discLineEdit->setText(t->getField(DISC_NUM));
    if(t->getField(TRACK_BPM).toInt() > 0)
        ui->bpmLineEdit->setText(t->getField(TRACK_BPM));
    ui->albumLineEdit->setText(t->getField(ALBUM));
    ui->artistLineEdit->setText(t->getField(ARTIST));
    ui->albumArtistLineEdit->setText(t->getField(ALBUM_ARTIST));
    ui->titleLineEdit->setText(t->getField(TITLE));
    if(t->getField(TRACK_YEAR).toInt() > 0)
        ui->yearLineEdit->setText(t->getField(TRACK_YEAR));
    QString g = t->getField(TRACK_GENRE);
    int i = ui->genreComboBox->findText(g);
    if(i == -1) {
        ui->genreComboBox->addItem(g);
        ui->genreComboBox->setCurrentIndex(ui->genreComboBox->findText(g));
    }
    else
        ui->genreComboBox->setCurrentIndex(i);
    ui->commentPlainTextEdit->setPlainText(t->getField(TRACK_COMMENTS));
    ui->composerLineEdit->setText(t->getField(TRACK_COMPOSER));
    ui->publisherLineEdit->setText(t->getField(TRACK_PUBLISHER));
    ui->fileSizeLabel->setText(ui->fileSizeLabel->text() + QString::number(QFileInfo(filepath).size()) + " bytes");
    ui->lengthLabel->setText(ui->lengthLabel->text() + t->getField(TRACK_LENGTH) + " seconds");
    ui->bitrateLabel->setText(ui->bitrateLabel->text() + t->getField(BITRATE) + " kbps");
    QString ch = "";
    if(t->getField(TRACK_CHANNELS) == "1") ch = "Mono";
    else if(t->getField(TRACK_CHANNELS) == "2") ch = "Stereo";
    ui->sampleRateLabel->setText(ui->sampleRateLabel->text() + t->getField(SAMPLE_RATE) + " Hz " + ch);
    ui->formatLabel->setText(ui->formatLabel->text() + this->getFormat(filepath));
    ui->encodedByLabel->setText(ui->encodedByLabel->text() + t->getField(TRACK_ENCODED_BY));
}

void FileInfoDialog::saveChanges() {
    qDebug() << "file info closing";
    this->saveID3v1Changes();
    this->saveID3v2Changes();
    this->accept();
}

void FileInfoDialog::saveID3v1Changes() {
    if(!ui->id3v1CheckBox->isChecked()) {
        //remove Id3v1 tags from file
        TagHandler::removeId3v1Tags(ui->entryLineEdit->text());
        return;
    }
    QHash<ID3v1Identifiers, QString> tosave;
    QHashIterator<QString, ID3v1Identifiers> i(editedID3v1Fields);
    while (i.hasNext()) {
        i.next();
        if(i.key() == ui->genreId3v1ComboBox->objectName()) {
            tosave.insert(i.value(), findChild<QComboBox*>(i.key())->currentText());
        }
        else
            tosave.insert(i.value(), findChild<QLineEdit*>(i.key())->text());
    }
    TagHandler::writeID3TagInfo(tosave, ui->entryLineEdit->text());
}


void FileInfoDialog::saveID3v2Changes() {
    if(!ui->id3v2CheckBox->isChecked()) {
        //remove Id3v1 tags from file
        TagHandler::removeId3v2Tags(ui->entryLineEdit->text());
        return;
    }
    QHash<QString, QString> tosave;
    QHashIterator<QString, QString> i(editedID3v2Fields);
    while (i.hasNext()) {
        i.next();
        if(i.key() == ui->commentsId3v2PlainTextEdit->objectName()) {
            tosave.insert(i.value(), findChild<QPlainTextEdit*>(i.key())->toPlainText());
        }
        else if(i.key() == ui->genreId3v2ComboBox->objectName()) {
            tosave.insert(i.value(), findChild<QComboBox*>(i.key())->currentText());
        }
        else
            tosave.insert(i.value(), findChild<QLineEdit*>(i.key())->text());
    }
    TagHandler::writeID3v2TagInfo(tosave, ui->entryLineEdit->text());
}


void FileInfoDialog::enableID3v1Fields(int state) {
    ui->id3v1CheckBox->setCheckState(static_cast<Qt::CheckState>(state));
    ui->trackId3v1LineEdit->setEnabled(static_cast<bool>(state));
    ui->titleId3v1LineEdit->setEnabled(static_cast<bool>(state));
    ui->artistId3v1LineEdit->setEnabled(static_cast<bool>(state));
    ui->albumId3v1LineEdit->setEnabled(static_cast<bool>(state));
    ui->yearId3v1LineEdit->setEnabled(static_cast<bool>(state));
    ui->genreId3v1ComboBox->setEnabled(static_cast<bool>(state));
    ui->commentsId3v1PlainTextEdit->setEnabled(static_cast<bool>(state));
    ui->copytoId3v2PushButton->setEnabled(static_cast<bool>(state));
    if(!ui->id3v1CheckBox->isChecked() && !ui->id3v2CheckBox->isChecked()) {
        clearBasicInfo();
        return;
    }
    if(ui->id3v1CheckBox->isChecked() && !ui->id3v2CheckBox->isChecked()) {
        this->clearBasicInfo();
        this->copyID3v1toBasic();
        return;
    }
    if(!ui->id3v1CheckBox->isChecked() && ui->id3v2CheckBox->isChecked()) {
        this->clearBasicInfo();
        this->copyID3v2toBasic();
        return;
    }
    if(ui->id3v1CheckBox->isChecked() && ui->id3v2CheckBox->isChecked()) {
        this->copyID3v1toBasic();
        this->copyID3v2toBasic();
        return;
    }
}


void FileInfoDialog::enableID3v2Fields(int state) {
    ui->id3v2CheckBox->setCheckState(static_cast<Qt::CheckState>(state));
    ui->trackId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->discId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->bpmId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->titleId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->artistId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->albumId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->albumArtistId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->yearId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->genreId3v2ComboBox->setEnabled(static_cast<bool>(state));
    ui->commentsId3v2PlainTextEdit->setEnabled(static_cast<bool>(state));
    ui->composerId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->publisherId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->origArtistId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->copyrightId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->urlId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->encodedId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->albumGainId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->trackGainId3v2LineEdit->setEnabled(static_cast<bool>(state));
    ui->copyToId3v1PushButton->setEnabled(static_cast<bool>(state));
    if(ui->id3v2CheckBox->isChecked()) {
        this->copyID3v2toBasic();
    }
    if(!ui->id3v2CheckBox->isChecked() && ui->id3v1CheckBox->isChecked()) {
        this->clearBasicInfo();
        this->copyID3v1toBasic();
        return;
    }
    if(!ui->id3v2CheckBox->isChecked() && !ui->id3v1CheckBox->isChecked()) {
        clearBasicInfo();
        return;
    }
}


void FileInfoDialog::fieldEdited(const QString &text){
    Q_UNUSED(text);
    QObject *obj = sender();
    obj->blockSignals(true);
    QString objectname = obj->objectName();
    qDebug() << objectname + QString(" edited");
    if(objectname.contains("LineEdit")) {
        findChild<QLineEdit*>(objectname.replace("LineEdit", "Id3v2LineEdit"))->setText(((QLineEdit*)obj)->text());
        if(!editedID3v2Fields.contains(objectname))
            editedID3v2Fields.insert(objectname, id3v2ObjectHash.value(objectname));
    }
    if(objectname.contains("ComboBox")) {
        ui->genreId3v2ComboBox->setCurrentText(ui->genreComboBox->currentText());
        if(!editedID3v2Fields.contains(ui->genreId3v2ComboBox->objectName()))
            editedID3v2Fields.insert(ui->genreId3v2ComboBox->objectName(),
                                     id3v2ObjectHash.value(ui->genreId3v2ComboBox->objectName()));
    }
    if(!ui->id3v2CheckBox->isChecked())
        enableID3v2Fields(Qt::Checked);

    obj->blockSignals(false);
}


void FileInfoDialog::id3v1FieldEdited(const QString &text) {
    Q_UNUSED(text);
    QObject *obj = sender();
    obj->blockSignals(true);
    QString objectname = obj->objectName();
    if(!editedID3v1Fields.contains(objectname)) {
        editedID3v1Fields.insert(objectname, id3v1ObjectHash[objectname]);
        //obj->blockSignals(true);
    }
    qDebug() << objectname + QString(" edited");
    if(objectname.contains("LineEdit")) {
        if(findChild<QLineEdit*>(objectname.replace("Id3v1", "Id3v2"))->text().isEmpty() ||
                !ui->id3v2CheckBox->isChecked()) {
            findChild<QLineEdit*>(objectname.remove("Id3v2"))->setText(((QLineEdit*)obj)->text());
        }
        obj->blockSignals(false);
        return;
    }
    if(objectname.contains("ComboBox")) {
        if(!ui->id3v2CheckBox->isChecked() || ui->genreId3v2ComboBox->currentText().isEmpty()) {
            ui->genreComboBox->blockSignals(true);
            ui->genreComboBox->setCurrentText(ui->genreId3v1ComboBox->currentText());
            ui->genreComboBox->blockSignals(false);
        }
        obj->blockSignals(false);
        return;
    }
    if(objectname.contains("comment")) {
        if(!ui->id3v2CheckBox->isChecked() || ui->commentsId3v2PlainTextEdit->toPlainText().isEmpty()) {
            ui->commentPlainTextEdit->blockSignals(true);
            ui->commentPlainTextEdit->setPlainText(ui->commentsId3v1PlainTextEdit->text());
            ui->commentPlainTextEdit->blockSignals(false);
        }
        obj->blockSignals(false);
        return;
    }
    obj->blockSignals(false);
}


void FileInfoDialog::id3v2FieldEdited(const QString &text) {
    Q_UNUSED(text);
    QObject *obj = sender();
    QString objectname = obj->objectName();
    if(!editedID3v2Fields.contains(objectname)) {
        editedID3v2Fields.insert(objectname, id3v2ObjectHash.value(objectname));
        //obj->blockSignals(true);
    }
    qDebug() << objectname + QString(" edited");
}


void FileInfoDialog::commentsFieldEdited() {
    QObject *obj = sender();
    QString objectname = obj->objectName();
    if(objectname == ui->commentsId3v2PlainTextEdit->objectName()) {
        if(!editedID3v2Fields.contains(objectname)) {
            editedID3v2Fields.insert(objectname, id3v2ObjectHash.value(objectname));
        }
        ui->commentPlainTextEdit->blockSignals(true);
        ui->commentPlainTextEdit->setPlainText(ui->commentsId3v2PlainTextEdit->toPlainText());
        ui->commentPlainTextEdit->blockSignals(false);
    }
    if(objectname == ui->commentPlainTextEdit->objectName()) {
        if(!editedID3v2Fields.contains(ui->commentsId3v2PlainTextEdit->objectName())) {
            editedID3v2Fields.insert(ui->commentsId3v2PlainTextEdit->objectName(), id3v2ObjectHash.value(ui->commentsId3v2PlainTextEdit->objectName()));
        }
        ui->commentsId3v2PlainTextEdit->blockSignals(true);
        ui->commentsId3v2PlainTextEdit->setPlainText(ui->commentPlainTextEdit->toPlainText());
        ui->commentsId3v2PlainTextEdit->blockSignals(false);
    }

    qDebug() << "comments edited";
}

QString FileInfoDialog::getFormat(QString filepath) {
    QString format;
    HSTREAM stream;
    void *wfm = malloc(sizeof(WAVEFORMATEX));
    stream = BASS_StreamCreateFile(false, filepath.toStdWString().c_str(), 0, 0, BASS_MUSIC_PRESCAN|BASS_STREAM_DECODE);
    BASS_CHANNELINFO *cinfo = new BASS_CHANNELINFO;
    BASS_ChannelGetInfo(stream, cinfo);
    //qDebug() << cinfo->ctype;
    switch(cinfo->ctype) {
    case BASS_CTYPE_STREAM_MP1:
        format = "MPEG-1 layer 1";
        break;
    case BASS_CTYPE_STREAM_MP2:
        format = "MPEG-1 layer 2";
        break;
    case BASS_CTYPE_STREAM_MP3:
        format = "MPEG-1 layer 3";
        break;
    case BASS_CTYPE_STREAM_WAV:
        format = "WAVE";
        break;
    case BASS_CTYPE_STREAM_MF:
        wfm = (void*)(BASS_ChannelGetTags(stream, BASS_TAG_WAVEFORMAT));
        format = getWmaFormat(wfm);
        break;
    default:
        format = "Unknown";
    }

    BASS_StreamFree(stream);
    return format;
}

QString FileInfoDialog::getWmaFormat(void *wfm) {
    qDebug() << ((WAVEFORMATEX*)wfm)->wFormatTag;
    switch(((WAVEFORMATEX*)wfm)->wFormatTag){
    case 0x0161:
        return "WMA";
    case 0x0162:
        return "WMA pro";
    case 0x0163:
        return "WMA lossless";
    case 0x1610:
        return "AAC";
    case WAVE_FORMAT_PCM:
        return "PCM";
    case WAVE_FORMAT_MPEG:
        if(((MPEG1WAVEFORMAT*)wfm)->fwHeadLayer == ACM_MPEG_LAYER1)
            return "MPEG-1 layer 1 (WMA)";
        else if(((MPEG1WAVEFORMAT*)wfm)->fwHeadLayer == ACM_MPEG_LAYER2)
            return "MPEG-1 layer 2 (WMA)";
        else if(((MPEG1WAVEFORMAT*)wfm)->fwHeadLayer == ACM_MPEG_LAYER3)
            return "MPEG-1 layer 3 (WMA)";
        else return "Unknown";
    case WAVE_FORMAT_MPEGLAYER3:
        return "MPEG layer 3 (WMA)";
    case WAVE_FORMAT_EXTENSIBLE:
        return "Multi-channel audio";
    default:
        qDebug() << "fdasf";
        return "Unknown";
    }
}

void FileInfoDialog::setupSignals() {
    //Basic info fields
    connect(ui->trackLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->discLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->bpmLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->artistLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->titleLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->artistLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->albumLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->albumArtistLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->yearLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->genreComboBox, &QComboBox::currentTextChanged, this, &FileInfoDialog::fieldEdited);
    connect(ui->commentPlainTextEdit, &QPlainTextEdit::textChanged, this, &FileInfoDialog::commentsFieldEdited);
    connect(ui->composerLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->publisherLineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    //ID3v1 fields
    connect(ui->trackId3v1LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v1FieldEdited);
    connect(ui->titleId3v1LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v1FieldEdited);
    connect(ui->artistId3v1LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v1FieldEdited);
    connect(ui->albumId3v1LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v1FieldEdited);
    connect(ui->yearId3v1LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v1FieldEdited);
    connect(ui->genreId3v1ComboBox, &QComboBox::currentTextChanged, this, &FileInfoDialog::id3v1FieldEdited);
    connect(ui->commentsId3v1PlainTextEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v1FieldEdited);
    //ID3v2 fields
    connect(ui->trackId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->trackId3v2LineEdit, &QLineEdit::textEdited, ui->trackLineEdit, &QLineEdit::setText);
    connect(ui->discId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->discId3v2LineEdit, &QLineEdit::textEdited, ui->discLineEdit, &QLineEdit::setText);
    connect(ui->bpmId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->bpmId3v2LineEdit, &QLineEdit::textEdited, ui->bpmLineEdit, &QLineEdit::setText);
    connect(ui->titleId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->titleId3v2LineEdit, &QLineEdit::textEdited, ui->titleLineEdit, &QLineEdit::setText);
    connect(ui->artistId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->artistId3v2LineEdit, &QLineEdit::textEdited, ui->artistLineEdit, &QLineEdit::setText);
    connect(ui->albumId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->albumId3v2LineEdit, &QLineEdit::textEdited, ui->albumLineEdit, &QLineEdit::setText);
    connect(ui->albumArtistId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->albumArtistId3v2LineEdit, &QLineEdit::textEdited, ui->albumArtistLineEdit, &QLineEdit::setText);
    connect(ui->yearId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->yearId3v2LineEdit, &QLineEdit::textEdited, ui->yearLineEdit, &QLineEdit::setText);
    connect(ui->composerId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->composerId3v2LineEdit, &QLineEdit::textEdited, ui->composerLineEdit, &QLineEdit::setText);
    connect(ui->publisherId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->origArtistId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->copyrightId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->urlId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->encodedId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->encodedId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::updateEncLabel);
    connect(ui->trackGainId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->albumGainId3v2LineEdit, &QLineEdit::textEdited, this, &FileInfoDialog::fieldEdited);
    connect(ui->genreId3v2ComboBox, &QComboBox::currentTextChanged, this, &FileInfoDialog::id3v2FieldEdited);
    connect(ui->genreId3v2ComboBox, &QComboBox::currentTextChanged, ui->genreComboBox, &QComboBox::setCurrentText);
    connect(ui->commentsId3v2PlainTextEdit, &QPlainTextEdit::textChanged, this, &FileInfoDialog::commentsFieldEdited);
    connect(ui->id3v1CheckBox, &QCheckBox::stateChanged, this, &FileInfoDialog::enableID3v1Fields);
    connect(ui->id3v2CheckBox, &QCheckBox::stateChanged, this, &FileInfoDialog::enableID3v2Fields);
    connect(ui->copyToId3v1PushButton, &QPushButton::clicked, this, &FileInfoDialog::copyID3v2toId3v1);
    connect(ui->copytoId3v2PushButton, &QPushButton::clicked, this, &FileInfoDialog::copyID3v1toId3v2);
}

void FileInfoDialog::populateGenreWidgets() {
    QStringList genres = TagHandler::getStandardGenreList();
    ui->genreComboBox->addItems(genres);
    ui->genreComboBox->setCurrentIndex(0);
    ui->genreId3v1ComboBox->addItems(genres);
    ui->genreId3v1ComboBox->setCurrentIndex(0);
    ui->genreId3v2ComboBox->addItems(genres);
    ui->genreId3v2ComboBox->setCurrentIndex(0);
}

void FileInfoDialog::clearBasicInfo() {
    ui->trackLineEdit->clear();
    ui->titleLineEdit->clear();
    ui->artistLineEdit->clear();
    ui->albumLineEdit->clear();
    ui->genreComboBox->blockSignals(true);
    ui->genreComboBox->setCurrentIndex(0);
    ui->genreComboBox->blockSignals(false);
    ui->yearLineEdit->clear();
    ui->albumArtistLineEdit->clear();
    ui->commentPlainTextEdit->blockSignals(true);
    ui->commentPlainTextEdit->clear();
    ui->commentPlainTextEdit->blockSignals(false);
    ui->composerLineEdit->clear();
    ui->encodedByLabel->setText("Encoded by: ");
}

void FileInfoDialog::initID3v1ObjectHash() {
    id3v1ObjectHash.insert(ui->trackId3v1LineEdit->objectName(), ID3_TRACK);
    id3v1ObjectHash.insert(ui->titleId3v1LineEdit->objectName(), ID3_TITLE);
    id3v1ObjectHash.insert(ui->artistId3v1LineEdit->objectName(), ID3_ARTIST);
    id3v1ObjectHash.insert(ui->albumId3v1LineEdit->objectName(), ID3_ALBUM);
    id3v1ObjectHash.insert(ui->yearId3v1LineEdit->objectName(), ID3_YEAR);
    id3v1ObjectHash.insert(ui->genreId3v1ComboBox->objectName(), ID3_GENRE);
    id3v1ObjectHash.insert(ui->commentsId3v1PlainTextEdit->objectName(), ID3_COMMENTS);
}

void FileInfoDialog::initID3v2ObjectHash() {
    id3v2ObjectHash.insert(ui->trackId3v2LineEdit->objectName(), QString("TRCK"));
    id3v2ObjectHash.insert(ui->discId3v2LineEdit->objectName(), QString("TPOS"));
    id3v2ObjectHash.insert(ui->titleId3v2LineEdit->objectName(), QString("TIT2"));
    id3v2ObjectHash.insert(ui->artistId3v2LineEdit->objectName(), QString("TPE1"));
    id3v2ObjectHash.insert(ui->albumId3v2LineEdit->objectName(), QString("TALB"));
    id3v2ObjectHash.insert(ui->albumArtistId3v2LineEdit->objectName(), QString("TPE2"));
    id3v2ObjectHash.insert(ui->yearId3v2LineEdit->objectName(), QString("TYER"));
    id3v2ObjectHash.insert(ui->genreId3v2ComboBox->objectName(), QString("TCON"));
    id3v2ObjectHash.insert(ui->composerId3v2LineEdit->objectName(), QString("TCOM"));
    id3v2ObjectHash.insert(ui->publisherId3v2LineEdit->objectName(), QString("TPUB"));
    id3v2ObjectHash.insert(ui->origArtistId3v2LineEdit->objectName(), QString("TOPE"));
    id3v2ObjectHash.insert(ui->copyrightId3v2LineEdit->objectName(), QString("WCOP"));
    id3v2ObjectHash.insert(ui->urlId3v2LineEdit->objectName(), QString("WXXX"));
    id3v2ObjectHash.insert(ui->encodedId3v2LineEdit->objectName(), QString("TENC"));
    id3v2ObjectHash.insert(ui->bpmId3v2LineEdit->objectName(), QString("TBPM"));
    id3v2ObjectHash.insert(ui->commentsId3v2PlainTextEdit->objectName(), QString("COMM"));

}

void FileInfoDialog::updateEncLabel(const QString &text) {
    ui->encodedByLabel->setText("Encoded by: " + text);
}

void FileInfoDialog::copyID3v1toBasic() {
    ui->trackLineEdit->setText(ui->trackId3v1LineEdit->text());
    ui->titleLineEdit->setText(ui->titleId3v1LineEdit->text());
    ui->artistLineEdit->setText(ui->artistId3v1LineEdit->text());
    ui->albumLineEdit->setText(ui->albumId3v1LineEdit->text());
    ui->yearLineEdit->setText(ui->yearId3v1LineEdit->text());
    ui->genreComboBox->blockSignals(true);
    ui->genreComboBox->setCurrentText(ui->genreId3v1ComboBox->currentText());
    ui->genreComboBox->blockSignals(false);
    ui->commentPlainTextEdit->blockSignals(true);
    ui->commentPlainTextEdit->setPlainText(ui->commentsId3v1PlainTextEdit->text());
    ui->commentPlainTextEdit->blockSignals(false);
}

void FileInfoDialog::copyID3v2toBasic() {
    if(!ui->trackId3v2LineEdit->text().isEmpty())
        ui->trackLineEdit->setText(ui->trackId3v2LineEdit->text());
    if(!ui->titleId3v2LineEdit->text().isEmpty())
        ui->titleLineEdit->setText(ui->titleId3v2LineEdit->text());
    if(!ui->artistId3v2LineEdit->text().isEmpty())
        ui->artistLineEdit->setText(ui->artistId3v2LineEdit->text());
    if(!ui->albumId3v2LineEdit->text().isEmpty())
        ui->albumLineEdit->setText(ui->albumId3v2LineEdit->text());
    if(!ui->yearId3v2LineEdit->text().isEmpty())
        ui->yearLineEdit->setText(ui->yearId3v2LineEdit->text());
    if(!ui->genreId3v2ComboBox->currentText().isEmpty()) {
        ui->genreComboBox->blockSignals(true);
        ui->genreComboBox->setCurrentText(ui->genreId3v2ComboBox->currentText());
        ui->genreComboBox->blockSignals(false);
    }
    if(!ui->commentsId3v2PlainTextEdit->toPlainText().isEmpty()) {
        ui->commentPlainTextEdit->blockSignals(true);
        ui->commentPlainTextEdit->setPlainText(ui->commentsId3v2PlainTextEdit->toPlainText());
        ui->commentPlainTextEdit->blockSignals(false);
    }
    ui->albumArtistLineEdit->setText(ui->albumArtistId3v2LineEdit->text());
    ui->composerLineEdit->setText(ui->composerId3v2LineEdit->text());
    ui->publisherLineEdit->setText(ui->publisherId3v2LineEdit->text());
    this->updateEncLabel(ui->encodedId3v2LineEdit->text());
}

void FileInfoDialog::copyID3v1toId3v2() {
    ui->trackId3v2LineEdit->setText(ui->trackId3v1LineEdit->text());
    if(!editedID3v2Fields.contains(ui->trackId3v2LineEdit->objectName()))
        editedID3v2Fields.insert(ui->trackId3v2LineEdit->objectName(), id3v2ObjectHash.value(ui->trackId3v2LineEdit->objectName()));
    ui->titleId3v2LineEdit->setText(ui->titleId3v1LineEdit->text());
    if(!editedID3v2Fields.contains(ui->titleId3v2LineEdit->objectName()))
        editedID3v2Fields.insert(ui->titleId3v2LineEdit->objectName(), id3v2ObjectHash.value(ui->titleId3v2LineEdit->objectName()));
    ui->artistId3v2LineEdit->setText(ui->artistId3v1LineEdit->text());
    if(!editedID3v2Fields.contains(ui->artistId3v2LineEdit->objectName()))
        editedID3v2Fields.insert(ui->artistId3v2LineEdit->objectName(), id3v2ObjectHash.value(ui->artistId3v2LineEdit->objectName()));
    ui->albumId3v2LineEdit->setText(ui->albumId3v1LineEdit->text());
    if(!editedID3v2Fields.contains(ui->albumId3v2LineEdit->objectName()))
        editedID3v2Fields.insert(ui->albumId3v2LineEdit->objectName(), id3v2ObjectHash.value(ui->albumId3v2LineEdit->objectName()));
    ui->yearId3v2LineEdit->setText(ui->yearId3v1LineEdit->text());
    if(!editedID3v2Fields.contains(ui->yearId3v2LineEdit->objectName()))
        editedID3v2Fields.insert(ui->yearId3v2LineEdit->objectName(), id3v2ObjectHash.value(ui->yearId3v2LineEdit->objectName()));
    ui->genreId3v2ComboBox->blockSignals(true);
    ui->genreId3v2ComboBox->setCurrentText(ui->genreId3v1ComboBox->currentText());
    ui->genreId3v2ComboBox->blockSignals(false);
    if(!editedID3v2Fields.contains(ui->genreId3v2ComboBox->objectName()))
        editedID3v2Fields.insert(ui->genreId3v2ComboBox->objectName(), id3v2ObjectHash.value(ui->genreId3v2ComboBox->objectName()));
    ui->commentsId3v2PlainTextEdit->blockSignals(true);
    ui->commentsId3v2PlainTextEdit->setPlainText(ui->commentsId3v1PlainTextEdit->text());
    ui->commentsId3v2PlainTextEdit->blockSignals(false);
    if(!editedID3v2Fields.contains(ui->commentsId3v2PlainTextEdit->objectName()))
        editedID3v2Fields.insert(ui->commentsId3v2PlainTextEdit->objectName(), id3v2ObjectHash.value(ui->commentsId3v2PlainTextEdit->objectName()));
    if(!ui->id3v2CheckBox->isChecked())
        enableID3v2Fields(Qt::Checked);
    this->copyID3v2toBasic();
}

void FileInfoDialog::copyID3v2toId3v1() {
    ui->trackId3v1LineEdit->setText(ui->trackId3v2LineEdit->text());
    if(!editedID3v1Fields.contains(ui->trackId3v1LineEdit->objectName()))
        editedID3v1Fields.insert(ui->trackId3v1LineEdit->objectName(), id3v1ObjectHash[ui->trackId3v1LineEdit->objectName()]);
    ui->titleId3v1LineEdit->setText(ui->titleId3v2LineEdit->text());
    if(!editedID3v1Fields.contains(ui->titleId3v1LineEdit->objectName()))
        editedID3v1Fields.insert(ui->titleId3v1LineEdit->objectName(), id3v1ObjectHash[ui->titleId3v1LineEdit->objectName()]);
    ui->artistId3v1LineEdit->setText(ui->artistId3v2LineEdit->text());
    if(!editedID3v1Fields.contains(ui->artistId3v1LineEdit->objectName()))
        editedID3v1Fields.insert(ui->artistId3v1LineEdit->objectName(), id3v1ObjectHash[ui->artistId3v1LineEdit->objectName()]);
    ui->albumId3v1LineEdit->setText(ui->albumId3v2LineEdit->text());
    if(!editedID3v1Fields.contains(ui->albumId3v1LineEdit->objectName()))
        editedID3v1Fields.insert(ui->albumId3v1LineEdit->objectName(), id3v1ObjectHash[ui->albumId3v1LineEdit->objectName()]);
    ui->yearId3v1LineEdit->setText(ui->yearId3v2LineEdit->text());
    if(!editedID3v1Fields.contains(ui->yearId3v1LineEdit->objectName()))
        editedID3v1Fields.insert(ui->yearId3v1LineEdit->objectName(), id3v1ObjectHash[ui->yearId3v1LineEdit->objectName()]);
    ui->genreId3v1ComboBox->blockSignals(true);
    ui->genreId3v1ComboBox->setCurrentText(ui->genreId3v2ComboBox->currentText());
    ui->genreId3v1ComboBox->blockSignals(false);
    if(!editedID3v1Fields.contains(ui->genreId3v1ComboBox->objectName()))
        editedID3v1Fields.insert(ui->genreId3v1ComboBox->objectName(), id3v1ObjectHash[ui->genreId3v1ComboBox->objectName()]);
    ui->commentsId3v1PlainTextEdit->blockSignals(true);
    ui->commentsId3v1PlainTextEdit->setText(ui->commentsId3v2PlainTextEdit->toPlainText());
    ui->commentsId3v1PlainTextEdit->blockSignals(false);
    if(!editedID3v1Fields.contains(ui->commentsId3v1PlainTextEdit->objectName()))
        editedID3v1Fields.insert(ui->commentsId3v1PlainTextEdit->objectName(), id3v1ObjectHash[ui->commentsId3v1PlainTextEdit->objectName()]);
    if(!ui->id3v1CheckBox->isChecked())
        enableID3v1Fields(Qt::Checked);
}

void FileInfoDialog::setupID3v1Signals(bool val) {
    //Possibly not necessary
    if(val) {
        connect(ui->trackId3v1LineEdit, &QLineEdit::textEdited, ui->trackLineEdit, &QLineEdit::setText);
        connect(ui->titleId3v1LineEdit, &QLineEdit::textEdited, ui->titleLineEdit, &QLineEdit::setText);
        connect(ui->artistId3v1LineEdit, &QLineEdit::textEdited, ui->artistLineEdit, &QLineEdit::setText);
        connect(ui->albumId3v1LineEdit, &QLineEdit::textEdited, ui->albumLineEdit, &QLineEdit::setText);
        connect(ui->yearId3v1LineEdit, &QLineEdit::textEdited, ui->yearLineEdit, &QLineEdit::setText);
        connect(ui->genreId3v1ComboBox, &QComboBox::currentTextChanged, ui->genreComboBox, &QComboBox::setCurrentText);
        connect(ui->commentsId3v1PlainTextEdit, &QLineEdit::textEdited,
                ui->commentPlainTextEdit, &QPlainTextEdit::setPlainText);
    }
    else {
        ui->trackId3v1LineEdit->disconnect(ui->trackLineEdit);
        ui->titleId3v1LineEdit->disconnect(ui->titleLineEdit);
        ui->artistId3v1LineEdit->disconnect(ui->artistLineEdit);
        ui->albumId3v1LineEdit->disconnect(ui->albumLineEdit);
        ui->yearId3v1LineEdit->disconnect(ui->yearLineEdit);
        ui->genreId3v1ComboBox->disconnect(ui->genreComboBox);
        ui->commentsId3v1PlainTextEdit->disconnect(ui->commentPlainTextEdit);
    }
}

void FileInfoDialog::setupInputMasks() {
    ui->yearLineEdit->setInputMask("0000");
    ui->yearId3v1LineEdit->setInputMask("0000");
    ui->yearId3v2LineEdit->setInputMask("0000");
    ui->trackId3v1LineEdit->setInputMask("00000");
}
