#ifndef EDITPLAYLISTENTRYDIALOG_H
#define EDITPLAYLISTENTRYDIALOG_H

#include <QDialog>

namespace Ui {
class EditPlaylistEntryDialog;
}

class EditPlaylistEntryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditPlaylistEntryDialog(QWidget *parent = 0);
    void setOldEntry(QString entry);
    void setNewEntry(QString entry);
    void selectEntryFile();
    void setRow(int row);
    void sendItemEntry();
    ~EditPlaylistEntryDialog();

private:
    Ui::EditPlaylistEntryDialog *ui;
    int entryrow;

signals:
    void signalItemEntryChanged(int, QString);
};

#endif // EDITPLAYLISTENTRYDIALOG_H
