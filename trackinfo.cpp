#include "trackinfo.h"
#include <QtDebug>

TrackInfo::TrackInfo() :
    id3v1(false), id3v2(false), ape(false), shoutcast(false)
{
    unsigned int f = 44100;
    this->frequency = static_cast<DWORD>(f);
}

QString TrackInfo::getTrackPath() {
    return this->trackPath;
}


void TrackInfo::setTrackPath(QString path) {
    this->trackPath = path;
}

void TrackInfo::setField(int field, QString value) {
    if(field >= 0 && field < NUM_OF_FIELDS) {
        this->fields[field].clear();
        this->fields[field] = value;
    }
}

QString TrackInfo::getField(int field) {
    if(field >= 0 && field < NUM_OF_FIELDS) {
        return this->fields[field];
    }
    else
        return NULL;
}

void TrackInfo::clearFields() {
    for(int i = 0; i < NUM_OF_FIELDS; i++) {
        this->fields[i].clear();
    }
}

void TrackInfo::setIcyField(int field, QString value) {
    if(field >= 0 && field < ICY_NUM_OF_FIELDS) {
        this->icyfields[field].clear();
        this->icyfields[field] = value;
    }
}

QString TrackInfo::getIcyField(int field) {
    if(field >=0 && field < ICY_NUM_OF_FIELDS)
        return this->icyfields[field];
    else
        return NULL;
}

void TrackInfo::clearIcyFields() {
    for(int i = 0; i < ICY_NUM_OF_FIELDS; ++i)
        this->icyfields[i].clear();
}

bool TrackInfo::hasID3v1() {return id3v1;}

bool TrackInfo::hasID3v2() {return id3v2;}

bool TrackInfo::hasAPE() {return ape;}

bool TrackInfo::hasShoutCast() {return shoutcast;}

void TrackInfo::setID3v1(bool val) {id3v1 = val;}

void TrackInfo::setID3v2(bool val) {id3v2 = val;}

void TrackInfo::setAPE(bool val) {ape = val;}

void TrackInfo::setShoutCast(bool val) {shoutcast = true;}





