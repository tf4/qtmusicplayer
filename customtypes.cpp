#include "customtypes.h"
#include <QString>
//#include <windows.h> // for Sleep

using namespace global_methods;

bool global_methods::isURL(QString value) {
    if(value.startsWith("http://") || value.startsWith("https://") || value.startsWith("ftp://"))
        return true;
    else
        return false;
}

QString global_methods::formatLengthDisplay(QString length) {
    bool ok;
    int ilength, imins, isecs, ihrs;
    QString res;
    ilength = length.toInt(&ok);
    if(!ok)
        return "0:00";
    if(ilength > 0) {
        res = ":";
        imins = ilength/60;
        isecs = ilength%60;
        if(isecs > 9)
            res += QString::number(isecs);
        else
            res += "0" + QString::number(isecs);
        if(imins > 60) {
            ihrs = imins/60;
            imins %= 60;
            if(imins > 9)
                res.prepend(QString::number(imins));
            else
                res.prepend("0" + QString::number(imins));
            res.prepend(QString::number(ihrs)+":");
            return res;
        }
        else
            res.prepend(QString::number(imins));
            return res;
    }
    else
        return "0:00";
}

/*void global_methods::qSleep(int ms)
{
    //QTEST_ASSERT(ms > 0);

#ifdef Q_OS_WIN
    Sleep(uint(ms));
#else
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
#endif
}*/
