#ifndef CUSTOMTYPES_H
#define CUSTOMTYPES_H

#include <QList>
#include <QVector>

#define ITEMTEXT    0
#define ITEMLENGTH  1
#define ITEMPATH    2
#define ITEMHASINFO 3
#define ITEMCOLUMNS 4

typedef QList<QVector<QString> > VectorList;

namespace global_methods
{
    bool isURL(QString value);
    void qSleep(int ms);
    QString formatLengthDisplay(QString length);
}

#endif // CUSTOMTYPES_H
