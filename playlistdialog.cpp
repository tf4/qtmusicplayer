#include "playlistdialog.h"
#include "ui_playlistdialog.h"
#include <QDebug>
#include <QtGlobal>
#include <QFileDialog>
#include <QDirIterator>
#include <QTime>
#include <QSettings>
#include "customtypes.h"
#include "defaults.h"
#include <numeric>
#include <QMessageBox>
#include "urldialog.h"

using namespace global_methods;

PlaylistDialog::PlaylistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlaylistDialog),
    framePressed(false)
{   
    dock = new DockDialogManager();
    listOperationProgressTimer = new QTimer();
    ui->setupUi(this);
    execpath = QCoreApplication::applicationDirPath();
    ui->totalTimeLineEdit->setAlignment(Qt::AlignRight);
    ui->totalTimeLineEdit->setText("0:00/0:00");
    this->setupActions();
    shuffleHistoryCursor = 0;
    connect(ui->addToolButton, &ToolButtonLeftContextMenu::customContextMenuRequested, this, &PlaylistDialog::runAddContextMenu);
    connect(ui->selectToolButton, &ToolButtonLeftContextMenu::customContextMenuRequested, this, &PlaylistDialog::runSelectContextMenu);
    connect(ui->removeToolButton, &ToolButtonLeftContextMenu::customContextMenuRequested, this, &PlaylistDialog::runRemoveContextMenu);
    connect(ui->miscToolButton, &ToolButtonLeftContextMenu::customContextMenuRequested, this, &PlaylistDialog::runMiscContextMenu);
    connect(ui->managePlaylistToolButton, &ToolButtonLeftContextMenu::customContextMenuRequested, this, &PlaylistDialog::runManageContextMenu);
    connect(ui->playlistTableView, &PlaylistTableView::customContextMenuRequested,
            this, &PlaylistDialog::runItemContextMenu);
    //Action connections
    connect(this->actionAddFolders, &QAction::triggered, this, &PlaylistDialog::enqueueFolder);
    connect(this->actionOpenPlaylist, &QAction::triggered, this, &PlaylistDialog::openPlaylist);
    connect(this->actionNewPlaylist, &QAction::triggered, this, &PlaylistDialog::clearPlaylist);
    connect(this->actionAddFiles, &QAction::triggered, this, &PlaylistDialog::addFilesInPlaylist);
    connect(this->actionAddURL, &QAction::triggered, this, &PlaylistDialog::addUrlInPlaylist);
    connect(this->actionSavePlaylist, &QAction::triggered, this, &PlaylistDialog::savePlaylist);
    connect(ui->playlistTableView, &PlaylistTableView::signalPlaylistSavedSuccessfully,
            this, &PlaylistDialog::savePlaylistResult);
    connect(this->actionViewPlaylistEntry, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::editPlaylistEntry);
    connect(this->actionViewFileInfo, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::viewSelectedItemsInfo);
    connect(this->actionReverseList, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::reversePlaylist);
    connect(this->actionRandomizeList, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::randomizePlaylist);
    connect(this->actionSortByTitle, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::sortPlaylistByTitle);
    connect(this->actionSortByFilename, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::sortPlaylistByFilename);
    connect(this->actionSortByFilepath, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::sortPlaylistByFilepath);
    connect(this->actionRemoveSelected, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::removeSelectedItems);
    connect(this->actionRemoveAll, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::removeAllItems);
    connect(this->actionCropSelected, &QAction::triggered,
           ui->playlistTableView, &PlaylistTableView::cropSelectedItems);
    connect(this->actionRemoveMissing, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::removeMissingItems);
    connect(this->actionRemoveDuplicates, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::removeDuplicateItems);
    connect(this->actionSelectNone, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::selectNone);
    connect(this->actionSelectAll, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::selectAll);
    connect(this->actionInvertSelection, &QAction::triggered,
            ui->playlistTableView, &PlaylistTableView::invertSelections);
    connect(actionCancelOperation, &QAction::triggered,
            this, &PlaylistDialog::cancelListOperation, Qt::QueuedConnection);
    connect(actionPlayItem, &QAction::triggered, ui->playlistTableView, &PlaylistTableView::playSelectedItem);
    connect(this->ui->playlistTableView, &PlaylistTableView::signalPlaylistTotalDurationChanged,
            this, &PlaylistDialog::updateTotalTime);
    connect(this->ui->playlistTableView, &PlaylistTableView::signalSelectedItemsDurationChanged,
            this, &PlaylistDialog::updatePartialTime);
    connect(this->ui->playlistTableView, &PlaylistTableView::signalDoubleClickOnItem,
            this, &PlaylistDialog::playlistItemDoubleClicked);
    connect(this->ui->playlistTableView, &PlaylistTableView::signalPlaylistRowNumChanged,
            this, &PlaylistDialog::reshufflePlaylist);
    connect(this->ui->playlistTableView, &PlaylistTableView::signalListOperationRunning,
            this, &PlaylistDialog::blockUserInput);
    connect(this->listOperationProgressTimer, &QTimer::timeout, this, &PlaylistDialog::updateProgressBar);

}

bool PlaylistDialog::initializeDefaults() {
    this->ui->playlistTableView->readM3UList(QDir::toNativeSeparators(execpath) + DEF_PLAYLIST);
    return this->readConfigIniFile();
}

void PlaylistDialog::saveDefaults() {
    this->writeConfigIniFile();
    ui->playlistTableView->writeM3UList(QDir::toNativeSeparators(execpath) + DEF_PLAYLIST);
}




bool PlaylistDialog::isBusy() {return ui->playlistTableView->isBusy();}



bool PlaylistDialog::readConfigIniFile() {
    bool isShowed = false;
    QSettings settings(QDir::toNativeSeparators(execpath) + DEF_INIFILE, QSettings::IniFormat);
    settings.beginGroup(PLAYBACK);
    ui->playlistTableView->setBusyItemInPlaylist(settings.value(LAST_TRACK_LINE, -1).toInt());
    settings.endGroup();
    settings.beginGroup(DIMENSIONS);
    restoreGeometry(settings.value(PLS_GEOMETRY).toByteArray());
    settings.endGroup();
    settings.beginGroup(WINDOWS);
    if(settings.value(PLAYLIST, 1).toBool()) {
        this->show();
        isShowed = true;
    }
    settings.endGroup();
    return isShowed;
}

void PlaylistDialog::writeConfigIniFile() {
    QSettings settings(QDir::toNativeSeparators(execpath) + DEF_INIFILE, QSettings::IniFormat);
    settings.beginGroup(PLAYBACK);
    settings.setValue(LAST_TRACK_LINE, ui->playlistTableView->getBusyItemFromPlaylist());
    settings.endGroup();
    settings.beginGroup(DIMENSIONS);
    settings.setValue(PLS_GEOMETRY, saveGeometry());
    settings.endGroup();
    settings.beginGroup(WINDOWS);
    settings.setValue(PLAYLIST, this->isVisible());
    settings.endGroup();
}

void PlaylistDialog::openPlaylist() {
    QString playlistfile = QFileDialog::getOpenFileName(this, tr("Open Playlist"),
                                                        ".", tr("M3U Files (*.m3u)"));
    if(!playlistfile.isEmpty()) {
        clearPlaylist();
        ui->playlistTableView->readM3UList(playlistfile);
    }
}

void PlaylistDialog::savePlaylist() {
    QFileDialog fd;
    fd.setAcceptMode(QFileDialog::AcceptSave);
    QString playlistfile = fd.getSaveFileName(this, tr("Open Playlist"),
                                                        ".", tr("M3U Files (*.m3u)"));
    if(!playlistfile.isEmpty()) {
        ui->playlistTableView->writeM3UList(playlistfile);
    }
}

void PlaylistDialog::savePlaylistResult(bool success) {
    if(!success) {
        //This is not so essential, as windows warns user if files are read-only.
        QMessageBox::warning(this->parentWidget(), tr("Playlist not Saved"),
                             "Failed to save playlist correctly!\nUser canceled the operation or has no permission to write",
                             QMessageBox::Discard);
    }
    //emit signal in order to notify closing
    emit this->signalLastSaveDone(0);
}

void PlaylistDialog::addFilesInPlaylist() {
    QStringList playlistitems = QFileDialog::getOpenFileNames(this, tr("Add Files"),
                                                        ".", tr("Audio Files (*.mp3 *.wav *.wma)"));
    if(!playlistitems.isEmpty())
        this->ui->playlistTableView->addItems(playlistitems);
}

void PlaylistDialog::addUrlInPlaylist() {
    UrlDialog dlg(this->parentWidget());
    dlg.setWindowTitle(tr("Add URL"));
    connect(&dlg, &UrlDialog::signalUrlEntered, ui->playlistTableView, &PlaylistTableView::addItems);
    dlg.exec();
}

void PlaylistDialog::openNewUrl() {
    UrlDialog dlg(this->parentWidget());
    connect(&dlg, &UrlDialog::signalUrlEntered, this, &PlaylistDialog::playNewFiles);
    dlg.exec();
}

void PlaylistDialog::openFolders() {
    QString folder = QFileDialog::getExistingDirectory(this->parentWidget(), tr("Open Directory"),
                                                    ".",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(!folder.isEmpty()) {
        clearPlaylist();
        ui->playlistTableView->readM3UList(folder, true);
    }
}

void PlaylistDialog::enqueueFolder() {
    QString folder = QFileDialog::getExistingDirectory(this->parentWidget(), tr("Open Directory"),
                                                    ".",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(!folder.isEmpty()) {
        ui->playlistTableView->appendItemsToPlaylist(folder);
    }
}


void PlaylistDialog::playNewFiles(QStringList files) {
    TrackInfo tif;
    QString filepath;
    clearPlaylist();
    ui->playlistTableView->addItems(files);
    filepath = ui->playlistTableView->getItem(0, &tif);
    ui->playlistTableView->setBusyItemInPlaylist(0);
    //emit this->signalFetchedNewItem(filepath, tif);
    this->playlistItemDoubleClicked(filepath, tif);
}

void PlaylistDialog::clearPlaylist() {
    VectorList empty;
    ui->playlistTableView->setBusyItemInPlaylist(-1);
    ui->playlistTableView->createViewModel(empty);
}

void PlaylistDialog::cancelListOperation() {
    this->ui->playlistTableView->stopListOperation();
}

void PlaylistDialog::viewBusyItemInfo() {
    int i = ui->playlistTableView->getBusyItemFromPlaylist();
    if(i < 0) return;
    QVector<int> l;
    l.append(i);
    this->ui->playlistTableView->viewFileInfo(l);
}

void PlaylistDialog::updateProgressBar() {
    int value = ui->playlistTableView->getOperationProgress();
    if(value == 0) ui->listOperationProgressBar->setMaximum(0);
    else ui->listOperationProgressBar->setMaximum(100);
    ui->listOperationProgressBar->setValue(value);
}

void PlaylistDialog::updateTotalTime(int totaltime) {
    QStringList tms = ui->totalTimeLineEdit->text().split("/");
    QString new_text = tms.first() + "/" + global_methods::formatLengthDisplay(QString::number(totaltime));
    ui->totalTimeLineEdit->setText(new_text);
}

void PlaylistDialog::updatePartialTime(int partialtime) {
    QStringList tms = ui->totalTimeLineEdit->text().split("/");
    QString new_text = global_methods::formatLengthDisplay(QString::number(partialtime)) + "/" + tms.last();
    ui->totalTimeLineEdit->setText(new_text);
}

void PlaylistDialog::playlistItemDoubleClicked(QString filepath, TrackInfo tif) {
    emit this->signalPlaylistItemDoubleClicked(filepath, tif);
}

bool PlaylistDialog::isDocked() {return dock->isDocked();}


void PlaylistDialog::setDocked(bool value) {return;}


void PlaylistDialog::getNextPlaylistItem(bool shuffle, bool repeatall) {
    QString filepath;
    TrackInfo tif;
    int nextitem;
    if(!shuffle) {
        nextitem = ui->playlistTableView->getBusyItemFromPlaylist() + 1;
        if(nextitem < ui->playlistTableView->numOfRows()) {
            filepath = ui->playlistTableView->getItem(nextitem, &tif);
            ui->playlistTableView->setBusyItemInPlaylist(nextitem);
            emit this->signalFetchedNewItem(filepath, tif);
        }
        else {
            if(repeatall) {
                filepath = ui->playlistTableView->getItem(0, &tif);
                ui->playlistTableView->setBusyItemInPlaylist(0);
                emit this->signalFetchedNewItem(filepath, tif);
            }
        }
    }
    else {
        //shuffle case
        if(ui->playlistTableView->numOfRows() == 0) //No items in playlist. Just return
            return;
        //TODO: insert lock wherever shufflelist is accessed
        //If history cursor is positive traverse history list backwards one step
        if(shuffleHistoryCursor > 0 && shuffleHistoryList.size() - shuffleHistoryCursor > 0){
            shuffleHistoryCursor--;
            nextitem = shuffleHistoryList[shuffleHistoryCursor];
            //Check if selected item is out of bounds and remove it
            while(nextitem >= ui->playlistTableView->numOfRows()){
                if(shuffleHistoryList.size() <= shuffleHistoryCursor)
                    return;
                shuffleHistoryList.removeAt(shuffleHistoryCursor);
                nextitem = shuffleHistoryList[shuffleHistoryCursor];
            }
        }
        else {
            //If no items in shuffle list and repeatall is on get a new shuffle list to start again
            if(shuffledList.isEmpty() && repeatall){
                reshufflePlaylist();
            }
            if(shuffledList.isEmpty())
                return;
            //Get an item at a random position of shuffle list
            QTime time = QTime::currentTime();
            qsrand(uint(time.msec()));
            nextitem = shuffledList.takeAt(qrand() % shuffledList.size());
            //Check if selected item is out of bounds and remove it
            while(nextitem >= ui->playlistTableView->numOfRows()) {
                if(shuffledList.isEmpty())
                    return;
                nextitem = shuffledList.takeAt(qrand() % shuffledList.size());
            }
            //Check if selected item is also in shuffle history list and remove it
            int existsinhistory = shuffleHistoryList.lastIndexOf(nextitem);
            if(existsinhistory > -1)
                shuffleHistoryList.removeAt(existsinhistory);
            //Add item in shuffle history
            shuffleHistoryList.prepend(nextitem);
        }
        //Notify audioplayer
        filepath = ui->playlistTableView->getItem(nextitem, &tif);
        ui->playlistTableView->setBusyItemInPlaylist(nextitem);
        emit this->signalFetchedNewItem(filepath, tif);
    }
}

void PlaylistDialog::getPreviousPlaylistItem(bool shuffle) {
    QString filepath;
    TrackInfo tif;
    int previousitem;
    if(!shuffle) {
        previousitem = ui->playlistTableView->getBusyItemFromPlaylist() - 1;
        if(previousitem >= 0) {
            filepath = ui->playlistTableView->getItem(previousitem, &tif);
            ui->playlistTableView->setBusyItemInPlaylist(previousitem);
            emit this->signalFetchedNewItem(filepath, tif);
        }
    }
    else {//shuffle case
        //No items in playlist or just the one currently played. return
        if(ui->playlistTableView->numOfRows() == 0 || shuffleHistoryList.size() <= 1)
            return;
        //If history cursor does not exceed bounds, traverse history list one step forward
        if(shuffleHistoryList.size() - 1 > shuffleHistoryCursor){
            shuffleHistoryCursor++;
            previousitem = shuffleHistoryList[shuffleHistoryCursor];
            //Check if item is out of playlist bounds and remove it
            while(previousitem >= ui->playlistTableView->numOfRows()) {
                if(shuffleHistoryList.size() <= shuffleHistoryCursor)
                    return;
                shuffleHistoryList.removeAt(shuffleHistoryCursor);
                previousitem = shuffleHistoryList[shuffleHistoryCursor];
                //Check if selected item exists in shuffle list and remove it (in order to be played only once)
                int existsinshuffle = shuffledList.lastIndexOf(previousitem);
                if(existsinshuffle > -1)
                    shuffledList.removeAt(existsinshuffle);
            }
        }
        else{
            return;
        }
        //Notify audioplayer
        filepath = ui->playlistTableView->getItem(previousitem, &tif);
        ui->playlistTableView->setBusyItemInPlaylist(previousitem);
        emit this->signalFetchedNewItem(filepath, tif);
    }
}

void PlaylistDialog::reshufflePlaylist(bool clear) {
    int listsum = 0;
    int rownum = ui->playlistTableView->numOfRows();
    if(rownum == 0){//nothing to reshuffle
        clearShuffledPlaylist();
        return;
    }
    if(clear){
        clearShuffledPlaylist();
    }
    else{
        if(!shuffledList.isEmpty())
            listsum = shuffledList[shuffledList.size() - 1] + 1;
    }
    if(listsum < rownum - 1) {//case where rows were inserted
        for(int i = listsum ; i < rownum; i++) {
            shuffledList.append(i);
        }
    }
    else {//case where rows were removed
        while(!shuffledList.isEmpty()){
            if(shuffledList[shuffledList.size() - 1] > rownum - 1)
                shuffledList.removeLast();
            else
                break;
        }
    }
}

void PlaylistDialog::clearShuffledPlaylist() {
    shuffledList.clear();
    shuffleHistoryList.clear();
    shuffleHistoryCursor = 0;
}

void PlaylistDialog::blockUserInput(bool block) {
    ui->addToolButton->setDisabled(block);
    ui->removeToolButton->setDisabled(block);
    ui->selectToolButton->setDisabled(block);
    ui->miscToolButton->setDisabled(block);
    ui->stackedWidget->setCurrentIndex(static_cast<int>(block));
    if(block) this->listOperationProgressTimer->start(100);
    else this->listOperationProgressTimer->stop();
    this->actionNewPlaylist->setDisabled(block);
    this->actionOpenPlaylist->setDisabled(block);
    this->actionSavePlaylist->setDisabled(block);
    this->actionCancelOperation->setDisabled(!block);
    emit this->signalMainBlockUserInput(block);
}



void PlaylistDialog::setUrlItemInfo(TrackInfo *tif) {
    ui->playlistTableView->updateItemInfo(ui->playlistTableView->getBusyItemFromPlaylist(),
                                          tif);
}

void PlaylistDialog::showEvent(QShowEvent *ev) {
    /*if(!ev->spontaneous()) {
        if(isDocked())
            this->move(this->parentWidget()->x()+this->parentWidget()->frameGeometry().width(),
                       this->parentWidget()->y());
        else
            this->move(currentGeometry.topLeft());
        this->resize(currentGeometry.size());
    }
    ev->accept();*/
}

void PlaylistDialog::hideEvent(QHideEvent *ev) {
    if(!ev->spontaneous()) {
        //currentGeometry = QRect(this->frameGeometry().topLeft(), this->size());
    }
}

void PlaylistDialog::closeEvent(QCloseEvent *) {
    //currentGeometry = QRect(this->frameGeometry().topLeft(), this->size());
    emit this->signalCloseEvent();
}

bool PlaylistDialog::event(QEvent *ev) {
    if(ev->type() == QEvent::NonClientAreaMouseButtonPress) {
        framePressed = true;
        dock->initDockState(this->frameGeometry(), parentWidget()->frameGeometry());
        return QDialog::event(ev);
    }
    if(ev->type() == QEvent::NonClientAreaMouseButtonRelease) {
        framePressed = false;
        dock->updateDockState(this->frameGeometry(), parentWidget()->frameGeometry());
        dock->updateDockedWinPos(this->winId(), this->frameGeometry(), parentWidget()->frameGeometry());
        return QDialog::event(ev);
    }
    if(ev->type() == QEvent::NonClientAreaMouseMove) {
        return QDialog::event(ev);
    }
    if(ev->type() == QEvent::Resize) {
        dock->initDockState(this->frameGeometry(), parentWidget()->frameGeometry());
        return QDialog::event(ev);
    }
    if(ev->type() == QEvent::Move) {
        if(framePressed) {
            ev->ignore();
            return true;
        }
        return QDialog::event(ev);
    }
    if(ev->type() == QEvent::MouseMove) {
        return QDialog::event(ev);
    }
    return QDialog::event(ev);
}

bool PlaylistDialog::nativeEvent(const QByteArray &eventType, void *message, long *result) {
    Q_UNUSED(eventType); Q_UNUSED(result);
    if(framePressed)
        dock->handleWin32MoveEvent(message, this->winId(), this->frameGeometry(), parentWidget()->frameGeometry());
    return false;
}



void PlaylistDialog::dragWindow(int x, int y) {
    if(isVisible() && isDocked()) {
        move(this->pos().x() + x, this->pos().y() + y);
    }
}

PlaylistDialog::~PlaylistDialog() {
    delete ui;
}

//Private methods

void PlaylistDialog::runAddContextMenu(const QPoint &point) {
    QMenu addToolButtonMenu(this);
    addToolButtonMenu.addAction(actionAddFiles);
    addToolButtonMenu.addAction(actionAddFolders);
    addToolButtonMenu.addAction(actionAddURL);
    addToolButtonMenu.exec(this->ui->addToolButton->mapToGlobal(point));
}

void PlaylistDialog::runSelectContextMenu(const QPoint &point) {
    QMenu selectToolButtonMenu(this);
    selectToolButtonMenu.addAction(actionSelectAll);
    selectToolButtonMenu.addAction(actionSelectNone);
    selectToolButtonMenu.addAction(actionInvertSelection);
    selectToolButtonMenu.exec(this->ui->selectToolButton->mapToGlobal(point));
}

void PlaylistDialog::runRemoveContextMenu(const QPoint &point) {
    QMenu removeToolButtonMenu(this);
    removeToolButtonMenu.addAction(actionRemoveSelected);
    removeToolButtonMenu.addAction(actionCropSelected);
    removeToolButtonMenu.addAction(actionRemoveAll);
    removeToolButtonMenu.addSeparator();
    QMenu *removeToolButtonSubMenu = removeToolButtonMenu.addMenu("Remove...");
    removeToolButtonSubMenu->addAction(actionRemoveMissing);
    removeToolButtonSubMenu->addAction(actionRemoveDuplicates);
    removeToolButtonMenu.exec(this->ui->removeToolButton->mapToGlobal(point));
}

void PlaylistDialog::runMiscContextMenu(const QPoint &point) {
    QMenu miscToolButtonMenu(this);
    QMenu *miscToolButtonSortSubMenu = miscToolButtonMenu.addMenu("Sort");
    miscToolButtonSortSubMenu->addAction(actionSortByTitle);
    miscToolButtonSortSubMenu->addAction(actionSortByFilename);
    miscToolButtonSortSubMenu->addAction(actionSortByFilepath);
    miscToolButtonSortSubMenu->addSeparator();
    miscToolButtonSortSubMenu->addAction(actionReverseList);
    miscToolButtonSortSubMenu->addAction(actionRandomizeList);
    QMenu *miscToolButtonFileInfoSubMenu = miscToolButtonMenu.addMenu("File Info");
    miscToolButtonFileInfoSubMenu->addAction(actionViewFileInfo);
    miscToolButtonFileInfoSubMenu->addAction(actionViewPlaylistEntry);
    miscToolButtonMenu.exec(this->ui->miscToolButton->mapToGlobal(point));
}

void PlaylistDialog::runManageContextMenu(const QPoint &point) {
    QMenu managePlaylistToolButtonMenu(this);
    managePlaylistToolButtonMenu.addAction(actionOpenPlaylist);
    managePlaylistToolButtonMenu.addAction(actionSavePlaylist);
    managePlaylistToolButtonMenu.addAction(actionNewPlaylist);
    managePlaylistToolButtonMenu.addSeparator();
    managePlaylistToolButtonMenu.addAction(actionCancelOperation);
    managePlaylistToolButtonMenu.exec(this->ui->managePlaylistToolButton->mapToGlobal(point));
}

void PlaylistDialog::runItemContextMenu(const QPoint &point) {
    QMenu itemMenu(this);
    itemMenu.addAction(actionPlayItem);
    itemMenu.addSeparator();
    itemMenu.addAction(actionRemoveSelected);
    itemMenu.addAction(actionCropSelected);
    itemMenu.addSeparator();
    itemMenu.addAction(actionViewFileInfo);
    itemMenu.addAction(actionViewPlaylistEntry);
    itemMenu.exec(this->ui->playlistTableView->mapToGlobal(point));
}

void PlaylistDialog::setupActions() {
    //--add
    this->actionAddFiles = new QAction(this);
    this->actionAddFiles->setText("Add File(s)");
    this->actionAddFiles->setObjectName("actionAddFiles");
    this->actionAddFolders = new QAction(this);
    this->actionAddFolders->setText("Add Folder");
    this->actionAddFolders->setObjectName("actionAddFolders");
    this->actionAddURL = new QAction(this);
    this->actionAddURL->setText("Add URL");
    this->actionAddURL->setObjectName("actionAddURL");
    this->actionSelectAll = new QAction(this);
    //--select
    this->actionSelectAll->setText("Select All");
    this->actionSelectAll->setObjectName("actionSelectAll");
    this->actionSelectNone = new QAction(this);
    this->actionSelectNone->setText("Select None");
    this->actionSelectNone->setObjectName("actionSelectNone");
    this->actionInvertSelection = new QAction(this);
    this->actionInvertSelection->setText("Invert Selection");
    this->actionInvertSelection->setObjectName("actionInvertSelection");
    //---remove
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->actionRemoveSelected = new QAction(this);
    this->actionRemoveSelected->setText("Remove Selected");
    this->actionRemoveSelected->setObjectName("actionRemoveSelected");
    this->actionCropSelected = new QAction(this);
    this->actionCropSelected->setText("Crop Selected");
    this->actionCropSelected->setObjectName("actionCropSelected");
    this->actionRemoveAll = new QAction(this);
    this->actionRemoveAll->setText("Remove All");
    this->actionRemoveAll->setObjectName("actionRemoveAll");
    this->actionRemoveMissing = new QAction(this);
    this->actionRemoveMissing->setText("Remove Missing");
    this->actionRemoveMissing->setObjectName("actionRemoveMissing");
    this->actionRemoveDuplicates = new QAction(this);
    this->actionRemoveDuplicates->setText("Remove Duplicates");
    this->actionRemoveDuplicates->setObjectName("actionRemoveDuplicates");
    //---misc
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->actionSortByTitle = new QAction(this);
    this->actionSortByTitle->setText("Sort by Title");
    this->actionSortByTitle->setObjectName("actionSortByTitle");
    this->actionSortByFilename = new QAction(this);
    this->actionSortByFilename->setText("Sort by Filename");
    this->actionSortByFilename->setObjectName("actionSortByFilename");
    this->actionSortByFilepath = new QAction(this);
    this->actionSortByFilepath->setText("Sort by Filepath");
    this->actionSortByFilepath->setObjectName("actionSortByFilepath");
    this->actionReverseList = new QAction(this);
    this->actionReverseList->setText("Reverse List");
    this->actionReverseList->setObjectName("actionReverseList");
    this->actionRandomizeList = new QAction(this);
    this->actionRandomizeList->setText("Randomize List");
    this->actionRandomizeList->setObjectName("actionRandomizeList");
    this->actionViewFileInfo = new QAction(this);
    this->actionViewFileInfo->setText("View File Info");
    this->actionViewFileInfo->setObjectName("actionViewFileInfo");
    this->actionViewPlaylistEntry = new QAction(this);
    this->actionViewPlaylistEntry->setText("View Playlist Entry");
    this->actionViewPlaylistEntry->setObjectName("actionViewPlaylistEntry");
    //---manage
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->actionOpenPlaylist = new QAction(this);
    this->actionOpenPlaylist->setText("Open Playlist");
    this->actionOpenPlaylist->setObjectName("actionOpenPlaylist");
    this->actionSavePlaylist = new QAction(this);
    this->actionSavePlaylist->setText("Save Playlist");
    this->actionSavePlaylist->setObjectName("actionSavePlaylist");
    this->actionNewPlaylist = new QAction(this);
    this->actionNewPlaylist->setText("New Playlist(Clear)");
    this->actionNewPlaylist->setObjectName("actionNewPlaylist");
    this->actionCancelOperation = new QAction(this);
    this->actionCancelOperation->setText("Cancel Operation");
    this->actionCancelOperation->setObjectName("actionCancelOperation");
    this->actionCancelOperation->setEnabled(false);
    //
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->actionPlayItem = new QAction(this);
    this->actionPlayItem->setText("Play item");
    this->actionPlayItem->setObjectName("actionPlayItem");
}



