#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "playlistdialog.h"
#include "bassaudio.h"
#include "trackinfo.h"
#include "utils/dockdialogmanager.h"

enum playback_states {STOP_ST=0, PLAY_ST=1, PAUSE_ST=2, FIN_ST=3};
enum playback_transitions {STOP_TRS=0, PLAY_TRS=1, NEXT_TRS=2, PAUSE_TRS=3, FIN_TRS=4};
struct playbackstate {
    bool out[5];
    playback_states next[5];
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //Configuration methods
    void setupActions();
    void readConfigIniFile();
    void writeConfigIniFile();
    void setWindowSize(QSize sz);
    void loadPlaybackOptions();
    //Methods for handling time, and track info display
    void setupLabelTags(bool url=false);
    void setTrackDisplayInfo(TrackInfo trackinfo);
    void receivedUrlMetaInfo(const char* metainfo, const char* metaextra);
    void setAudioInfo(QString bitrate="0", QString freq="0", QString channels="0");
    void initializeTrackPositionDisplay(int length);
    void updateTrackPositionDisplay(int length);
    //Methods regarding playback
    bool playCurrentTrack();
    void stopCurrentTrack();
    void pauseCurrentTrack();
    void playNextTrack();
    void playPreviousTrack();
    void toggleShuffle();
    void playlistDoubleClicked(QString filepath, TrackInfo tif);
    void receivedNextTrack(QString filepath, TrackInfo tif);
    void trackFinished();
    //Methods for opening/enqueing files
    void openFiles();
    void enqueueFiles();
    //Methods regarding the track that is currently playing
    QString getCurrentTrack();
    void setCurrentTrack(QString path);
    //Methods for moving backward/forward inside the currently playing track
    void seekTrackPosition();
    void seekTime();
    void handleSliderPress();
    void forwardNSEcs();
    void backNSecs();
    //Methods for setting volume levels
    void volumeSlide();
    void volumeUp();
    void volumeDown();
    void toggleVolumeMute();
    //Methods for showing/hiding child windows
    void togglePlaylistVisibility();
    void showAboutInfo();
    //Event handlers
    void blockUserInput(bool block);
    void moveEvent(QMoveEvent *ev);
    bool eventFilter(QObject *source, QEvent *event);
    bool event(QEvent *event);
    void resizeEvent(QResizeEvent *ev);
    void closeEvent(QCloseEvent *event);


private:
    //variables
    Ui::MainWindow *ui;
    PlaylistDialog *playlistdialog;
    BassAudio *audio;
    QString execPath;
    QString currentTrack;
    playbackstate *playback_fsm;
    playbackstate currentPlaybackState;   
    bool trackSliderGroovePressed;
    int trackSliderLastPos;
    bool renderFinished;
    bool framePressed;
    DockDialogManager *dock;
    //methods
    void executePlaybackFsm(playback_transitions transition);
};

#endif // MAINWINDOW_H
