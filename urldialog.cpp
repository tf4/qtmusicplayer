#include "urldialog.h"
#include "ui_urldialog.h"

UrlDialog::UrlDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UrlDialog)
{
    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &UrlDialog::onAccept);
}

void UrlDialog::onAccept() {
    if(!ui->comboBox->currentText().isEmpty())
        emit this->signalUrlEntered(QStringList() << ui->comboBox->currentText());
    this->accept();
}

UrlDialog::~UrlDialog()
{
    delete ui;
}
